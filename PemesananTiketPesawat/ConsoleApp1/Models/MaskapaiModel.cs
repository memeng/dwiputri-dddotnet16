﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace PemesananTiketPesawat.Models
{
    [Table("Maskapai")]
    public class MaskapaiModel
    {
        [Key]
        [Column("kode_maskapai")]
        public string KodeMaskapai { get; set; }
        [Column("nomor_maskapai")]
        public string NomorMaskapai { get; set; }




    }
}
