﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace PemesananTiketPesawat.Models
{   
    [Table("Pemesanan")]
    public class PemesananModel
    {
        [Key]
        [Column("kode_pemesanan")]
        public long KodePemesanan { get; set; }
        [Column("nama_asal_bandara")]
        public string NamaBandaraAsal { get; set; }
        [Column("asal_bandara")]
        public string DaerahBandaraAsal { get; set; }
        [Column("tujuan_bandara")]
        public string DaerahBandaraTujuan { get; set; }
        [Column("nama_tujuan_bandara")]
        public string NamaBandaraTujuan { get; set; }
        [Column("tanggal_berangkat")]
        public DateTime TanggalKeberangkatan { get; set; }
        [Column("tanggal_kedatangan")]
        public DateTime TanggalKedatangan { get; set; }
        [Column("jam_keberangkatan")]
        public DateTime JamKeberangkatan { get; set; }
        [Column("jam_kedatangan")]
        public DateTime JamKedatangan { get; set; }

        [Column("kode_maskapai")]
        public string KodeMaskapai { get; set; }
        [Column("nama_maskapai")]
        public string NamaMaskapai { get; set; }
        [Column("durasi")]
        public DateTime Durasi { get; set; }
        [Column("batas_bagasi")]
        public Decimal BatasBagasi { get; set; }
        [Column("batas_bagasi_kabin")]
        public Decimal BatasBagasiKabin { get; set; }
        [Column("metode_pembayaran")]
        public string MetodePembayaran { get; set; }
        [Column("harga_tiket_perorang")]
        public Decimal HargaTiketPerOrang { get; set; }
        [Column("jumlah_orang")]
        public int JumlahOrang { get; set; }
        [Column("total_harga_tiket")]
        public Decimal TotalHargaTiket { get; set; }
        [Column("status_pemesanan")]
        public string StatusPemesanan { get; set; }
        [Column("kode_pembayaran")]
        public string KodePembayaran { get; set; }
        [Column("kode_checkin")]
        public string KodeCheckIn { get; set; }
        [Column("kode_checkout")]
        public string KodeCheckOut { get; set; }
        [Column("kode_penerbangan")]
        public string KodePenerbangan { get; set; }

    }
}
