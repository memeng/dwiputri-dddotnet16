﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace PemesananTiketPesawat.Models
{
    [Table("Jenis_Pesawat")]
    public class JenisPesawatModel
    {
        [Key]
        [Column("kode_pesawat")]
        public string KodePesawat { get; set; }
        [Column("tahun_pesawat")]
        public string TahunPesawat { get; set; }


    }
}
