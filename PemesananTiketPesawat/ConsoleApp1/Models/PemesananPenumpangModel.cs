﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace PemesananTiketPesawat.Models
{
    [Table("Pemesanan")]
    public class PemesananPenumpangModel
    {
        [Key]
        [Column("judul")]
        public string Judul { get; set; }
        [Column("nama_penumpang")]
        public string NamaPenumpang { get; set; }
        [Column("KTP")]
        public string KTP { get; set; }

        public string KodePemesanan { get; set; }

        public string NomorKursi { get; set; }

    }
}
