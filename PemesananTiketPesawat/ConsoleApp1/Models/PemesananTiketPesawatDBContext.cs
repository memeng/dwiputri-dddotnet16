﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PemesananTiketPesawat.Models
{ 
    public class PemesananTiketPesawatDBContext : DbContext //represents a session with the database and can be used
                                                            //to query and save instances of your entities
    {
        // class represents an entity set that can be used for create, read, update, and delete operations.
        //DbSet<TEntity> Class
        //A non-generic version of DbSet<TEntity> which can be used when the type of entity is not known at build time.
        //Inheritance   Object DbQuery DbSet
        public DbSet<JadwalModel>? Jadwals { get; set; }
        public DbSet<BandaraModel>? Bandaras { get; set; }
        public DbSet<GerbangBandaraModel>? Gerbangs { get; set; }
        public DbSet<JenisPesawatModel>? JenisPesawats { get; set; }
        public DbSet<KursiPesawatModel>? KursiPesawats { get; set; }
        public DbSet<MaskapaiModel>? Maskapais { get; set; }

        public DbSet<PemesananModel>? Pemesanans { get; set; }


        //use a DbContextOptionsBuilder to create instances of this class
        //and it is not designed to be directly constructed in your application code.
        public PemesananTiketPesawatDBContext(DbContextOptions<PemesananTiketPesawatDBContext> dbContextOptions) : base(dbContextOptions)
        {

        }

        private string _connString; //string untuk mengkoneksikan database
        public PemesananTiketPesawatDBContext(string connectionString)
        {
            _connString = connectionString;
        }

        //You can use ModelBuilder to construct a model for a context by overriding OnModelCreating(ModelBuilder)
        //on your derived context. Alternatively you can create the model externally and set it on a
        //DbContextOptions instance that is passed to the context constructor.
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JadwalModel>();
        }
        //Used to configure TOptions instances.
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connString);
        }


    }
}
