﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema; //Provides attribute classes that are used to define metadata for ASP.NET MVC and ASP.NET data controls.
using System.ComponentModel.DataAnnotations; //Provides attribute classes that are used to define metadata for ASP.NET MVC and ASP.NET data controls.

namespace PemesananTiketPesawat.Models
{

    [Table("Bandara")]
    public class BandaraModel
    {
        [Key]
        [Column("kode_bandara")]
        public string KodeBandara { get; set; }
        [Column("nama_bandara")]
        public string NamaBandara { get; set; }
        [Column("provinsi_bandara")]
        public string ProvinsiBandara { get; set; }
        [Column("kota_bandara")]
        public string KotaBandara { get; set; }






    }
}
