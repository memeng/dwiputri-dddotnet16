﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace PemesananTiketPesawat.Models
{
    [Table("Pesawat")]
    public class PesawatModel
    {
        [Key]
        [Column("kode_penerbangan")]
        public string KodePenerbangan { get; set; }
        [Column("jenis_pesawat")]
        public string JenisPesawat { get; set; }
        [Column("kode_maskapai")]
        public string KodeMaskapai { get; set; }

    }
}
