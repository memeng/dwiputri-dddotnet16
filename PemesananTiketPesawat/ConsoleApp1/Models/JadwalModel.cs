﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace PemesananTiketPesawat.Models
{
    [Table("Jadwal")]
    public class JadwalModel
    {   
        [Key]
        [Column("kode_jadwal")]
        public long KodeJadwal { get; set; }
        [Column("kode_maskapai")]
        public string KodeMaskapai { get; set; }
        [Column("kode_penerbangan")]
        public string KodePenerbangan { get; set; }
        [Column("tanggal_keberangkatan")]
        public DateTime TanggalKeberangkatan { get; set; }
        [Column("jam_keberangkatan")]
        public DateTime JamKeberangkatan { get; set; }
        [Column("jam_kedatangan")]
        public DateTime JamKedatangan { get; set; }
        [Column("batas_bagasi")]
        public Decimal BatasBagasi { get; set; }
        [Column("batas_bagasi_kabin")]
        public Decimal BatasBagasiKabin { get; set; }
        [Column("kode_bandara_asal")]
        public string KodeBandaraAsal { get; set; }
        [Column("kode_bandara_tujuan")]

        public string KodeBandaraTujuan { get; set; }
        [Column("tanggal_kedatangan")]

        public DateTime TanggalKedatangan { get; set; }








    }
}
