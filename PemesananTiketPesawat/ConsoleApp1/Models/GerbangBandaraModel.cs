﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace PemesananTiketPesawat.Models
{
    [Table("Gerbang")]

    public class GerbangBandaraModel
    {
        [Key]
        [Column("kode_gerbang")]
        public long KodeGerbang { get; set; }
        [Column("nomor_gerbang")]
        public string NomorGerbang { get; set; }
        [Column("nomor_pintu")]
        public string NomorPintu { get; set; }
        [Column("kode_bandara")]
        public string KodeBandara { get; set; }


    }
}
