﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace PemesananTiketPesawat.Models
{   
    [Table("Kursi_Pesawat_Model")]
    public class KursiPesawatModel
    {
        [Key]
        [Column("nomor_kursi")]
        public string NomorKursi { get; set; }
        [Column("status_kursi")]
        public string StatusKursi { get; set; }
        [Column("kode_penerbangan")]
        public string KodePenerbangan { get; set; }


    }
}
