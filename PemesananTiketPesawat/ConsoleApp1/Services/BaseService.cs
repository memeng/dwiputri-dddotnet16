﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PemesananTiketPesawat;
using PemesananTiketPesawat.Models;

namespace PemesananTiketPesawat.Services
{
    public class BaseService
    {   //buat variabel untuk menyompan ke database
        protected Models.PemesananTiketPesawatDBContext _db;
        //buat konstruktornya
        public BaseService(Models.PemesananTiketPesawatDBContext db)
        {
            _db = db;
        }
    }
}
