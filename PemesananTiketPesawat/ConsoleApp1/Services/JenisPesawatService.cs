﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PemesananTiketPesawat.Models;
using Microsoft.EntityFrameworkCore;

namespace PemesananTiketPesawat.Services
{
    public class JenisPesawatService : BaseService //class JenisPesawatService terdeverisasi dari class BaseService
    {
        public List <Models.JenisPesawatModel> _jenisPesawat { get; set; }

        public JenisPesawatService(PemesananTiketPesawatDBContext db) : base(db) //memanggil konstruktor db pada class basis
        {
            //_bandaras = new List<Models.BandaraModel>();
        }

        //metode untuk mengalihkan ke database
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection(); // var _dbConn adalah   
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE airport;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        //validasidata
        private void ValidasiData(string tahunPesawat)

        {
            if (string.IsNullOrEmpty(tahunPesawat))
            {
                throw new Exception("tahun_pesqawat_kosong");
            }
        }

        private void aturNilai(ref Models.JenisPesawatModel _jenispesawat,
           string tahunPesawat) //untuk refrensi menggunakan 
                                                                           //objek baru yaitu _jadwal metode diatur menjadi private mungkin agar tidak                                                                //mempengaruhi metode diluarnya
        {
            _jenispesawat.TahunPesawat = tahunPesawat;
        }

        public string AddJenisPesawat(string tahunPesawat) //belum tahu kenapa long
        {
            this.ValidasiData(tahunPesawat); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var _jenispesawat = new Models.JenisPesawatModel(); //buat objek baru _jadwal


            aturNilai(ref _jenispesawat, tahunPesawat); //panggil aturnilai

            _db.JenisPesawats.Add(_jenispesawat);
            _db.SaveChanges();

            return _jenispesawat.KodePesawat;
        }

        private Models.JenisPesawatModel findByKodepesawat(string kodePesawat)
        {
            /*  if(string.IsNullOrEmpty(kodeAirport))
            {
                throw new ArgumentNullException("kode_airport_is_null");
            }
            var _fAirport = _db.Airports?.Where(x => x.KodeAiport.ToLower().Equals(kodeAirport)).FirstOrDefault();
            if(_fAirport == null)
            {
                throw new NullReferenceException("airport_not_found");
            }
            return _fAirport;*/
            if (string.IsNullOrEmpty(kodePesawat))
            {
                throw new ArgumentNullException("kode_pesawat_kosong");
            }
            var _fJenispesawat  = _db.JenisPesawats?.Where(x => x.KodePesawat.ToLower().Equals(kodePesawat)).FirstOrDefault();
            //var _fAirport = _db.Airports?.Where(x => x.KodeAiport.ToLower().Equals(kodeAirport)).FirstOrDefault();
            if (_fJenispesawat == null)
            {
                throw new NullReferenceException("kode_pesawat_not_found");
            }
            return _fJenispesawat;
        }

        public Models.JenisPesawatModel FindPesawatByKodePesawat(string kodePesawat)
        {
            return findByKodepesawat(kodePesawat);
        }


    }


}
