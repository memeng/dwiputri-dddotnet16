﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PemesananTiketPesawat.Models;
using Microsoft.EntityFrameworkCore;

namespace PemesananTiketPesawat.Services
{
    public class MaskapaiService : BaseService //class JenisPesawatService terdeverisasi dari class BaseService
    {
        public List<Models.MaskapaiModel> _maskapai { get; set; }

        public MaskapaiService(PemesananTiketPesawatDBContext db) : base(db) //memanggil konstruktor db pada class basis
        {
            //_bandaras = new List<Models.BandaraModel>();
        }

        //metode untuk mengalihkan ke database
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection(); // var _dbConn adalah   
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE airport;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        //validasidata
        private void ValidasiData(string nomorMaskapai)

        {

            if (string.IsNullOrEmpty(nomorMaskapai))
            {
                throw new Exception("nomor_maskapai_kosong");
            }

        }

        private void aturNilai(ref Models.MaskapaiModel _maskapai, string nomormaskapai) //untuk refrensi menggunakan 
                                                                                         //objek baru yaitu _jadwal metode diatur menjadi private mungkin agar tidak                                                                //mempengaruhi metode diluarnya
        { 
            _maskapai.NomorMaskapai = nomormaskapai;
        }

        public string AddMaskapai(string nomormaskapai) //belum tahu kenapa long
        {
            this.ValidasiData(nomormaskapai); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var _maskapai = new Models.MaskapaiModel(); //buat objek baru _jadwal


            aturNilai(ref _maskapai, nomormaskapai); //panggil aturnilai

            _db.Maskapais.Add(_maskapai);
            _db.SaveChanges();

            return _maskapai.KodeMaskapai;
        }

        private Models.MaskapaiModel findByKodeMaskapai(string kodeMaskapai)
        {

            if (string.IsNullOrEmpty(kodeMaskapai))
            {
                throw new ArgumentNullException("kode_maskapai_kosong");
            }
            var _fMaskapai = _db.Maskapais?.Where(x => x.KodeMaskapai.ToLower().Equals(kodeMaskapai)).FirstOrDefault();
            //var _fAirport = _db.Airports?.Where(x => x.KodeAiport.ToLower().Equals(kodeAirport)).FirstOrDefault();

            if (_fMaskapai == null)
            {
                throw new NullReferenceException("kursi_maskapai_not_found");
            }
            return _fMaskapai;
        }

        public Models.MaskapaiModel FindByKodeMaskapai(string kodeMaskapai)
        {
            return findByKodeMaskapai(kodeMaskapai);
        }


    }


}
