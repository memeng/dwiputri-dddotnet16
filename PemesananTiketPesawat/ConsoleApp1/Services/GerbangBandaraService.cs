﻿using PemesananTiketPesawat.Models;
using Microsoft.EntityFrameworkCore;


namespace PemesananTiketPesawat.Services
{
    public class GerbangBandaraService : BaseService
    {
        //sambungkan dulu dengan modelnya
        public List<Models.GerbangBandaraModel> _gerbangs { get; set; }

        //buat konstruktor kelasnya
        public GerbangBandaraService(PemesananTiketPesawatDBContext db) : base(db)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE airport_gateway;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        //validasidata
        private void ValidasiData(string nomorGerbang, string nomorPintu, string kodeBandara)

        {
            if (string.IsNullOrEmpty(nomorGerbang))
            {
                throw new Exception("nomor_Gerbang_kosong");
            }

            if (string.IsNullOrEmpty(nomorPintu))
            {
                throw new Exception("nomor_pintu_kosong");
            }
            if (string.IsNullOrEmpty(kodeBandara))
            {
                throw new Exception("kode_Bandara_kosong");
            }

        }


        private void aturNilai(ref Models.GerbangBandaraModel _gerbang,
            string nomorGerbang, string nomorPintu, string kodeBandara) //untuk refrensi menggunakan 
                                                                        //objek baru yaitu _jadwal metode diatur menjadi private mungkin agar tidak
                                                                        //mempengaruhi metode diluarnya
        {
            _gerbang.NomorGerbang = nomorGerbang;
            _gerbang.NomorPintu = nomorPintu;
            _gerbang.KodeBandara = kodeBandara;

        }

        public long AddGerbang(string nomorGerbang, string nomorPintu, string kodeBandara) //belum tahu kenapa long
        {
            this.ValidasiData(nomorGerbang, nomorPintu, kodeBandara); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var _gerbang = new Models.GerbangBandaraModel(); //buat objek baru _jadwal

            _gerbang.KodeGerbang = Helpers.IDHelper.ID();

            aturNilai(ref _gerbang, nomorGerbang, nomorPintu, kodeBandara); //panggil aturnilai

           // _gerbangs.Add(_gerbang); //menambahkan element p11111111111111ada list

            _db.Gerbangs.Add(_gerbang);
            _db.SaveChanges();
            return _gerbang.KodeGerbang;
        }

        public Models.GerbangBandaraModel _cariDenganKode(long kodeGerbang)
        {
            if (kodeGerbang <= 0)
            {
                throw new ArgumentException("invalid_id_gateway");
            }
            var _fGerbangBandara = _db.Gerbangs.Where(x => x.KodeGerbang == kodeGerbang).FirstOrDefault();
            if (_fGerbangBandara == null)
            {
                throw new NullReferenceException("airport_gateway_not_found");
            }
            return _fGerbangBandara;
        }

        public Models.GerbangBandaraModel cariDenganKode(long kodeGerbang)
        {
            return _cariDenganKode(kodeGerbang);
        }



    }
}