﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PemesananTiketPesawat.Models;
using Microsoft.EntityFrameworkCore;

namespace PemesananTiketPesawat.Services
{
    public class KursiPesawatService : BaseService //class JenisPesawatService terdeverisasi dari class BaseService
    {
        public List<Models.KursiPesawatModel> _kursiPesawat { get; set; }

        public KursiPesawatService(PemesananTiketPesawatDBContext db) : base(db) //memanggil konstruktor db pada class basis
        {
            //_bandaras = new List<Models.BandaraModel>();
        }

        //metode untuk mengalihkan ke database
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection(); // var _dbConn adalah   
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE airport;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        //validasidata
        private void ValidasiData(string statusKursi, string kodePenerbangan)

        {

            if (string.IsNullOrEmpty(statusKursi))
            {
                throw new Exception("status_kursi_kosong");
            }

            if (string.IsNullOrEmpty(kodePenerbangan))
            {
                throw new Exception("kode_penerbangan_kosong");
            }

        }

        private void aturNilai(ref Models.KursiPesawatModel _kursipesawat, string statusKursi, string kodePenerbangan) //untuk refrensi menggunakan 
                                //objek baru yaitu _jadwal metode diatur menjadi private mungkin agar tidak                                                                //mempengaruhi metode diluarnya
        {

            _kursipesawat.StatusKursi = statusKursi;
            _kursipesawat.KodePenerbangan = kodePenerbangan;              
        }

        public string AddKursiPesawat( string statusKursi, string kodePenerbangan) //belum tahu kenapa long
        {
            this.ValidasiData(statusKursi, kodePenerbangan); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var _kursipesawat = new Models.KursiPesawatModel(); //buat objek baru _jadwal


            aturNilai(ref _kursipesawat, statusKursi, kodePenerbangan); //panggil aturnilai

            _db.KursiPesawats.Add(_kursipesawat);
            _db.SaveChanges();

            return _kursipesawat.NomorKursi;
        }

        private Models.KursiPesawatModel findByNomorKursi(string nomorKursi)
        {
            
            if (string.IsNullOrEmpty(nomorKursi))
            {
                throw new ArgumentNullException("nomor_kursi_kosong");
            }
            var _fKursipesawat = _db.KursiPesawats?.Where(x => x.NomorKursi.ToLower().Equals(nomorKursi)).FirstOrDefault();
            //var _fAirport = _db.Airports?.Where(x => x.KodeAiport.ToLower().Equals(kodeAirport)).FirstOrDefault();
            
            if (_fKursipesawat == null)
            {
                throw new NullReferenceException("kursi_pesawat_not_found");
            }
            return _fKursipesawat;
        }

        public Models.KursiPesawatModel FindByNomorKursi(string nomorKursi)
        {
            return findByNomorKursi(nomorKursi);
          }


    }


}
