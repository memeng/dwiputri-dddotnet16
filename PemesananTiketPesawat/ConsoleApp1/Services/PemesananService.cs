﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PemesananTiketPesawat.Models;
using Microsoft.EntityFrameworkCore;
using System.Globalization;


namespace PemesananTiketPesawat.Services
{
    public class PemesananService : BaseService //class JenisPesawatService terdeverisasi dari class BaseService
    {
        public List<Models.PemesananModel> _pemesanans { get; set; }

        private CultureInfo _cultureInfo;

        public PemesananService(PemesananTiketPesawatDBContext db) : base(db) //memanggil konstruktor db pada class basis
        {
            _cultureInfo = CultureInfo.InvariantCulture; //ini namanya apa??
        }

        //metode untuk mengalihkan ke database
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection(); // var _dbConn adalah   
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE airport;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

         private DateTime[] parseTglJamKeberangkatanKedatangan(string tanggalKeberangakatan, string tanggalKedatangan,
            string jamKeberangkatan, string jamKedatangan, string durasi)

        {
            DateTime[] hasil = new DateTime[5]; //mencipatakan objek hasil

            hasil[0] = DateTime.ParseExact(tanggalKeberangakatan, "yyyy-MM-dd", _cultureInfo);
            hasil[1] = DateTime.ParseExact(tanggalKedatangan, "yyyy-MM-dd", _cultureInfo);
            hasil[2] = DateTime.ParseExact(jamKeberangkatan, "hh:mm", _cultureInfo);
            hasil[3] = DateTime.ParseExact(jamKedatangan, "hh:mm", _cultureInfo);
            hasil[4] = DateTime.ParseExact(durasi, "hh:mm", _cultureInfo);

            return hasil;
        }

        //validasidata
        private void ValidasiData(string daerahAsalBandara, string namaBandaraAsal, string daerahTujuanBandara, string namaBandaraTujuan, 
            string kodeMaskapai, string namaMaskapai,string durasi, string metodePembayaran, int hargaTiketPerOrang, 
            int jumlahOrang, int totalHargaTiket, string statusPemesanan, string kodePembayaran, string kodeCheckIn, string kodePenerbangan,
           string tanggalKeberangkatan, string tanggalKedatangan, string kodeCheckOut,
            string jamKeberangkatan, string jamKedatangan, int batasBagasi, int batasBagasiKabin)

        {
            if (string.IsNullOrEmpty(daerahAsalBandara))
            {
                throw new Exception("daerah_asal_bandara_kosong");
            }

            if (string.IsNullOrEmpty(namaBandaraAsal))
            {
                throw new Exception("nama_bandara_asal_kosong");
            }

            if (string.IsNullOrEmpty(daerahTujuanBandara))
            {
                throw new Exception("daerah_tujuan_bandara_kosong");
            }

            if (string.IsNullOrEmpty(namaBandaraTujuan))
            {
                throw new Exception("nama_bandara_tujuan_kosong");
            }

            if (string.IsNullOrEmpty(tanggalKeberangkatan))
            {
                throw new Exception("tanggal_keberangatan_kosong");
            }
            if (string.IsNullOrEmpty(tanggalKedatangan))
            {
                throw new Exception("tanggal_kedatangan_kosong");
            }
            if (string.IsNullOrEmpty(jamKeberangkatan))
            {
                throw new Exception("jam_keberangkatan_kosong");
            }
            if (string.IsNullOrEmpty(jamKedatangan))
            {
                throw new Exception("jam_kedatangan_kosong");
            }

            if (string.IsNullOrEmpty(kodeMaskapai))
            {
                throw new Exception("kode_maskapai_kosong");
            }

            if (string.IsNullOrEmpty(namaMaskapai))
            {
                throw new Exception("nama_maskapai_kosong");
            }

            if (string.IsNullOrEmpty(durasi))
            {
                throw new Exception("durasi_kosong");
            }
            if (batasBagasi < 0)
            {
                throw new Exception("batas_bagasi_harus_lebih_besar_atau_sama_dengan_nol");
            }
            if (batasBagasiKabin < 0)
            {
                throw new Exception("batas_bagasi_kabin_harus_lebih_besar_atau_sama_dengan_nol");
            }

            if (string.IsNullOrEmpty(metodePembayaran))
            {
                throw new Exception("kode_penerbangan_kosong");
            }

            if (hargaTiketPerOrang < 0)
            {
                throw new Exception("harga_harus_lebih_besar_dari_nol");
            }

            if (jumlahOrang < 0)
            {
                throw new Exception("jumlah_orang_harus_lebih_besar_dari_nol");
            }
            if (totalHargaTiket < 0)
            {
                throw new Exception("total_harus_lebih_besar_dari_nol");
            }
            if (string.IsNullOrEmpty(statusPemesanan))
            {
                throw new Exception("status_pemesanan_kosong");
            }
            if (string.IsNullOrEmpty(kodePembayaran))
            {
                throw new Exception("kode_pembayaran_kosong");
            }
            if (string.IsNullOrEmpty(kodeCheckIn))
            {
                throw new Exception("kode_checkin_kosong");
            }
            if (string.IsNullOrEmpty(kodeCheckOut))
            {
                throw new Exception("kode_checkout_kosong");
            }

            if (string.IsNullOrEmpty(kodePenerbangan))
            {
                throw new Exception("kode_penerbangan_kosong");
            }
        
        }

        private void validasiID(long kodePemesanan)
        {
            if (kodePemesanan == 0)
            {
                throw new Exception("invalid_kode_pemesanan");
            }
        }

        private void aturNilai(ref Models.PemesananModel _pemesanan, 
            string daerahBandaraAsal, string namaBandaraAsal, string daerahBandaraTujuan, 
            string namaBandaraTujuan, string kodeMaskapai, string namaMaskapai, DateTime durasi, string metodePembayaran,
            int hargaTiketPerOrang, int jumlahOrang, int totalHargaTiket, string statusPemesanan, string kodePembayaran,
            string kodeCheckIn, string kodePenerbangan,
           DateTime tanggalKeberangkatan, DateTime tanggalKedatangan,
            DateTime jamKeberangkatan, DateTime jamKedatangan, int batasBagasi, 
            int batasBagasiKabin, string kodeCheckOut) //untuk refrensi menggunakan 
                                                                                         //objek baru yaitu _jadwal metode diatur menjadi private mungkin agar tidak                                                                //mempengaruhi metode diluarnya
        {
            _pemesanan.DaerahBandaraAsal = daerahBandaraAsal;
            _pemesanan.NamaBandaraAsal = namaBandaraAsal;
            _pemesanan.DaerahBandaraTujuan = daerahBandaraTujuan;
            _pemesanan.NamaBandaraTujuan = namaBandaraTujuan;
            _pemesanan.KodeMaskapai = kodeMaskapai;
            _pemesanan.NamaMaskapai = namaMaskapai;
            _pemesanan.Durasi = durasi;
            _pemesanan.MetodePembayaran = metodePembayaran;
            _pemesanan.HargaTiketPerOrang = hargaTiketPerOrang;
            _pemesanan.JumlahOrang = jumlahOrang;
            _pemesanan.TotalHargaTiket = totalHargaTiket;
            _pemesanan.StatusPemesanan = statusPemesanan;
            _pemesanan.KodePembayaran = kodePembayaran;
            _pemesanan.KodeCheckIn = kodeCheckIn;
            _pemesanan.KodeCheckOut = kodeCheckOut;
            _pemesanan.KodePenerbangan= kodePenerbangan;
           
            _pemesanan.TanggalKeberangkatan = tanggalKeberangkatan;
            _pemesanan.TanggalKedatangan = tanggalKedatangan;
            _pemesanan.JamKeberangkatan = jamKeberangkatan;
            _pemesanan.JamKedatangan = jamKedatangan;
            _pemesanan.BatasBagasi = batasBagasi;
            _pemesanan.BatasBagasiKabin = batasBagasiKabin;
        }

        
        public long AddPemesanan(string daerahBandaraAsal, string namaBandaraAsal, string daerahBandaraTujuan, string namaBandaraTujuan,
            string kodeMaskapai, string namaMaskapai, string durasi, string metodePembayaran, int hargaTiketPerOrang,
            int jumlahOrang, int totalHargaTiket, string statusPemesanan, string kodePembayaran, string kodeCheckIn, string kodePenerbangan,
           string tanggalKeberangkatan, string tanggalKedatangan, string kodeCheckOut,
            string jamKeberangkatan, string jamKedatangan, int batasBagasi, int batasBagasiKabin) //belum tahu kenapa long
        {
            this.ValidasiData(daerahBandaraAsal, namaBandaraAsal, daerahBandaraTujuan, namaBandaraTujuan,
            kodeMaskapai, namaMaskapai, durasi, metodePembayaran, hargaTiketPerOrang,
             jumlahOrang, totalHargaTiket, statusPemesanan, kodePembayaran, kodeCheckIn, kodePenerbangan,
           tanggalKeberangkatan, tanggalKedatangan, kodeCheckOut,
           jamKeberangkatan, jamKedatangan, batasBagasi, batasBagasiKabin); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var dt = parseTglJamKeberangkatanKedatangan(tanggalKeberangkatan,
                   jamKeberangkatan,
                   tanggalKedatangan,
                   jamKedatangan,
                   durasi);


            DateTime _tanggalKeberangkatan = dt[0];
            DateTime _tanggalKedatangan = dt[1];
            DateTime _jamKeberangkatan = dt[2];
            DateTime _jamKedatangan = dt[3];
            DateTime _durasi = dt[4];

            var _pemesanan = new Models.PemesananModel(); //buat objek baru _jadwal

            _pemesanan.KodePemesanan = Helpers.IDHelper.ID(); //belum tahu ini apa

            aturNilai(ref _pemesanan,  daerahBandaraAsal, namaBandaraAsal,  daerahBandaraTujuan,
            namaBandaraTujuan,  kodeMaskapai,  namaMaskapai,  _durasi,  metodePembayaran,
             hargaTiketPerOrang,  jumlahOrang,  totalHargaTiket,  statusPemesanan,  kodePembayaran,
             kodeCheckIn,  kodePenerbangan,
            _tanggalKeberangkatan,  _tanggalKedatangan,
             _jamKeberangkatan,  _jamKedatangan,  batasBagasi,
             batasBagasiKabin,  kodeCheckOut); //panggil aturnilai

            _db.Pemesanans.Add(_pemesanan);
            _db.SaveChanges();

            return _pemesanan.KodePemesanan;
        }
        

        public long UpdatePemesanan(long kodePemesanan,
            string daerahBandaraAsal, string namaBandaraAsal, string daerahBandaraTujuan, string namaBandaraTujuan,
            string kodeMaskapai, string namaMaskapai, string durasi, string metodePembayaran, int hargaTiketPerOrang,
            int jumlahOrang, int totalHargaTiket, string statusPemesanan, string kodePembayaran, string kodeCheckIn, string kodePenerbangan,
           string tanggalKeberangkatan, string tanggalKedatangan, string kodeCheckOut,
            string jamKeberangkatan, string jamKedatangan, int batasBagasi, int batasBagasiKabin)
        {
            this.ValidasiData(daerahBandaraAsal, namaBandaraAsal, daerahBandaraTujuan, namaBandaraTujuan,
            kodeMaskapai, namaMaskapai, durasi, metodePembayaran, hargaTiketPerOrang,
             jumlahOrang, totalHargaTiket, statusPemesanan, kodePembayaran, kodeCheckIn, kodePenerbangan,
           tanggalKeberangkatan, tanggalKedatangan, kodeCheckOut,
           jamKeberangkatan, jamKedatangan, batasBagasi, batasBagasiKabin); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var dt = parseTglJamKeberangkatanKedatangan(tanggalKeberangkatan,
                   jamKeberangkatan,
                   tanggalKedatangan,
                   jamKedatangan, durasi); //var dt dalam bentuk array untuk memanggil kembali parseTglJamKeberangkatanKedatangan
                                   //datetime 

            DateTime _tanggalKeberangkatan = dt[0];
            DateTime _tanggalKedatangan = dt[1];
            DateTime _jamKeberangkatan = dt[2];
            DateTime _jamKedatangan = dt[3];
            DateTime _durasi = dt[4];


            var _fpemesanan = _pemesanans.Where(x => x.KodePemesanan == kodePemesanan).FirstOrDefault();
            if (_fpemesanan == null)
            {
                throw new Exception("pemesanan_not_found");
            }
            var _pemesanan = new Models.PemesananModel();

            _pemesanan.KodePemesanan = Helpers.IDHelper.ID(); //belum tahu ini apa

            //atur niali dengan memanggil aturnilai dengan beberapa variabel yg datatipenya berubah
            aturNilai(ref _pemesanan, daerahBandaraAsal, namaBandaraAsal, daerahBandaraTujuan, namaBandaraTujuan,
            kodeMaskapai, namaMaskapai, _durasi, metodePembayaran, hargaTiketPerOrang,
             jumlahOrang, totalHargaTiket, statusPemesanan, kodePembayaran, kodeCheckIn, kodePenerbangan,
           _tanggalKeberangkatan, _tanggalKedatangan, 
           _jamKeberangkatan, _jamKedatangan, batasBagasi, batasBagasiKabin, kodeCheckOut);

            return _pemesanan.KodePemesanan;
        }

        public void HapusPemesanan(long kodePemesanan)
        {
            validasiID(kodePemesanan);

            var _fpemesanan = CariPemesananBerdasarkanKode(kodePemesanan);
            if (_fpemesanan != null)
            {
                // _jadwals.Add(_jadwal);

                _pemesanans.Remove(_fpemesanan);
            }
        }

        public Models.PemesananModel? CariPemesananBerdasarkanKode(long kodePemesanan)
        {
            /*
             * LINQ
             * First => temukan yang pertama, jika tidak ketemu error
             * FirstOrDefault => temukan yang pertama, jika tidak ketemu return null             
             */
            return _pemesanans.Where(x => x.KodePemesanan == kodePemesanan).FirstOrDefault();
        }

        public List<Models.PemesananModel> DapatkanJadwal()
        {
            return _pemesanans;
        }
    }


}
