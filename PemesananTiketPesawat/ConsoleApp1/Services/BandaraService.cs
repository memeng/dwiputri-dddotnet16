﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PemesananTiketPesawat.Models;
using Microsoft.EntityFrameworkCore;


namespace PemesananTiketPesawat.Services
{
    public class BandaraService : BaseService
    {
        //sambungkan dulu dengan modelnya
        public List<Models.BandaraModel> _bandaras { get; set; }

        //buat konstruktor kelasnya
        public BandaraService(PemesananTiketPesawatDBContext db) : base(db)
        {
            //_bandaras = new List<Models.BandaraModel>();
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE airport;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }


        //validasidata
        private void ValidasiData(string namaBandara, string provinsiBandara, string kotaBandara)

        {
            if (string.IsNullOrEmpty(namaBandara))
            {
                throw new Exception("nama_Bandara_kosong");
            }

            if (string.IsNullOrEmpty(provinsiBandara))
            {
                throw new Exception("nama_provinsi_bandara_kosong");
            }
            if (string.IsNullOrEmpty(kotaBandara))
            {
                throw new Exception("nama_kota_Bandara_kosong");
            }

        }


        private void aturNilai(ref Models.BandaraModel _bandara,
            string namaBandara, string provinsiBandara, string kotaBandara) //untuk refrensi menggunakan 
                                                                            //objek baru yaitu _jadwal metode diatur menjadi private mungkin agar tidak                                                                //mempengaruhi metode diluarnya
        {
            _bandara.NamaBandara = namaBandara;
            _bandara.ProvinsiBandara = provinsiBandara;
            _bandara.KotaBandara = kotaBandara;

        }

        public string AddBandara(string namaBandara, string provinsiBandara, string kotaBandara) //belum tahu kenapa long
        {
            this.ValidasiData(namaBandara, provinsiBandara, kotaBandara); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var _bandara = new Models.BandaraModel(); //buat objek baru _jadwal


            aturNilai(ref _bandara, namaBandara, provinsiBandara, kotaBandara); //panggil aturnilai

            //_bandaras.Add(_bandara); //menambahkan element p11111111111111ada list

            _db.Bandaras.Add(_bandara);
            _db.SaveChanges();

            return _bandara.KodeBandara;
        }

        private Models.BandaraModel findByKodeAirport(string kodeBandara)
        {
            /*  if(string.IsNullOrEmpty(kodeAirport))
            {
                throw new ArgumentNullException("kode_airport_is_null");
            }
            var _fAirport = _db.Airports?.Where(x => x.KodeAiport.ToLower().Equals(kodeAirport)).FirstOrDefault();
            if(_fAirport == null)
            {
                throw new NullReferenceException("airport_not_found");
            }
            return _fAirport;*/
            if (string.IsNullOrEmpty(kodeBandara))
            {
                throw new ArgumentNullException("kode_bandara_kosong");
            }
            var _fBandara = _db.Bandaras?.Where(x => x.KodeBandara.ToLower().Equals(kodeBandara)).FirstOrDefault();
            //var _fAirport = _db.Airports?.Where(x => x.KodeAiport.ToLower().Equals(kodeAirport)).FirstOrDefault();
            if (_fBandara == null)
            {
                throw new NullReferenceException("kode_Bandara_not_found");
            }
            return _fBandara;
        }

        public Models.BandaraModel FindAirportByKodeAirport(string kodeBandara)
        {
            return findByKodeAirport(kodeBandara);
        }



    }
}