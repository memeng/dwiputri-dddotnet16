﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PemesananTiketPesawat.Services
{
    public class KalkulatorService
    {
        public int Tambah(int a, int b)
        {
            return a + b;
        }

        public int Kurang(int a, int b)
        {
            return a - b;
        }

        public float Bagi(int a, int b)
        {
            return (float)a / (float)b;
        }

        public int Kali(int a, int b)
        {
            return a * b;
        }

    }
}
