﻿using Xunit;
using PemesananTiketPesawat.Services;
using PemesananTiketPesawat.Models;
using System.Collections.Generic;
using System;


namespace PemesananTiketPesawat.Test
{
    public class GerbangBandaraServisTes
    {
        [Fact]
        public void TestGerbangBandara_PositiveTest()
        {


            using (var _db = new Models.PemesananTiketPesawatDBContext(TestVars.ConnectionString))
            {
                GerbangBandaraService gerbangBandaraServis = new GerbangBandaraService(_db);
                gerbangBandaraServis.Migrate();



                string _kodeBandara = "CGK";
                string _nomorGerbang = "7";
                string _nomorPintu = "2";

                var _idGateway = gerbangBandaraServis.AddGerbang(_kodeBandara, _nomorGerbang, _nomorPintu);

                var _fAirportGateway = gerbangBandaraServis.cariDenganKode(_idGateway);
                Assert.NotNull(_fAirportGateway);
                if (_fAirportGateway != null)
                {
                    Assert.Equal(_kodeBandara, _fAirportGateway.KodeBandara);
                    Assert.Equal(_nomorGerbang, _fAirportGateway.NomorGerbang);
                    Assert.Equal(_nomorPintu, _fAirportGateway.NomorPintu);

                }

            }
        }
    }
}
