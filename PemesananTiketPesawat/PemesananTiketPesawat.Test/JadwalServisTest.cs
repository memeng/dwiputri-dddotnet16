using Xunit;
using System;
using System.Collections.Generic;
using PemesananTiketPesawat.Models;
using PemesananTiketPesawat.Services;

namespace PemesananTiketPesawat.Test
{
    public class JadwalServisTest
    {
        [Fact]
        public void TestJadwalService_NegativeTest()
        {
            //buat objeknya dulu
            JadwalService _jadwalservis = new JadwalService();

            //tambahkan objek baru dengan nilai salah
            string _kodeMaskapai = "";
            string _kodePenerbangan = "";
            string _kodeBandaraAsal = "";
            string _kodeBandaraTujuan = "";
            string _tanggalKeberangkatan = "";
            string _jamKeberangkatan = "";
            string _tanggalKedatangan = "";
            string _jamKedatangan = "";
            int _batasBagasi = -1;
            int _batasBagasiKabin = -1;

            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("kode_maskapai_kosong", ex.Message);
            }

            _kodeMaskapai = "LR/LNI";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("kode_penerbangan_kosong", ex.Message);
            }

            _kodePenerbangan = "JT19";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("kode_Bandara_Asal_kosong", ex.Message);
            }

            _kodeBandaraAsal = "PKU098";

            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("kode_Bandara_Tujuan_kosong", ex.Message);
            }

            _kodeBandaraTujuan = "SSQ001";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("tanggal_keberangKatan_kosong", ex.Message);
            }

            _tanggalKeberangkatan = "2022 - 02 - 24";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("tanggal_kedatangan_kosong", ex.Message);
            }

            _tanggalKedatangan = " 2022 - 02 - 24";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("jam_keberangKatan_Invalid", ex.Message);
            }

            _jamKeberangkatan = "08:00";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("jam_kedatangan_kosong", ex.Message);
            }

            _jamKedatangan = "08:00";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("batas_bagasi_harus_lebih_besar_atau_sama_dengan_nol", ex.Message);
            }

            _batasBagasi = 20;

            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("batas_bagasi_kabin_harus_lebih_besar_atau_sama_dengan_nol", ex.Message);
            }

            _batasBagasiKabin = 10;
            _tanggalKeberangkatan = "2022-02-3";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("tanggal_keberangkatan_invalid", ex.Message);
            }

            _tanggalKedatangan = "2022-02-4";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("tanggal_kedatangan_invalid", ex.Message);
            }

            _jamKeberangkatan = "25:00";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("jam_keberangkatan_invalid", ex.Message);
            }

            _jamKedatangan = "26:00";
            try
            {
                _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            }
            catch (Exception ex)
            {
                Assert.Equal("jam_kedatangan_invalid", ex.Message);
            }
        }
            //selanjutnya positive test
            [Fact]
            public void TestTambahSchedule_PositiveTest()
            {


                JadwalService _jadwalservis = new JadwalService();


                string _kodeMaskapai = "LR/LNI";
                string _kodePenerbangan = "JT619";
                string _kodeBandaraAsal = "CGK";
                string _kodeBandaraTujuan = "DPS";
                string _tanggalKeberangkatan = "2022-02-24";
                string _jamKeberangkatan = "08:00";
                string _tanggalKedatangan = "2022-02-24";
                string _jamKedatangan = "10:00";
                int _batasBagasi = 20;
                int _batasBagasiKabin = 7;


                var _kodeJadwal = _jadwalservis.AddJadwal(_kodeMaskapai, _kodePenerbangan, _kodeBandaraAsal,
            _kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, _batasBagasi, _batasBagasiKabin);

            Models.JadwalModel? _fjadwal = _jadwalservis.CariJadwalBerdasarkanKode(_kodeJadwal);
                Assert.NotNull(_fjadwal);

                if (_fjadwal != null)
                {
                    Assert.Equal(_kodeMaskapai, _fjadwal.KodeMaskapai);
                    Assert.Equal(_kodePenerbangan, _fjadwal.KodePenerbangan);
                    Assert.Equal(_kodeBandaraAsal, _fjadwal.KodeBandaraAsal);
                    Assert.Equal(_kodeBandaraTujuan, _fjadwal.KodeBandaraTujuan);

                    Assert.Equal(_tanggalKeberangkatan, _fjadwal.TanggalKeberangkatan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKeberangkatan, _fjadwal.JamKeberangkatan.ToString("hh:mm"));

                    Assert.Equal(_tanggalKedatangan, _fjadwal.TanggalKedatangan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKedatangan, _fjadwal.JamKedatangan.ToString("hh:mm"));

                    Assert.Equal(_batasBagasi, _fjadwal.BatasBagasi);
                    Assert.Equal(_batasBagasiKabin, _fjadwal.BatasBagasiKabin);
                }


                List<JadwalModel> _jadwals = _jadwalservis.DapatkanJadwal();
                Assert.Equal(1, _jadwals.Count);


                _jamKeberangkatan = "10:00";
                _jamKedatangan = "12:00";

                _jadwalservis.UpdateJadwal(_kodeJadwal,
                    _kodeMaskapai,
                    _kodePenerbangan,
                    _kodeBandaraAsal,
                    _kodeBandaraTujuan,
                    _tanggalKeberangkatan,
                    _jamKeberangkatan,
                    _tanggalKedatangan,
                    _jamKedatangan,
                    _batasBagasi,
                    _batasBagasiKabin);

                _fjadwal = _jadwalservis.CariJadwalBerdasarkanKode(_kodeJadwal);
                Assert.NotNull(_fjadwal);
                if (_fjadwal != null)
                {
                Assert.Equal(_kodeMaskapai, _fjadwal.KodeMaskapai);
                Assert.Equal(_kodePenerbangan, _fjadwal.KodePenerbangan);
                Assert.Equal(_kodeBandaraAsal, _fjadwal.KodeBandaraAsal);
                Assert.Equal(_kodeBandaraTujuan, _fjadwal.KodeBandaraTujuan);

                Assert.Equal(_tanggalKeberangkatan, _fjadwal.TanggalKeberangkatan.ToString("yyyy-MM-dd"));
                Assert.Equal(_jamKeberangkatan, _fjadwal.JamKeberangkatan.ToString("hh:mm"));

                Assert.Equal(_tanggalKedatangan, _fjadwal.TanggalKedatangan.ToString("yyyy-MM-dd"));
                Assert.Equal(_jamKedatangan, _fjadwal.JamKedatangan.ToString("hh:mm"));

                Assert.Equal(_batasBagasi, _fjadwal.BatasBagasi);
                Assert.Equal(_batasBagasiKabin, _fjadwal.BatasBagasiKabin);
            }

                const string SCHEDULE_NOT_FOUND = "schedule_not_found";
                _jadwalservis.HapusJadwal(_kodeJadwal);
                try
                {
                    _fjadwal = _jadwalservis.CariJadwalBerdasarkanKode(_kodeJadwal);
                }
                catch (Exception e)
                {
                    Assert.Equal(SCHEDULE_NOT_FOUND, e.Message);
                }

                //2

                _kodeMaskapai = "ID/BTK";
                _kodePenerbangan = "BTK123";
                _kodeBandaraAsal = "CGK";
                _kodeBandaraTujuan = "DPS";
                _tanggalKeberangkatan = "2022-02-25";
                _jamKeberangkatan = "09:00";
                _tanggalKedatangan = "2022-02-25";
                _jamKedatangan = "10:30";
                _batasBagasi = 20;
                _batasBagasiKabin = 7;


                _kodeJadwal = _jadwalservis.AddJadwal(_kodeMaskapai,
                    _kodePenerbangan,
                    _kodeBandaraAsal,
                    _kodeBandaraTujuan,
                    _tanggalKeberangkatan,
                    _jamKeberangkatan,
                    _tanggalKedatangan,
                    _jamKedatangan,
                    _batasBagasi,
                    _batasBagasiKabin);


            _fjadwal = _jadwalservis.CariJadwalBerdasarkanKode(_kodeJadwal);
            Assert.NotNull(_fjadwal);
            if (_fjadwal != null)
            {
                Assert.Equal(_kodeMaskapai, _fjadwal.KodeMaskapai);
                Assert.Equal(_kodePenerbangan, _fjadwal.KodePenerbangan);
                Assert.Equal(_kodeBandaraAsal, _fjadwal.KodeBandaraAsal);
                Assert.Equal(_kodeBandaraTujuan, _fjadwal.KodeBandaraTujuan);

                Assert.Equal(_tanggalKeberangkatan, _fjadwal.TanggalKeberangkatan.ToString("yyyy-MM-dd"));
                Assert.Equal(_jamKeberangkatan, _fjadwal.JamKeberangkatan.ToString("hh:mm"));

                Assert.Equal(_tanggalKedatangan, _fjadwal.TanggalKedatangan.ToString("yyyy-MM-dd"));
                Assert.Equal(_jamKedatangan, _fjadwal.JamKedatangan.ToString("hh:mm"));

                Assert.Equal(_batasBagasi, _fjadwal.BatasBagasi);
                Assert.Equal(_batasBagasiKabin, _fjadwal.BatasBagasiKabin);
            }

            _jadwals = _jadwalservis.DapatkanJadwal();
                Assert.Equal(1, _jadwals.Count);



            }
        
    }
}