﻿using Xunit;
using PemesananTiketPesawat.Services;
using PemesananTiketPesawat.Models;
using System.Collections.Generic;
using System;


namespace PemesananTiketPesawat.Test
{
    public class BandaraServiceTest
    {
        [Fact]
        public void TesBandaraService_PositiveTest()
        {
            using (var _db = new Models.PemesananTiketPesawatDBContext(TestVars.ConnectionString))
            {
                BandaraService _bandaraService = new BandaraService(_db);
                _bandaraService.Migrate();

                string _namaBandara = "Sultan Syarif Qasim II";
                string _provinsiBandara = "Riau";
                string _kotaBandara = "Pekanbaru";

                var _kodeBandara = _bandaraService.AddBandara(_namaBandara, _provinsiBandara, _kotaBandara);

                var _fAirportGateway = _bandaraService.FindAirportByKodeAirport(_kodeBandara);
                Assert.NotNull(_fAirportGateway);
                if (_fAirportGateway != null)
                {
                    Assert.Equal(_namaBandara, _fAirportGateway.NamaBandara);
                    Assert.Equal(_provinsiBandara, _fAirportGateway.ProvinsiBandara);
                    Assert.Equal(_kotaBandara, _fAirportGateway.KotaBandara);

                }

            }
        }
    }
}
