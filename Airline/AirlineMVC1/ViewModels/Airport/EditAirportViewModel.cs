﻿using System.ComponentModel.DataAnnotations;

namespace AirlineMVC.ViewModels.Airport
{
    public class EditAirportViewModel
    {

        [Display(Name = "Kode Bandara")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Kode Bandara harus diisi")]
        public string KodeAirport { get; set; }


        [Display(Name = "Nama Bandara")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Nama Bandara harus diisi")]
        public string NamaAirport { get; set; }

        [Display(Name = "Propinsi")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Propinsi harus diisi")]
        public string Propinsi { get; set; }

        [Display(Name = "Kota")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Kota harus diisi")]
        public string Kota { get; set; }

        public EditAirportViewModel() { }
        public EditAirportViewModel(string kodeAirport,
          string namaAiport,
          string propinsi,
          string kota)
        {
            this.KodeAirport = kodeAirport;
            this.NamaAirport = namaAiport;
            this.Propinsi = propinsi;
            this.Kota = kota;
        }
    }
}
