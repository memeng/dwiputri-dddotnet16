﻿using System.ComponentModel.DataAnnotations;

namespace AirlineMVC.ViewModels.Airport
{
    public class ListAirportViewModel
    {
        [Display(Name = "Kode Bandara")]
        public string KodeAirport { get; set; }

        [Display(Name = "Nama Bandara")]
        public string NamaAirport { get; set; }

        [Display(Name = "Propinsi")]
        public string Propinsi { get; set; }

        [Display(Name = "Kota")]
        public string Kota { get; set; }

        public ListAirportViewModel() { }

        public ListAirportViewModel(string kodeAirport,
          string namaAiport,
          string propinsi,
          string kota)
        {
            this.KodeAirport = kodeAirport;
            this.NamaAirport = namaAiport;
            this.Propinsi = propinsi;
            this.Kota = kota;
        }
    }
}
