﻿using Microsoft.AspNetCore.Mvc;
using Airline;
using AirlineMVC;

namespace AirlineMVC.Controllers
{

    public class AirportController : BaseController
    {

        static List<ViewModels.Airport.ListAirportViewModel> _airports = new List<ViewModels.Airport.ListAirportViewModel>();
        private ILogger<AirportController>? _logger;
        private Airline.Services.AirportService _airportService;
        public AirportController(ILogger<AirportController> logger,
            Airline.Services.AirportService airportService)

        {
            _logger = logger;
            _airportService = airportService;

            if (_airports.Count == 0)
            {
                _airports.Clear();
                _airports.Add(new ViewModels.Airport.ListAirportViewModel("CGK", "SOEKARNO-HATTA", "BANTEN", "TANGERANG"));
                _airports.Add(new ViewModels.Airport.ListAirportViewModel("DPS", "NGURAH RAI", "BALI", "DENPASAR"));
            }


        }
        public async Task<IActionResult> Index()
        {
            if (_isLogin())
            {
                var _airports = _airportService.FindList();
                List<ViewModels.Airport.ListAirportViewModel> _listAirportModels = new List<ViewModels.Airport.ListAirportViewModel>();
                foreach (var m in _airports)
                {
                    _listAirportModels.Add(new ViewModels.Airport.ListAirportViewModel()
                    {
                        KodeAirport = m.KodeAiport,
                        NamaAirport = m.NamaAirport,
                        Kota = m.Kota,
                        Propinsi = m.Propinsi,
                    });
                }

                return View("Index", _listAirportModels);
            }

            return RedirectToAction("Login", "Login");

        }

        public async Task<IActionResult> Edit(string id)
        {
            if (_isLogin())
            {
                var _listAirport = _airports.Where(x => x.KodeAirport == id).FirstOrDefault();
                if (_listAirport == null)
                {
                    return NotFound();
                }

                var _editAirport = new ViewModels.Airport.EditAirportViewModel()
                {
                    KodeAirport = _listAirport.KodeAirport,
                    NamaAirport = _listAirport.NamaAirport,
                    Kota = _listAirport.Kota,
                    Propinsi = _listAirport.Propinsi
                };

                return View("Edit", _editAirport);
            }

            return RedirectToAction("Login", "Login");


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id,
            [Bind("KodeAirport,NamaAirport,Kota,Propinsi")] ViewModels.Airport.EditAirportViewModel editAirportViewModel)
        {
            if (_isLogin())
            {

                if (id != editAirportViewModel.KodeAirport)
                {
                    return NotFound();
                }


                try
                {
                    if (ModelState.IsValid)
                    {
                        var _fAirport = _airports.Where(x => x.KodeAirport == id).FirstOrDefault();
                        if (_fAirport != null)
                        {
                            _fAirport.NamaAirport = editAirportViewModel.NamaAirport;
                            _fAirport.Propinsi = editAirportViewModel.Propinsi;
                            _fAirport.Kota = editAirportViewModel.Kota;
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            throw new Exception("Airport not found");
                        }
                    }

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }

                return View("Edit", editAirportViewModel);
            }

            return RedirectToAction("Login", "Login");

        }

    }
}
