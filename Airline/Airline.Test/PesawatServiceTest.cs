﻿using Xunit;
using Airline.Services;
using Airline.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;


namespace Airline.Test
{
    public class PesawatServiceTest
    {
        [Fact]
        public void TestPesawat_PositiveTest()
        {

            var dbOption = new DbContextOptionsBuilder<AirlineDbContext>()
               .UseSqlServer(TestVars.ConnectionString)
               .Options;

            using (var _db = new Models.AirlineDbContext(dbOption))
            {
                PesawatService _pesawatService = new PesawatService(_db);
                _pesawatService.Migrate();

                string _kodePenerbangan = "aaa";
                string _jenisPesawat = "bbb";
                string _kodeMaskapai = "ccc";


                _pesawatService.AddPesawat(_kodePenerbangan, _jenisPesawat, _kodeMaskapai);

                var _fPesawat = _pesawatService.FindByKodePenerbangan(_kodePenerbangan);
                Assert.NotNull(_fPesawat);
                if (_fPesawat != null)
                {
                    Assert.Equal(_kodePenerbangan, _fPesawat.KodePenerbangan);
                    Assert.Equal(_jenisPesawat, _fPesawat.KodeJenisPesawat);
                    Assert.Equal(_kodeMaskapai, _fPesawat.KodeMaskapai);
                }

            }
        }


        [Fact]
        public void TestAirport_SeedingAirports()
        {
            var dbOption = new DbContextOptionsBuilder<AirlineDbContext>()
              .UseSqlServer(TestVars.ConnectionString)
              .Options;

            using (var _db = new Models.AirlineDbContext(dbOption))
            {

                PesawatService _pesawatService = new PesawatService(_db);
                _pesawatService.Migrate();

                Models.PesawatModel[] _pesawats = new Models.PesawatModel[]
                {
                    new PesawatModel("aaa", "bbb", "ccc"),
                    new PesawatModel("DPS", "Ngurah Rai", "Bali"),
                    new PesawatModel("SUB", "Juanda", "Jawa Timur"),
                   
                };

                for (int i = 0; i < _pesawats.Length; i++)
                {
                    var _pesawat = _pesawats[i];
                    var _kodePenerbangan = _pesawat.KodePenerbangan;
                    var _jenisPesawat = _pesawat.KodeJenisPesawat;
                    var _kodeMaskapai = _pesawat.KodeMaskapai;


                    _pesawatService.AddPesawat(_kodePenerbangan, _jenisPesawat, _kodeMaskapai);

                    var _fPesawat = _pesawatService.FindByKodePenerbangan(_kodePenerbangan);
                    Assert.NotNull(_fPesawat);
                    if (_fPesawat != null)
                    {
                        Assert.Equal(_kodePenerbangan, _fPesawat.KodePenerbangan);
                        Assert.Equal(_jenisPesawat, _fPesawat.KodeJenisPesawat);
                        Assert.Equal(_kodeMaskapai, _fPesawat.KodeMaskapai);
                    }
                }



            }

        }

    }
}
