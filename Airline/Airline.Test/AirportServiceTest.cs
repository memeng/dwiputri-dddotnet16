﻿using Xunit;
using Airline.Services;
using Airline.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;


namespace Airline.Test
{
    public class AirportServiceTest
    {
        [Fact]
        public void TestAirport_PositiveTest()
        {

            var dbOption = new DbContextOptionsBuilder<AirlineDbContext>()
               .UseSqlServer(TestVars.ConnectionString)
               .Options;

            using (var _db = new Models.AirlineDbContext(dbOption))
            {
                AirportService _airportService = new AirportService(_db);
                _airportService.Migrate();

                string _kodeAirport = "CGK";
                string _namaAirport = "Soekarno Hatta";
                string _propinsi = "Banten";
                string _kota = "Tangerang";

                _airportService.AddAirport(_kodeAirport, _namaAirport, _propinsi, _kota);

                var _fAirport = _airportService.FindAirportByKodeAirport(_kodeAirport);
                Assert.NotNull(_fAirport);
                if (_fAirport != null)
                {
                    Assert.Equal(_kodeAirport, _fAirport.KodeAiport);
                    Assert.Equal(_namaAirport, _fAirport.NamaAirport);
                    Assert.Equal(_propinsi, _fAirport.Propinsi);
                    Assert.Equal(_kota, _fAirport.Kota);
                }

            }
        }


        [Fact]
        public void TestAirport_SeedingAirports()
        {
            var dbOption = new DbContextOptionsBuilder<AirlineDbContext>()
              .UseSqlServer(TestVars.ConnectionString)
              .Options;

            using (var _db = new Models.AirlineDbContext(dbOption))
            {

                AirportService _airportService = new AirportService(_db);
                _airportService.Migrate();

                Models.AirportModel[] _aiports = new Models.AirportModel[]
                {
                    new AirportModel("CGK", "Soekarnoa-Hatta", "Banten", "Tangerang"),
                    new AirportModel("DPS", "Ngurah Rai", "Bali", "Denpasar"),
                    new AirportModel("SUB", "Juanda", "Jawa Timur", "Sidoarjo"),
                    new AirportModel("SOC", "Adisumarmo", "Jawa Tengah", "Boyolali Regency"),
                };

                for (int i = 0; i < _aiports.Length; i++)
                {
                    var _airport = _aiports[i];
                    var _kodeAirport = _airport.KodeAiport;
                    var _namaAirport = _airport.NamaAirport;
                    var _propinsi = _airport.Propinsi;
                    var _kota = _airport.Kota;

                    _airportService.AddAirport(_kodeAirport, _namaAirport, _propinsi, _kota);

                    var _fAirport = _airportService.FindAirportByKodeAirport(_kodeAirport);
                    Assert.NotNull(_fAirport);
                    if (_fAirport != null)
                    {
                        Assert.Equal(_kodeAirport, _fAirport.KodeAiport);
                        Assert.Equal(_namaAirport, _fAirport.NamaAirport);
                        Assert.Equal(_propinsi, _fAirport.Propinsi);
                        Assert.Equal(_kota, _fAirport.Kota);
                    }
                }



            }

        }

    }
}
