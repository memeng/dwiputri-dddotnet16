﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.SqlServer;

namespace Airline.Models
{
    public class AirlineDbContext : DbContext
    {

        /*tables*/
        public DbSet<ScheduleModel> Schedules { get; set; }
        public DbSet<AirportModel> Airports { get; set; }
        public DbSet<AirportGatewayModel> AirportGateways { get; set; }
        public DbSet<BookingModel> Bookings { get; set; }
        public DbSet<BookingPenumpangModel> BookingPenumpangs { get; set; }
      
       public DbSet<JenisPesawatModel> JenisPesawats { get; set; }

        public DbSet<KursiPesawatModel> KursiPesawats { get; set; }

        public DbSet<MaskapaiModel> Maskapaies { get; set; }

        public DbSet<PesawatModel> Pesawats { get; set; }

        /*constructors*/
        public AirlineDbContext(DbContextOptions<AirlineDbContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        private string _connString;
        public AirlineDbContext(string connectionString)
        {
            _connString = connectionString;
        }

        /*Configurasi DbContext*/
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ScheduleModel>();
            modelBuilder.Entity<AirportModel>();
            modelBuilder.Entity<AirportGatewayModel>();
            modelBuilder.Entity<JenisPesawatModel>();
            modelBuilder.Entity<BookingModel>();
            modelBuilder.Entity<BookingPenumpangModel>();
            modelBuilder.Entity<KursiPesawatModel>();
            modelBuilder.Entity<MaskapaiModel>();
            modelBuilder.Entity<PesawatModel>();
        }

        public class AirlineDbContextFactory : IDesignTimeDbContextFactory<AirlineDbContext>
        {
            public AirlineDbContext CreateDbContext(string[] args)
            {
                var optionsBuilder = new DbContextOptionsBuilder<AirlineDbContext>();
                optionsBuilder.UseSqlServer("Server = localhost\\SQLEXPRESS; DataBase = AIRLINE_BOOKING; user id = sa; Password = 123456;");

                return new AirlineDbContext(optionsBuilder.Options);

            }
        }

    }
}
