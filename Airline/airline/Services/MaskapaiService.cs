﻿using Airline.Models;
using Microsoft.EntityFrameworkCore;

namespace Airline.Services
{
    public class MaskapaiService : BaseService
    {
        public MaskapaiService(AirlineDbContext db) : base(db)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE maskapai;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }


        private void validateBase(string kodeMaskapai,
            string namaMaskapai)
        {
            if (string.IsNullOrEmpty(kodeMaskapai))
            {
                throw new Exception("kode_maskapai_kosong");
            }
            if (string.IsNullOrEmpty(namaMaskapai))
            {
                throw new Exception("nama_maskapai_kosong");
            }
        }

        private void setValue(ref Models.MaskapaiModel _maskapai,
            string kodeMaskapai,
            string namaMaskapai)
        {

            _maskapai.KodeMaskapai = kodeMaskapai;

            _maskapai.NamaMaskapai = namaMaskapai;

        }

        public void AddMaskapai(string kodeMaskapai,
            string namaMaskapai)
        {

            validateBase(kodeMaskapai, namaMaskapai);

            var _maskapai = new Models.MaskapaiModel();

            setValue(ref _maskapai, kodeMaskapai, namaMaskapai);

            _db.Maskapaies.Add(_maskapai);

            _db.SaveChanges();

        }

        private Models.MaskapaiModel findByKodeMaskapai(string kodeMaskapai)
        {
            if (string.IsNullOrEmpty(kodeMaskapai))
            {
                throw new ArgumentNullException("kode_maskapai_is_null");
            }

            var _fMaskapai = _db.Maskapaies?.Where(x => x.KodeMaskapai.ToLower().Equals(kodeMaskapai)).FirstOrDefault();
            if (_fMaskapai == null)
            {
                throw new NullReferenceException("maskapai_not_found");
            }
            return _fMaskapai;
        }

        public Models.MaskapaiModel FindMaskapaiByKodeMaskapai(string kodeMaskapai)
        {
            return findByKodeMaskapai(kodeMaskapai);
        }

    }
}
