﻿using System.Globalization;
using Microsoft.EntityFrameworkCore;

namespace Airline.Services
{  
    public class ScheduleService : BaseService
    {

        private CultureInfo _cultureInfo;


        /*Constructors*/
        public ScheduleService(Models.AirlineDbContext db) : base(db)
        {
            _cultureInfo = CultureInfo.InvariantCulture;
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE schedule;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();

        }

        private DateTime[] parseTglJamKeberangkatanKedatangan(string tglKeberangkatan,
            string jamKeberangkatan,
            string tglKedatangan,
            string jamKedatangan)
        {
            DateTime[] result = new DateTime[4];

            //_tglKeberangkatan
            //_jamKeberangkatan
            //_tglKedatangan
            //_jamKedatangan
            result[0] = DateTime.ParseExact(tglKeberangkatan, "yyyy-MM-dd", _cultureInfo);
            result[1] = DateTime.ParseExact(jamKeberangkatan, "hh:mm tt", _cultureInfo);
            result[2] = DateTime.ParseExact(tglKedatangan, "yyyy-MM-dd", _cultureInfo);
            result[3] = DateTime.ParseExact(jamKedatangan, "hh:mm tt", _cultureInfo);

            return result;
        }

        private void validateBase(string kodeMaskapai,
            string kodePenerbangan,
            string kodeAirportAsal,
            string kodeAirportTujuan,
            string tglKeberangkatan,
            string jamKeberangkatan,
            string tglKedatangan,
            string jamKedatangan,
            int batasBagasi,
            int batasBagasiKabin)
        {

            if (string.IsNullOrEmpty(kodeMaskapai))
            {
                throw new Exception("kode_maskapai_kosong");
            }

            if (string.IsNullOrEmpty(kodePenerbangan))
            {
                throw new Exception("kode_penerbangan_kosong");
            }
            if (string.IsNullOrEmpty(kodeAirportAsal))
            {
                throw new Exception("kode_airport_asal_kosong");
            }
            if (string.IsNullOrEmpty(kodeAirportTujuan))
            {
                throw new Exception("kode_airport_tujuan_kosong");
            }

            if (string.IsNullOrEmpty(tglKeberangkatan))
            {
                throw new Exception("tgl_keberangkatan_kosong");
            }
            if (string.IsNullOrEmpty(jamKeberangkatan))
            {
                throw new Exception("jam_keberangkatan_kosong");
            }

            if (string.IsNullOrEmpty(tglKedatangan))
            {
                throw new Exception("tgl_kedatangan_kosong");
            }
            if (string.IsNullOrEmpty(jamKedatangan))
            {
                throw new Exception("jam_kedatangan_kosong");
            }

            if (batasBagasi < 0)
            {
                throw new Exception("batas_bagasi_harus_lebih_besar_atau_sama_dengan_nol");
            }
            if (batasBagasiKabin < 0)
            {
                throw new Exception("batas_bagasi_kabin_harus_lebih_besar_atau_sama_dengan_nol");
            }

        }

        private void validateID(long scheduleID)
        {
            if (scheduleID == 0)
            {
                throw new Exception("invalid_schedule_id");
            }
        }

        private void setValue(ref Models.ScheduleModel _schedule,
            string kodeMaskapai,
            string kodePenerbangan,
            string kodeAirportAsal,
            string kodeAirportTujuan,
            DateTime tglKeberangkatan,
            DateTime jamKeberangkatan,
            DateTime tglKedatangan,
            DateTime jamKedatangan,
            int batasBagasi,
            int batasBagasiKabin)
        {
            _schedule.KodeMaskapai = kodeMaskapai;
            _schedule.KodePenerbangan = kodePenerbangan;
            _schedule.KodeAirportAsal = kodeAirportAsal;
            _schedule.KodeAirportTujuan = kodeAirportTujuan;
            _schedule.TglKeberangkatan = tglKeberangkatan;
            _schedule.JamKeberangkatan = jamKeberangkatan;
            _schedule.TglKedatangan = tglKedatangan;
            _schedule.JamKedatangan = jamKedatangan;
            _schedule.BatasBagasi = batasBagasi;
            _schedule.BatasBagasiKabin = batasBagasiKabin;
        }


        public long TambahSchedule(string kodeMaskapai,
            string kodePenerbangan,
            string kodeAirportAsal,
            string kodeAirportTujuan,
            string tglKeberangkatan,
            string jamKeberangkatan,
            string tglKedatangan,
            string jamKedatangan,
            int batasBagasi,
            int batasBagasiKabin)
        {

            this.validateBase(kodeMaskapai,
                    kodePenerbangan,
                    kodeAirportAsal,
                    kodeAirportTujuan,
                    tglKeberangkatan,
                    jamKeberangkatan,
                    tglKedatangan,
                    jamKedatangan,
                    batasBagasi,
                    batasBagasiKabin);

            var dt = parseTglJamKeberangkatanKedatangan(tglKeberangkatan,
                    jamKeberangkatan,
                    tglKedatangan,
                    jamKedatangan);

            DateTime _tglKeberangkatan = dt[0];
            DateTime _jamKeberangkatan = dt[1];
            DateTime _tglKedatangan = dt[2];
            DateTime _jamKedatangan = dt[3];

            var _schedule = new Models.ScheduleModel();
            _schedule.ScheduleID = Helpers.IDHelpers.ID();

            setValue(ref _schedule,
                kodeMaskapai,
                    kodePenerbangan,
                    kodeAirportAsal,
                    kodeAirportTujuan,
                    _tglKeberangkatan,
                    _jamKeberangkatan,
                    _tglKedatangan,
                    _jamKedatangan,
                    batasBagasi,
                    batasBagasiKabin);

            /*_schedules.Add(_schedule);*/

            _db.Schedules.Add(_schedule);
            _db.SaveChanges();

            return _schedule.ScheduleID;

        }

        public void UpdateSchedule(long scheduleID,
            string kodeMaskapai,
            string kodePenerbangan,
            string kodeAirportAsal,
            string kodeAirportTujuan,
            string tglKeberangkatan,
            string jamKeberangkatan,
            string tglKedatangan,
            string jamKedatangan,
            int batasBagasi,
            int batasBagasiKabin)
        {
            validateID(scheduleID);

            this.validateBase(kodeMaskapai,
                  kodePenerbangan,
                  kodeAirportAsal,
                  kodeAirportTujuan,
                  tglKeberangkatan,
                  jamKeberangkatan,
                  tglKedatangan,
                  jamKedatangan,
                  batasBagasi,
                  batasBagasiKabin);

            var dt = parseTglJamKeberangkatanKedatangan(tglKeberangkatan,
                 jamKeberangkatan,
                 tglKedatangan,
                 jamKedatangan);

            DateTime _tglKeberangkatan = dt[0];
            DateTime _jamKeberangkatan = dt[1];
            DateTime _tglKedatangan = dt[2];
            DateTime _jamKedatangan = dt[3];

            var _fSchedule = findByID(scheduleID);

            setValue(ref _fSchedule,
                kodeMaskapai,
                    kodePenerbangan,
                    kodeAirportAsal,
                    kodeAirportTujuan,
                    _tglKeberangkatan,
                    _jamKeberangkatan,
                    _tglKedatangan,
                    _jamKedatangan,
                    batasBagasi,
                    batasBagasiKabin);

            _db.SaveChanges();

        }

        public void DeleteSchedule(long scheduleID)
        {
            validateID(scheduleID);

            var _fSchedule = findByID(scheduleID);
            if (_fSchedule != null)
            {
                _db.Schedules.Remove(_fSchedule);
                _db.SaveChanges();
            }
        }

        public Models.ScheduleModel? findByID(long scheduleID)
        {
            //ini apa sih??
            var _fSchedule = _db.Schedules.Where(x => x.ScheduleID == scheduleID).FirstOrDefault();
            if (_fSchedule == null)
            {
                throw new Exception("schedule_not_found");
            }
            return _fSchedule;
        }


        public Models.ScheduleModel? FindScheduleByID(long scheduleID)
        {
            return findByID(scheduleID);
        }

        public List<Models.ScheduleModel> GetSchedules()
        {
            return _db.Schedules.ToList();
        }


    }
}
