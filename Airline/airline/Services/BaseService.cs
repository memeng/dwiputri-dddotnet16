﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airline.Services
{
    public class BaseService
    {
        protected Models.AirlineDbContext _db;
        public BaseService(Models.AirlineDbContext db)
        {
            _db = db;
        }
    }
}
