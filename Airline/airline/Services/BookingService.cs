﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
namespace Airline.Services
{
    public class BookingService : BaseService
    {
        //metode
        public BookingService(Models.AirlineDbContext db) : base(db) { }
        
        public void Migrate()
        {
            var _dbconn = _db.Database.GetDbConnection();
            _dbconn.Open();
            var _cmd = _dbconn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE booking";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();

            _cmd = _dbconn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE booking_penumpang;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();

            _dbconn.Close();
        }

        private string createKodeBooking()
        {
            return string.Format("%x", Helpers.IDHelpers.ID());
        }

        public long NewBooking(long scheduleID)
        {
            try
            {
                var _schedule = _db.Schedules
                    .Include(x => x.AirportAsal)
                    .Include(x => x.AirportTujuan)
                    .FirstOrDefault(s => s.ScheduleID == scheduleID);

                if (_schedule == null)
                {
                    throw new Exception("schedule_not_found");
                }

                var _booking = new Models.BookingModel();
                _booking.KodeBooking = createKodeBooking();
                _booking.KodeAirportAsal = _schedule.KodeAirportAsal;

            }

            catch (Exception ex)

            {

            }

            return 0;
        }   
    }
}
