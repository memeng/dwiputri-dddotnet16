﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airline.Models;
using Microsoft.EntityFrameworkCore;

namespace Airline.Services
{
    public class JenisPesawatService : BaseService
    {
        public JenisPesawatService(AirlineDbContext db) :base(db)
        {

        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "Truncate TABLE jenis_pesawat;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        private void validateBase(string kodeJenisPesawat, int tahunPesawat)
        {
            if(string.IsNullOrEmpty(kodeJenisPesawat))
            {
                throw new Exception("kode_jenis_pesawat_kosong");
            }
            if(tahunPesawat <= 0)
            {
                throw new Exception("tahun_pesawat_must_be_greater_than_zero");

            }               
        }

        private void setValue(ref Models.JenisPesawatModel _jenis_pesawat,
            string kodeJenisPesawat,
            int tahunPesawat)
        {
            _jenis_pesawat.KodeJenisPesawat = kodeJenisPesawat;
            _jenis_pesawat.TahunPesawat = tahunPesawat;

        }

        public void AddJenisPesawat(string kodeJenisPesawat, int tahunPesawat)
        {
            validateBase(kodeJenisPesawat, tahunPesawat);

            var _jenis_pesawat = new Models.JenisPesawatModel();

            setValue(ref _jenis_pesawat, kodeJenisPesawat, tahunPesawat);
            _db.JenisPesawats.Add(_jenis_pesawat);
            _db.SaveChanges();
        }

        private Models.JenisPesawatModel findByKodeJenisPesawat(string kodeJenisPesawat)
        {
            if(string.IsNullOrEmpty(kodeJenisPesawat))
            {
                throw new ArgumentNullException("kode_jenis_pesawat_kosong");
            }

            var _fJenisPesawat = _db.JenisPesawats?.Where(x => x.KodeJenisPesawat.ToLower().Equals(kodeJenisPesawat)).FirstOrDefault();
            if(_fJenisPesawat == null)
            {
                throw new NullReferenceException("jenis_pesawat_not_found");
            }
            return _fJenisPesawat;
        }

        public Models.JenisPesawatModel FindJenisPesawatByKodeJenisPesawat(string kodeJenisPesawat)
        {
            return findByKodeJenisPesawat(kodeJenisPesawat);
        }
            }
}
