﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
    [Table("maskapai")]
    public class MaskapaiModel
    {

        [Key]
        [Column("kode_maskapai", TypeName = "varchar(30)")]
        public string KodeMaskapai { get; set; }

        [Column("nama_maskapai", TypeName = "varchar(60)")]
        public string NamaMaskapai { get; set; }

        public MaskapaiModel() { }

        public MaskapaiModel(string kodeMaskapai, string namaMaskapai)
        {
            this.KodeMaskapai = kodeMaskapai;
            this.NamaMaskapai = namaMaskapai;
        }
    }
}
