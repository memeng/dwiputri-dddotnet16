﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
    [Table("booking_penumpang")]
    public class BookingPenumpangModel
    {
        [Key]
        [Column("ktp", TypeName = "varchar(30)")]
        public string KTP { get; set; }

        [Column("kode_booking", TypeName = "varchar(5)")]
        public string KodeBooking { get; set; }

        [Column("title", TypeName = "varchar(3)")]
        public string Title { get; set; }

        [Column("nama_penumpang", TypeName = "varchar(60)")]
        public string NamaPenumpang { get; set; }

        [Column("nomor_kursi", TypeName = "varchar (5)")]
        public string NomorKursi { get; set; }



    }
}
