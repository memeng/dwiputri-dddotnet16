﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
        [Table("jenis_pesawat")]
        public class JenisPesawatModel
        {

            [Key]
            [Column("kode_jenis_pesawat", TypeName = "varchar(30)")]
            public string KodeJenisPesawat { get; set; }

            [Column("tahun_pesawat", TypeName = "int")]
            public int TahunPesawat { get; set; }
            

            //why??? kok dipanggil objekny>>
            public JenisPesawatModel() { }

            public JenisPesawatModel(string kodeJenisPesawat,
                int tahunPesawat)
            {

                this.KodeJenisPesawat = kodeJenisPesawat;
                this.TahunPesawat = tahunPesawat;

            }
        }
}

