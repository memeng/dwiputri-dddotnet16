﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
    [Table("schedule")]
    public class ScheduleModel
    {
        [Key]
        [Column("schedule_id", TypeName = "bigint")]
        public long ScheduleID { get; set; }

        [Column("kode_maskapai", TypeName = "varchar(30)")]
        public string KodeMaskapai { get; set; }

        [Column("kode_penerbangan", TypeName = "varchar(30)")]
        public string KodePenerbangan { get; set; }

        [Column("kode_airport_asal", TypeName = "varchar(30)")]
        public string KodeAirportAsal { get; set; }

        [Column("kode_airport_tujuan", TypeName = "varchar(30)")]
        public string KodeAirportTujuan { get; set; }

        [Column("tgl_keberangkatan", TypeName ="datetime")]
        public DateTime TglKeberangkatan { get; set; }

        [Column("jam_keberangkatan", TypeName = "datetime")]
        public DateTime JamKeberangkatan { get; set; }

        [Column("tgl_kedatangan", TypeName ="datetime")]
        public DateTime TglKedatangan { get; set; }

        [Column("jam_kedatangan", TypeName ="datetime")]
        public DateTime JamKedatangan { get; set; }

        [Column("batas_bagasi", TypeName = "int")]
        public int BatasBagasi { get; set; }

        [Column("batas_bagasi_kabin", TypeName ="int")]
        public int BatasBagasiKabin { get; set; }

        [ForeignKey("KodeAirportAsal")]
        public AirportModel AirportAsal { get; set; }

        [ForeignKey("KodeAirportTujuan")]
        public AirportModel AirportTujuan { get; set; }

        [NotMapped]
        public string StrTglKeberangkatan { get; set; }
        
        [NotMapped]
        public string StrTglKedatangan { get; set; }

        public ScheduleModel() { }
        public ScheduleModel(string kodeMaskapai,
            string kodePenerbangan,
            string kodeAirportAsal, 
            string kodeAirportTujuan,
            string tglKeberangkatan,
            string tglKedatangan,
            int batasBagasi,
            int batasBagasiKabin)
        {
            this.KodeMaskapai = kodeMaskapai;
            this.KodePenerbangan = kodePenerbangan;
            this.KodeAirportAsal = kodeAirportAsal;
            this.KodeAirportTujuan = kodeAirportTujuan;
            this.StrTglKeberangkatan = tglKeberangkatan;
            this.StrTglKedatangan = tglKedatangan;
            this.BatasBagasi = batasBagasi;
            this.BatasBagasiKabin = batasBagasiKabin;

        }
    }
}
