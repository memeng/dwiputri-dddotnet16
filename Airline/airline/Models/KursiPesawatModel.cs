﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
    [Table("kursi_pesawat")]
    public class KursiPesawatModel
    {
        [Key]
        [Column("id_kursi_pesawat", TypeName = "bigint")]
        /*[DatabaseGenerated(DatabaseGeneratedOption.Identity)]*/
        public long IDKursiPesawat { get; set; } //PK

        [Column("kode_penerbangan", TypeName = "varchar(30)")]
        public string KodePenerbangan { get; set; } //FK

        [Column("nomor_kursi", TypeName = "varchar(5)")]
        public string NomorKursi { get; set; }

        //kok dibikin metodenya?/
        public KursiPesawatModel() { }

        public KursiPesawatModel(string kodePenerbangan,
            string nomorKursi)
        {
            this.KodePenerbangan = kodePenerbangan;
            this.NomorKursi = nomorKursi;

        }

    }
}
