﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
    [Table("airport")]
    public class AirportModel
    {
        [Key]
        [Column("kode_airport", TypeName = "varchar(30)")]
        public string KodeAiport { get; set; }

        [Column("nama_airport", TypeName = "varchar(60)")]
        public string NamaAirport { get; set; }

        [Column("propinsi", TypeName = "varchar(30)")]
        public string Propinsi { get; set; }

        [Column("kota", TypeName ="varchar(30)")]
        public string Kota { get; set; }

        public AirportModel() { }

        public AirportModel(string kodeAirport,
            string namaAirport,
            string propinsi,
            string kota)
        {
            this.KodeAiport = kodeAirport;
            this.NamaAirport = namaAirport;
            this.Propinsi = propinsi;
            this.Kota = kota;  
        }
    }
}
