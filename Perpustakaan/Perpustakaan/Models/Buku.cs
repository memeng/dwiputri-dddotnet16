﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.Models
{
    
    
    public class Buku
    {
        //variable inisiasinya
        public string judul { get; set; }

        public int rak { get; set; }

        //untuk describe kata-kata dihalaman  
        public string kata { get; set; }
        public int halaman { get; set; }
         
        //konstruktornya
        public Buku(string Judul, int Rak, string Kata, int Halaman)
        {
            judul = Judul;
            rak = Rak;
            kata = Kata;
            halaman = Halaman;
        }


    }

    
}
