﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.Services
{
    public class BukuServices
    {
        public List<Models.Buku> _Buku = new List<Models.Buku>();

        public List <Models.Buku> Bukus
        {
            get
            {
                return _Buku;
            }
        }

        public void Buku1(string judul, int rak, string kata, int halaman)
        {
            _Buku.Add(new Models.Buku ( judul, rak, kata, halaman));
        }

        public void Buku2(string judul, int rak, string kata, int halaman)
        {
            _Buku.Add(new Models.Buku(judul, rak, kata, halaman));
        }

        public Models.Buku[] SearchKata(string inputan)
        {
            List<Models.Buku> results = new List<Models.Buku>();
            for (int i = 0; i < _Buku.Count; i++)
            {
                if (_Buku[i].kata.ToLower().Contains(inputan.ToLower()) ||
                    _Buku[i].kata.ToLower().Contains(inputan.ToLower()))

                {
                    results.Add(_Buku[i]);
                }
            }
            return results.ToArray();
        }

    }
}
