﻿using System;
using System.Globalization;
using System.Text;
class latihanData
{
    // buat judulnya

    static string judul()
    {
        return "DAFTAR KARYAWAN";
    }

    static string makeLine(int length, string character, bool newline)
    {
        StringBuilder garisJudul = new StringBuilder();
        for (int i = 0; i < length; i++)
        {
            garisJudul.Append(character);
        }
        if (newline)
        {
            garisJudul.Append("\n");
        }
        return garisJudul.ToString();
    }

    const int ALIGN_LEFT = 1;
    const int ALIGN_RIGHT = 2;

    const int EMPLOYEE_CODE_LENGTH = 20;
    const int EMPLOYEE_NAME_LENGTH = 20;
    const int SALARY_LENGTH = 20;

    int totalLength = EMPLOYEE_CODE_LENGTH + EMPLOYEE_NAME_LENGTH + SALARY_LENGTH + 3;

    static string field(int length, string value, int align, char emptySpace,
       string endDelimiter)
    {
        if (align == ALIGN_LEFT)
        {
            return value.PadRight(length, emptySpace) + endDelimiter;

        }
        else if (align == ALIGN_RIGHT)
        {
            return value.PadLeft(length, emptySpace) + endDelimiter;
        }
        return "";
    }
    static void listEmployeesHeader()
    {
        string[] fields = new string[]
        {
            field(EMPLOYEE_CODE_LENGTH, "Employee Code", ALIGN_LEFT, ' ', "|"),
            field(EMPLOYEE_NAME_LENGTH, "Employee Name", ALIGN_LEFT, ' ', "|"),
            field(SALARY_LENGTH, "Salary", ALIGN_RIGHT, ' ', "|"),
        };

        int totalLength = EMPLOYEE_CODE_LENGTH + EMPLOYEE_NAME_LENGTH + SALARY_LENGTH + 3;

        Console.Write(makeLine(totalLength, "=", true));
        //Console.Write(getListTitle());
        Console.Write(makeLine(totalLength, "=", true));
        //Console.Write(row(fields));
        Console.Write(makeLine(totalLength, "=", true));

    }

    static void Main(string[] args)
    {
        static string getListTitle()
        {
            return "DAFTAR KARYAWAN\n";
        }

        static string makeLine(int length, string character, bool newLine)
        {
            StringBuilder line = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                line.Append(character);
            }
            if (newLine)
            {
                line.Append("\n");
            }
            return line.ToString();
        }

        const int ALIGN_LEFT = 1;
        const int ALIGN_RIGHT = 2;

        const int EMPLOYEE_CODE_LENGTH = 20;
        const int EMPLOYEE_NAME_LENGTH = 20;
        const int SALARY_LENGTH = 20;

        static string field(int length, string value, int align, char emptySpace,
            string endDelimiter)
        {
            if (align == ALIGN_LEFT)
            {
                return value.PadRight(length, emptySpace) + endDelimiter;

            }
            else if (align == ALIGN_RIGHT)
            {
                return value.PadLeft(length, emptySpace) + endDelimiter;
            }
            return "";
        }

        static string row(string[] fields)
        {
            StringBuilder _row = new StringBuilder();
            foreach (string field in fields)
            {
                _row.Append(field);
            }
            return _row.Append("\n").ToString();
        }

        static void listEmployeesHeader()
        {
            string[] fields = new string[]
            {
            field(EMPLOYEE_CODE_LENGTH, "Employee Code", ALIGN_LEFT, ' ', "|"),
            field(EMPLOYEE_NAME_LENGTH, "Employee Name", ALIGN_LEFT, ' ', "|"),
            field(SALARY_LENGTH, "Salary", ALIGN_RIGHT, ' ', "|"),
            };

            int totalLength = EMPLOYEE_CODE_LENGTH + EMPLOYEE_NAME_LENGTH + SALARY_LENGTH + 3;

            Console.Write(makeLine(totalLength, "=", true));
            Console.Write(getListTitle());
            Console.Write(makeLine(totalLength, "=", true));
            Console.Write(row(fields));
            Console.Write(makeLine(totalLength, "=", true));

        }
        static void listEmployees(List<string> employeeCodes,
            List<string> employeeNames,
            List<decimal> salaries,
            List<string> currencies
        )
        {

            listEmployeesHeader();

            for (int i = 0; i < employeeCodes.Count; i++)
            {
                string[] fields = new string[]
                {
                field(EMPLOYEE_CODE_LENGTH, employeeCodes[i], ALIGN_LEFT, ' ', "|"),
                field(EMPLOYEE_NAME_LENGTH, employeeNames[i], ALIGN_LEFT, ' ', "|"),
                field(SALARY_LENGTH, salaries[i].ToString("#,###") + " " + currencies[i], ALIGN_RIGHT, ' ', "|"),
                };

                Console.Write(row(fields));

            }

        }

        const int ACTION_ADD_NEW_EMPLOYEE = 1;
        const int ACTION_EDIT_EMPLOYEE = 2;
        const int ACTION_DELETE_EMPLOYEE = 3;
        const int ACTION_EXIT = 4;

        static void actionMenus()
        {
            Console.WriteLine("Actions:");
            Console.WriteLine("1. Add New Employee");
            Console.WriteLine("2. Edit Employee");
            Console.WriteLine("3. Delete Employee");
            Console.WriteLine("4. Exit");

        }

        static string inputEmployeeCode()
        {
            string employeeCode = "";
            while (true)
            {
                Console.Write("Masukan Employee Code :");
                employeeCode = Console.ReadLine();
                if (employeeCode.Length > 0)
                {
                    break;
                }
            }
            return employeeCode;
        }

        static string inputEmployeeName()
        {
            string employeeName = "";
            while (true)
            {
                Console.Write("Masukan Employee Name :");
                employeeName = Console.ReadLine();
                if (employeeName.Length > 0)
                {
                    break;
                }
            }
            return employeeName;

        }

        static decimal inputSalary()
        {
            string strSalary = "";
            decimal salary = 0;
            while (true)
            {
                Console.Write("Masukan Salary :");
                strSalary = Console.ReadLine();
                if (strSalary.Length > 0)
                {
                    try
                    {
                        salary = Convert.ToDecimal(strSalary);
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Salary harus angka!");
                    }
                }
            }
            return salary;

        }
        static string inputCurrency()
        {
            string currency = "";
            while (true)
            {
                Console.Write("Masukan Currency :");
                currency = Console.ReadLine();
                if (currency.Length > 0)
                {
                    break;
                }
            }
            return currency;

        }

        static int inputAction()
        {
            string input = Console.ReadLine();
            return int.Parse(input);
        }
        static void conditionalAndLoop()
        {

            List<string> employeeCodes = new List<string>();
            List<string> employeeNames = new List<string>();
            List<decimal> salaries = new List<decimal>();
            List<string> currencies = new List<string>();



            /*List<DateTime> _checkInsByEmployee = new List<DateTime>();
            Dictionary<string, List<DateTime>> checkIns = new Dictionary<string, List<DateTime>>();
            checkIns.Add("E001", _checkInsByEmployee);

            List<DateTime> _checkOutsByEmployee = new List<DateTime>();
            Dictionary<string, List<DateTime>> checkOuts = new Dictionary<string, List<DateTime>>();
            checkOuts.Add("E001", _checkOutsByEmployee);


            foreach (var checkIn in checkIns)
            {
                string employeeCode = checkIn.Key;
                List<DateTime> times = checkIn.Value;
            }*/


            //Add to List
            /*employeeCodes.Add("E001");
            employeeCodes.Add("E002");
            employeeCodes.Add("E003");

            employeeNames.Add("Faisal");
            employeeNames.Add("Dwi Putri");
            employeeNames.Add("Vido");

            salaries.Add(5000000);
            salaries.Add(5500000);
            salaries.Add(600000);

            currencies.Add("IDR");
            currencies.Add("IDR");
            currencies.Add("IDR");

            for (int i = 0; i < employeeCodes.Count; i++)
            {
                Console.WriteLine(employeeCodes[i]);
            }

            foreach (string employeeCode in employeeCodes)
            {
                Console.WriteLine(employeeCode);
            }*/


            bool loop = true;
            while (loop)
            {
                //Console.Clear();
                listEmployees(employeeCodes,
                       employeeNames,
                       salaries,
                       currencies);

                actionMenus();

                var action = inputAction();
                switch (action)
                {
                    case ACTION_ADD_NEW_EMPLOYEE:

                        var employeeCode = inputEmployeeCode();
                        var employeeName = inputEmployeeName();
                        var salary = inputSalary();
                        var currency = inputCurrency();

                        employeeCodes.Add(employeeCode);
                        employeeNames.Add(employeeName);
                        salaries.Add(salary);
                        currencies.Add(currency);

                        break;

                    case ACTION_EDIT_EMPLOYEE:
                        Console.WriteLine("Your Action is Edit Employee");
                        Console.ReadLine();
                        break;

                    case ACTION_DELETE_EMPLOYEE:
                        Console.WriteLine("Your Action is Delete Employee");
                        Console.ReadLine();
                        break;

                    case ACTION_EXIT:
                        loop = false;
                        break;

                }
            }
        }


            }
            static void dataTypes()
    {
        Console.Write(judul());
        //Console.Write(makeLine(totalLength, "=", true));
        //Console.Write(makeLine(totalLength, "=", true));
    }
        static void accountBalances ()
        {
            //Buat Transaction Date

            DateTime[] transactionDate = new DateTime[]
                {
                DateTime.Parse("2022-02-11"),
                DateTime.Parse("2022-02-12"),
                DateTime.Parse("2022-02-13"),
                };

            string[] customerCode = new string[]
                {
                "C001",
                "C002",
                "C003",
                };

            string[] customerName = new string[]
                {
                "Faisal",
                "Vido  ",
                "Susanto",
                 };

            decimal[] transactionAmount = new decimal[]
            {
            -500000,
            1000000,
            2000000,
            3000000,
            };

            decimal[] accountBalances = new decimal[]
           {
            transactionAmount[0] + transactionAmount [1],
            transactionAmount[0] + transactionAmount [2],
            transactionAmount[0] + transactionAmount [3],
           };
            //ini untuk buat kopnya
            for (int i = 0; i <= 100; i++)
            {
                Console.Write("=");
            }
            Console.WriteLine(" ");

            Console.WriteLine("Account Balance Histories");

            for (int i = 0; i <= 100; i++)
            {
                Console.Write("=");
            }
            Console.WriteLine(" ");

            Console.WriteLine("Transaction Date \t | Customer Code \t | Customer Name \t | Transaction Amount");

            for (int i = 0; i <= 100; i++)
            {
                Console.Write("=");
            }
            Console.WriteLine(" ");
            Console.WriteLine("{0} \t\t |{1} \t\t\t |{2} \t\t |{3} IDR\t\t ",
                    transactionDate[0].ToString("yyyy-MM-dd"),
                    customerCode[0],
                    customerName[0],
                    transactionAmount[1].ToString("#,###.00"));

            Console.WriteLine("{0} \t\t |{1} \t\t\t |{2} \t\t |{3} IDR\t\t ",
                  transactionDate[1].ToString("yyyy-MM-dd"),
                  customerCode[0],
                  customerName[0],
                  transactionAmount[0].ToString("#,###.00"));

            Console.WriteLine("{0} \t\t |{1} \t\t\t |{2}   \t\t |{3} IDR\t\t ",
                    transactionDate[0].ToString("yyyy-MM-dd"),
                    customerCode[1],
                    customerName[1],
                    transactionAmount[2].ToString("#,###.00"));

            Console.WriteLine("{0} \t\t |{1} \t\t\t |{2}   \t\t |{3} IDR\t\t ",
                    transactionDate[1].ToString("yyyy-MM-dd"),
                    customerCode[1],
                    customerName[1],
                    transactionAmount[0].ToString("#,###.00"));

            Console.WriteLine("{0} \t\t |{1} \t\t\t |{2} \t\t |{3} IDR\t\t ",
                    transactionDate[1].ToString("yyyy-MM-dd"),
                    customerCode[2],
                    customerName[2],
                    transactionAmount[3].ToString("#,###.00"));

            Console.WriteLine("{0} \t\t |{1} \t\t\t |{2} \t\t |{3} IDR\t\t ",
                    transactionDate[2].ToString("yyyy-MM-dd"),
                    customerCode[2],
                    customerName[2],
                    transactionAmount[0].ToString("#,###.00"));

            //buat account balance
            Console.WriteLine("\n \n");
            for (int i = 0; i <= 100; i++)
            {
                Console.Write("=");
            }
            Console.WriteLine(" ");

            Console.WriteLine("Account Balance Summary");

            for (int i = 0; i <= 100; i++)
            {
                Console.Write("=");
            }
            Console.WriteLine(" ");

            Console.WriteLine("Customer Code \t | Customer Name \t\t | Transaction Amount");

            //buat grouping nya
            Console.WriteLine("=======================================================================================");
            for (int i = 0; i <= customerCode.Length - 1; i++)
            {
                Console.WriteLine("{0} \t\t |{1} \t\t\t |{2} IDR\t\t ",
                     customerCode[i],
                     customerName[i],
                     accountBalances[i].ToString("#,###.00"));
            }
            Console.WriteLine("=======================================================================================");

        }

   // static void conditionalLoop()
    //{

      //}
}





