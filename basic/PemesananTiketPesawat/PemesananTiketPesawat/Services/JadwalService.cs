﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;


namespace PemesananTiketPesawat.Services
{
    public class JadwalService
    {
        //sambungkan dengan foldermodel dulu
        public List<Models.JadwalModel> _jadwals { get; set; } 

        private CultureInfo _cultureInfo;
        
        //buat konstruktor Jadwalservice
        public JadwalService()
        {
            _jadwals = new List<Models.JadwalModel>(); //ini objek class jadwal
            _cultureInfo = CultureInfo.InvariantCulture; //ini namanya apa??
        }

        //buat format datetime

        private DateTime[] parseTglJamKeberangkatanKedatangan(string tanggalKeberangakatan, string tanggalKedatangan,
            string jamKeberangkatan, string jamKedatangan)

        {
            DateTime[] hasil = new DateTime[4]; //mencipatakan objek hasil

            hasil[0] = DateTime.ParseExact(tanggalKeberangakatan, "yyyy-MM-dd", _cultureInfo);
            hasil[1] = DateTime.ParseExact(tanggalKedatangan, "yyyy-MM-dd", _cultureInfo);
            hasil[2] = DateTime.ParseExact(jamKeberangkatan, "hh:mm", _cultureInfo);
            hasil[3] = DateTime.ParseExact(jamKedatangan, "hh:mm", _cultureInfo);

            return hasil;
        }

        //untuk mencechek kevalidan variable primary key

        private void validateBase(string kodeMaskapai, string kodePenerbangan, string kodeBandaraAsal,
            string kodeBandaraTujuan, string tanggalKeberangkatan, string tanggalKedatangan,
            string jamKeberangkatan,
            string jamKedatangan, int batasBagasi, int batasBagasiKabin)
            
        {
            if(string.IsNullOrEmpty(kodeMaskapai))
            {
                throw new Exception("kode_maskapai_kosong"); 
            }

            if (string.IsNullOrEmpty(kodePenerbangan))
            {
                throw new Exception("kode_penerbangan_kosong");
            }
            if (string.IsNullOrEmpty(kodeBandaraAsal))
            {
                throw new Exception("kode_Bandara_Asal_kosong");
            }
            if (string.IsNullOrEmpty(kodeBandaraTujuan))
            {
                throw new Exception("kode_Bandara_Tujuan_kosong");
            }
            if (string.IsNullOrEmpty(tanggalKeberangkatan))
            {
                throw new Exception("tanggal_keberangatan");
            }
            if (string.IsNullOrEmpty(tanggalKedatangan))
            {
                throw new Exception("tanggal_kedatangan");
            }
            if (string.IsNullOrEmpty(jamKeberangkatan))
            {
                throw new Exception("jam_keberangkatan");
            }
            if (string.IsNullOrEmpty(jamKedatangan))
            {
                throw new Exception("jam_kedatangan");
            }
            if (batasBagasi < 0)
            {
                throw new Exception("batas_bagasi_harus_lebih_besar_atau_sama_dengan_nol");
            }
            if (batasBagasiKabin < 0)
            {
                throw new Exception("batas_bagasi_kabin_harus_lebih_besar_atau_sama_dengan_nol");
            }
        }

        //belum ngerti kenapa kodejadwal terpisah sendiri
        private void validasiID(long kodeJadwal)
        {
            if (kodeJadwal == 0)
            {
                throw new Exception("invalid_kode_jadwal");
            }
        }

        private void aturNilai(ref Models.JadwalModel _jadwal, string kodeMaskapai, string kodePenerbangan, string kodeBandaraAsal,
            string kodeBandaraTujuan, DateTime tanggalKeberangkatan, DateTime tanggalKedatangan, DateTime jamKeberangkatan,
            DateTime jamKedatangan, int batasBagasi, int batasBagasiKabin) //untuk refrensi menggunakan 
            //objek baru yaitu _jadwal metode diatur menjadi private mungkin agar tidak
            //mempengaruhi metode diluarnya
        {
            _jadwal.KodeMaskapai = kodeMaskapai;
            _jadwal.KodePenerbangan = kodePenerbangan;
            _jadwal.JamKeberangkatan = jamKeberangkatan;
            _jadwal.JamKedatangan = jamKedatangan;
            _jadwal.KodeBandaraAsal = kodeBandaraAsal;
            _jadwal.BatasBagasi = batasBagasi;
            _jadwal.BatasBagasiKabin = batasBagasiKabin;
            _jadwal.KodeBandaraTujuan = kodeBandaraTujuan;
            _jadwal.TanggalKeberangkatan = tanggalKeberangkatan;
            _jadwal.TanggalKedatangan = tanggalKedatangan;

        }

        //metode untuk menambah variabel jadwal dengan metode terparameterisasi
        public long AddJadwal(string kodeMaskapai, string kodePenerbangan, string kodeBandaraAsal,
            string kodeBandaraTujuan, string tanggalKeberangkatan, string tanggalKedatangan,
            string jamKeberangkatan,
            string jamKedatangan, int batasBagasi, int batasBagasiKabin) //belum tahu kenapa long
        {
            this.validateBase(kodeMaskapai, kodePenerbangan, kodeBandaraAsal,
           kodeBandaraTujuan, tanggalKeberangkatan, tanggalKedatangan, jamKeberangkatan,
           jamKedatangan, batasBagasi, batasBagasiKabin); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var dt = parseTglJamKeberangkatanKedatangan(tanggalKeberangkatan,
                   jamKeberangkatan,
                   tanggalKedatangan,
                   jamKedatangan); //var dt dalam bentuk array untuk memanggil kembali parseTglJamKeberangkatanKedatangan
                                       //datetime 

            DateTime _tanggalKeberangkatan = dt[0];
            DateTime _tanggalKedatangan = dt[1];
            DateTime _jamKeberangkatan = dt[2];
            DateTime _jamKedatangan = dt[3];

            var _jadwal = new Models.JadwalModel(); //buat objek baru _jadwal

            _jadwal.KodeJadwal = Helpers.IDHelper.ID(); //belum tahu ini apa

            aturNilai(ref _jadwal, kodeMaskapai, kodePenerbangan, kodeBandaraAsal,
            kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, batasBagasi, batasBagasiKabin); //panggil aturnilai
            
                _jadwals.Add(_jadwal); //menambahkan element pada list


            return _jadwal.KodeJadwal;
        }

        public long UpdateJadwal(long kodeJadwal,
            string kodeMaskapai, string kodePenerbangan, string kodeBandaraAsal,
          string kodeBandaraTujuan, string tanggalKeberangkatan, string tanggalKedatangan,
          string jamKeberangkatan,
          string jamKedatangan, int batasBagasi, int batasBagasiKabin)
        {
            this.validateBase(kodeMaskapai, kodePenerbangan, kodeBandaraAsal,
           kodeBandaraTujuan, tanggalKeberangkatan, tanggalKedatangan, jamKeberangkatan,
           jamKedatangan, batasBagasi, batasBagasiKabin); //penggunaan this untuk memanggil konstruktor didalam konstruktor

            var dt = parseTglJamKeberangkatanKedatangan(tanggalKeberangkatan,
                   jamKeberangkatan,
                   tanggalKedatangan,
                   jamKedatangan); //var dt dalam bentuk array untuk memanggil kembali parseTglJamKeberangkatanKedatangan
                                   //datetime 

            DateTime _tanggalKeberangkatan = dt[0];
            DateTime _tanggalKedatangan = dt[1];
            DateTime _jamKeberangkatan = dt[2];
            DateTime _jamKedatangan = dt[3];

            
            var _fjadwal = _jadwals.Where(x => x.KodeJadwal == kodeJadwal).FirstOrDefault();
            if (_fjadwal == null)
            {
                throw new Exception("schedule_not_found");
            }
            var _jadwal = new Models.JadwalModel();

            _jadwal.KodeJadwal = Helpers.IDHelper.ID(); //belum tahu ini apa

            //atur niali dengan memanggil aturnilai dengan beberapa variabel yg datatipenya berubah
            aturNilai(ref _jadwal, kodeMaskapai, kodePenerbangan, kodeBandaraAsal,
            kodeBandaraTujuan, _tanggalKeberangkatan, _tanggalKedatangan, _jamKeberangkatan,
             _jamKedatangan, batasBagasi, batasBagasiKabin);

            return _jadwal.KodeJadwal;
        }

        public void HapusJadwal(long kodeJadwal)
        {
            validasiID(kodeJadwal);

            var _fjadwal = CariJadwalBerdasarkanKode(kodeJadwal);
            if (_fjadwal != null)
            {
               // _jadwals.Add(_jadwal);

                _jadwals.Remove(_fjadwal);
            }
        }

        public Models.JadwalModel? CariJadwalBerdasarkanKode(long kodeJadwal)
        {
            /*
             * LINQ
             * First => temukan yang pertama, jika tidak ketemu error
             * FirstOrDefault => temukan yang pertama, jika tidak ketemu return null             
             */
            return _jadwals.Where(x => x.KodeJadwal == kodeJadwal).FirstOrDefault();
        }

        public List<Models.JadwalModel> DapatkanJadwal()
        {
            return _jadwals;
        }
    }
}
