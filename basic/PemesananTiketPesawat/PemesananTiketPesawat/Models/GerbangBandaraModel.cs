﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PemesananTiketPesawat.Models
{
    public class GerbangBandaraModel
    {
        public string KodeGerbang { get; set; }

        public string NomorGerbang { get; set; }

        public string NomorPintu { get; set; }

        public string KodeBandara { get; set; }


    }
}
