﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PemesananTiketPesawat.Models
{
    public class KursiPesawatModel
    {
        public string NomorKursi { get; set; }

        public string StatusKursi { get; set; }

        public string KodePenerbangan { get; set; }


    }
}
