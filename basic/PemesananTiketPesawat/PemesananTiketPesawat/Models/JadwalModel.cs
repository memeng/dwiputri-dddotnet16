﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace PemesananTiketPesawat.Models
{
    public class JadwalModel
    {
        public long KodeJadwal { get; set; }

        public string KodeMaskapai { get; set; }

        public string KodePenerbangan { get; set; }

        public DateTime TanggalKeberangkatan { get; set; }

        public DateTime  JamKeberangkatan { get; set; }

        public DateTime JamKedatangan { get; set; }

        public Decimal BatasBagasi { get; set; }

        public Decimal BatasBagasiKabin { get; set; }

        public string KodeBandaraAsal { get; set; }

        public string KodeBandaraTujuan { get; set; }

        public DateTime TanggalKedatangan { get; set; }








    }
}
