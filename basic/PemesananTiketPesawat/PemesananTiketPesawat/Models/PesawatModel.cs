﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PemesananTiketPesawat.Models
{
    public class PesawatModel
    {
        public string KodePenerbangan { get; set; }

        public string JenisPesawat { get; set; }

        public string KodeMaskapai { get; set; }

    }
}
