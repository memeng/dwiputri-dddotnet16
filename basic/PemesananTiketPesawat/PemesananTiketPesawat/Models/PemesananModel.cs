﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PemesananTiketPesawat.Models
{
    public class PemesananModel
    {
        public string KodePemesanan { get; set; }

        public string NamaAsalBandara { get; set; }

        public string AsalBandara { get; set; }

        public string TujuanBandara { get; set; }

        public string NamaTujuanBandara { get; set; }

        public DateTime TanggalBerangkat { get; set; }

        public DateTime JamKeberangkatan { get; set; }

        public string KodeMaskapai { get; set; }

        public string NamaMaskapai { get; set; }

        public DateTime Durasi { get; set; }

        public Decimal BatasBagasi { get; set; }

        public Decimal BatasBagasiKabin { get; set; }

        public string MetodePembayaran { get; set; }

        public Decimal HargaTiketPerOrang { get; set; }

        public int JumlahOrang { get; set; }

        public Decimal TotalHargaTiket { get; set; }

        public string StatusPemesanan { get; set; }

        public string KodePembayaran { get; set; }

        public string KodeCheckIn { get; set; }

        public string KodeCheckOut { get; set; }

        public string KodePenerbangan { get; set; }

    }
}
