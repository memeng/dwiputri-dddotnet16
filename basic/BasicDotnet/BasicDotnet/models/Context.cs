﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicDotnet.models
{
    class Context
    {
        public List<Dialog> Dialogs { get; set; }
        public List<Response> Responses { get; set; }

        public Context()
        {
            this.Dialogs = new List<Dialog>();
        }
    }

    class Dialog
    {
        public string Conversation {  get; set; }
    }

    class Response
    {
        public string Message { get; set; } 
    }
}
