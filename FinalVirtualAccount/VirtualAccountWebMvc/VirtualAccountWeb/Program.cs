using Microsoft.EntityFrameworkCore;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.ClearProviders();
builder.Logging.AddConsole();


// Add services to the container.
builder.Services.AddDbContext<VirtualAccountServices.Models.VirtualAccountDbContext>(options =>
options.UseSqlServer("name=ConnectionStrings:VIRTUAL_ACCOUNT"));
//builder.Services.AddScoped<VirtualAccountServices.Services.VirtualAccountService>();



builder.Services.AddControllersWithViews();
//builder.Services.AddMvc();
//builder.Services.AddHttpClient();



// Add services to the container.
//builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.Use(async (context, next) =>
{
    if (!(context.Request.Path == "" ||
            context.Request.Path == "/" ||
            context.Request.Path == "/Login" ||
            context.Request.Path == "/Login/Login" ||
            context.Request.Path == "/Login /Index"
            )

    )
    {

        var _token = "";
        if (context.Request.Method == "GET")
        {
            _token = context.Request.Query["t"];
        }
        else
        {
            _token = context.Request.Form["t"];
        }

        if (!string.IsNullOrEmpty(_token))
        {

            var scope = app.Services.CreateScope();
            var db = scope.ServiceProvider.GetService<VirtualAccountServices.Models.VirtualAccountDbContext>();
            if (db != null)
            {
                LoginService _LoginService = new LoginService(db, "");
                try
                {
                    _LoginService.FindByLoginID(_token);
                    await next.Invoke();
                    return;

                }
                catch (Exception ex)
                {
                    context.Response.Redirect("/Error");
                    return;
                }
            }
            else
            {
                context.Response.Redirect("/Error");
                return;
            }

        }
        else
        {
            context.Response.Redirect("/Login");
            return;

        }

    }
    else
    {
        await next.Invoke();
    }

});

app.MapControllerRoute("Login",
    "{controller=Login}/{action=Index}/{t}/{id?}");


//app.MapAreaControllerRoute("admin_with_token",
//    "Admin",
//    "Admin/{controller=Airport}/{action=Index}/{t}/{id?}");

//app.MapAreaControllerRoute("passenger",
//    "Passenger",
//    "Passenger/{controller=Login}/{action=Login}/{id?}");

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Login}/{action=Index}/{id?}");

app.Run();
