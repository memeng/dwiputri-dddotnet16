﻿using System.Web;
using VirtualAccountServices.Models;
using Microsoft.AspNetCore.Mvc;

namespace VirtualAccountWeb.Controllers
{
    public class BaseController : Controller
    {
        public static bool IsLogin = false;

        protected bool _isLogin()
        {

            if (!BaseController.IsLogin)
            {
                return false;
            }
               
            return true;
        }

    }
}
