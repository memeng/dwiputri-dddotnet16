﻿using VirtualAccountWeb;
namespace VirtualAccountWeb.Areas.ViewModels.VirtualAccount
{
    public class ListVirtualAccountViewModel : BaseViewModel
    {
        public List<DetailsVirtualAccountViewModel> VirtualAccounts { get; set; } = new List<DetailsVirtualAccountViewModel>();

    }
}
