﻿using System.ComponentModel.DataAnnotations;

namespace VirtualAccountWeb.Areas.ViewModels.VirtualAccount
{
    public class DetailsVirtualAccountViewModel : BaseViewModel
    {
        [Display(Name = "Virtual Account ID")]
        public string VirtualAccountID { get; set; } = String.Empty;

        [Display(Name = " Email")]
        public string Email { get; set; } = String.Empty;

        [Display(Name = "Password")]
        public string Password { get; set; } = String.Empty;

        [Display(Name = "Username")]
        public string Username { get; set; } = String.Empty;

        [Display(Name = "Balance")]
        public int Balance { get; set; }

        [Display(Name = "Mobile Phone")]
        public string MobilePhone { get; set; }


        public DetailsVirtualAccountViewModel() { }
        public DetailsVirtualAccountViewModel(string virtualAccountID,
            string email,
            string password,
            string username,
            int balance,
            string mobilephone)
        {
            this.VirtualAccountID = virtualAccountID;
            this.Email = email;
            this.Password = password;
            this.Username = username;
            this.Balance = balance;
            this.MobilePhone = mobilephone;
        }
    }
}
