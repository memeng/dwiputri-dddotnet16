﻿namespace VirtualAccountWeb.Areas.Controllers
{
    public class ErrorController
    {
        public IActionResult Index()
        {
            return View("/Areas/Views/Shared/Error.cshtml");
        }

    }
}

