﻿using Microsoft.AspNetCore.Mvc;
using VirtualAccountServices;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;

namespace VirtualAccountWeb.Areas.Controllers 
{
    public class VirtualAccountController : BaseController

    {

        
        static List<ViewModels.VirtualAccount.DetailsVirtualAccountViewModel> _virtualAccounts = new List<ViewModels.VirtualAccount.DetailsVirtualAccountViewModel>();

        private ILogger<VirtualAccountController> _logger;
        private VirtualAccountServices.Services.VirtualAccountService _virtualAccountService;

        public VirtualAccountController(ILogger<VirtualAccountController> logger,
            VirtualAccountServices.Services.VirtualAccountService virtualAccount)
        {
            _logger = logger;
            _virtualAccountService = virtualAccount;

            if (_virtualAccounts.Count == 0)
            {
                _virtualAccounts.Clear();
                _virtualAccounts.Add(new ViewModels.VirtualAccount.DetailsVirtualAccountViewModel("001", "dwi@gmail.com", "123456", "dwi0103", 1000000,"08123456"));
                _virtualAccounts.Add(new ViewModels.VirtualAccount.DetailsVirtualAccountViewModel("002", "putri@gmail.com", "123456", "putri0103", 2000000, "08123457"));
            }
        }

        // GET: VirtualAccountController
        public IActionResult Index(ViewModels.VirtualAccount.DetailsVirtualAccountViewModel indexViewModel)
        {
            var _virtualAccounts = _virtualAccountService.FindList();
            List<ViewModels.VirtualAccount.DetailsVirtualAccountViewModel> _virtualAccoutViewModels = new List<ViewModels.VirtualAccount.DetailsVirtualAccountViewModel>();

            foreach (var m in _virtualAccounts)
            {
                _virtualAccoutViewModels.Add(new ViewModels.VirtualAccount.DetailsVirtualAccountViewModel()
                {
                    VirtualAccountID = m.VirtualAccountID,
                    Email = m.Email,
                    Password = m.Password,
                    Username = m.Username,
                    Balance = m.Balance,
                    MobilePhone = m.MobilePhone,
                });
            }

            _logger.LogInformation("VirtualAccounts Counts {0}", _virtualAccounts.Count);
            var _listVirtualAccountViewModel = new ViewModels.VirtualAccount.ListVirtualAccountViewModel()
            {
                VirtualAccounts = _virtualAccoutViewModels,
                t = indexViewModel.t,
            };

            return View("Index", _listVirtualAccountViewModel);
        }


        
    }
}