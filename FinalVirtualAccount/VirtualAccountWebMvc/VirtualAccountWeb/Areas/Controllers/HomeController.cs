﻿using Microsoft.AspNetCore.Mvc;

namespace VirtualAccountWeb.Areas.Controllers
{
    
    public class HomeController : Controller
    {
        public IActionResult Index(ViewModels.Home.HomeViewModel homeViewModel)
        {
            return View(homeViewModel);
        }

    }
}

