﻿using Microsoft.AspNetCore.Mvc;
using VirtualAccountServices.Services;
using VirtualAccountWeb.Areas.ViewModels;

namespace VirtualAccountWeb.Areas.Controllers
{
    public class LoginController : BaseController
    {
        //create a logger
        private ILogger<LoginController> _logger;

        public LoginController(ILogger<LoginController> logger)
        {
            _logger = logger;
        }

        //Defines a contract that represents the result of an action method.
        public IActionResult Login()
        {
            _logger.Log(LogLevel.Debug, "Log message in the Index() method");

            if (!_isLogin())
            {
                var _loginViewModel = new ViewModels.Login.LoginViewModel();
                return View(_loginViewModel);
            }

            else
            {
                return RedirectToAction("Index","Home");
            }

        }

      
        private VirtualAccountServices.Models.VirtualAccountDbContext _db;

        public LoginController(ILogger<LoginController> logger,
            VirtualAccountServices.Models.VirtualAccountDbContext db)
        {
            
        }

        public IActionResult Index()
        {
            if (!_isLogin())
            {
                var _loginViewModel = new ViewModels.Login.LoginViewModel();
                return View(_loginViewModel);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login([Bind("Email, Password")] ViewModels.Login.LoginViewModel _loginVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_loginVM.Email == "dwi@gmail.com" && _loginVM.Password == "123456")
                    {
                        BaseController.IsLogin = true;
                        return RedirectToAction("Index", "Home");
                    }

                    else
                    {
                        throw new Exception("Login is not valid");
                    }
                }
            }
                
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View(_loginVM);

        }

        public IActionResult Logout()
        {
            BaseController.IsLogin = false;
            return RedirectToAction("Login", "Login");
        }




    }
}


