﻿using Xunit;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using VirtualAccountTest;

namespace VirtualAccountServices.Test
{
    //    /*
    //     * developer new project
    //     * developer maintenance project
    //     * QA Developer / Test Developer
    //     * Lead Technical 
    //     */
    public class TransferServiceTest
    {
        //        /*
        //         * Negative Test / False Test
        //         * 
        //         */
        //        /*
        //        [Fact]
        //        public void TransferService_NegativeTest()
        //        {

        //            TransferService _transferService = new TransferService(null);

        //            string _transferDate = "";
        //            string _fromVirtualAccountID = "";
        //            string _toVirtualAccountID = "";
        //            int _transferAmount = -1;


        //            try
        //            {
        //                _transferService.AddTransfer(_transferDate,
        //                _fromVirtualAccountID,
        //                _toVirtualAccountID,
        //                _transferAmount);
        //            }

        //            catch (Exception ex)
        //            {
        //                Assert.Equal("transfer_date_kosong", ex.Message);
        //            }

        //            _transferDate = "2022-02-24";
        //            try
        //            {
        //                _transferService.AddTransfer(_transferDate,
        //                _fromVirtualAccountID,
        //                _toVirtualAccountID,
        //                _transferAmount);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("from_virtual_account_id_kosong", ex.Message);
        //            }

        //            _fromVirtualAccountID = "1234565";
        //            try
        //            {
        //                _transferService.AddTransfer(_transferDate,
        //                _fromVirtualAccountID,
        //                _toVirtualAccountID,
        //                _transferAmount);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("to_virtual_account_id_kosong", ex.Message);
        //            }

        //            _toVirtualAccountID = "12334557";
        //            try
        //            {
        //                _transferService.AddTransfer(_transferDate,
        //               _fromVirtualAccountID,
        //               _toVirtualAccountID,
        //               _transferAmount);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("transfer_amount_negatif", ex.Message);
        //            }

        //            _transferAmount = 100000;
        //            _transferDate = "2022-02-3 25:00";
        //            try
        //            {
        //                _transferService.AddTransfer(_transferDate,
        //               _fromVirtualAccountID,
        //               _toVirtualAccountID,
        //               _transferAmount);
        //            }
        //            catch (Exception ex)
        //            {
        //                /*String '2022-02-3' was not recognized as a valid DateTime.\
        //                Assert.Contains("was not recognized as a valid DateTime", ex.Message);
        //            }
        //                */



        //            /*Positive Test */

        [Fact]

        void TestAdd_PositiveTest()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {

                TransferService _transferService = new TransferService(_db);
                _transferService.Migrate();


                string _transferDate = "2022-04-01 12:05 AM";
                string _fromVirtualAccountID = "001";
                string _toVirtualAccountID = "002";
                int _transferAmount = 10000;

                var _transferID = _transferService.AddTransfer(_transferDate,
           _fromVirtualAccountID,
           _toVirtualAccountID,
           _transferAmount);


                Models.TransferModel? _ftransfer = _transferService.FindTransferByID(_transferID);
                Assert.NotNull(_ftransfer);

                if (_ftransfer != null)
                {
                    Assert.Equal(_transferDate, _ftransfer.TransferDate.ToString("yyyy-MM-dd hh:mm tt"));
                    Assert.Equal(_transferAmount, _ftransfer.TransferAmount);
                    Assert.Equal(_fromVirtualAccountID, _ftransfer.FromVirtualAccountID);
                    Assert.Equal(_toVirtualAccountID, _ftransfer.ToVirtualAccountID);

                }

            }

        }
    }
}


//        /*
//        [Fact]
//        void TestTambahTransaction_SeedingTransaction()
//        {
//            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
//                .UseSqlServer(TestVars.ConnectionString)
//                .Options;

//            using (var _db = new Models.VirtualAccountDbContext(dbOption))
//            {

//                TransactionService _transactionService = new TransactionService(_db, TestVars.LoginEmail);
//                _transactionService.Migrate();

//                List<TransactionModel> _transactions = new List<TransactionModel>();
//                _transactions.Add(new TransactionModel()
//                {

//                    strTransactionDate = "2022-6-23",
//                    TransactionAmount = 10000,
//                    VirtualAccountID = "123454323",
//                    MerchantAccountID = "1235645",
//                    ReferenceID = "btn",
//                });

//                _transactions.Add(new TransactionModel()
//                {

//                    strTransactionDate = "2022-6-23",
//                    TransactionAmount = 10000,
//                    VirtualAccountID = "123454323",
//                    MerchantAccountID = "1235645",
//                    ReferenceID = "btn",
//                });


//                for (int i = 0; i < _transactions.Count; i++)
//                {

//                    var _transaction = _transactions[i];
//                    var _virtualAccountID = _transaction.VirtualAccountID;
//                    var _transactionAmount = _transaction.TransactionAmount;
//                    var _transactionDate = _transaction.strTransactionDate;
//                    var _vaMerchantID = _transaction.MerchantAccountID;
//                    var _referenceID = _transaction.ReferenceID;

//                    var _transactionn = _transactionService.TambahTransaction(
//                        _transactionDate,
//                         _virtualAccountID,
//                        _vaMerchantID,
//                       _transactionAmount,
//                         _referenceID);


//                    Models.TransactionModel? _fTransaction = _transactionService.FindTrasanctionByID();
//                    Assert.NotNull(_fTransaction);

//                    if (_fTransaction != null)
//                    {
//                        Assert.Equal(_virtualAccountID, _fTransaction.VirtualAccountID);
//                        Assert.Equal(_transactionAmount, _fTransaction.TransactionAmount);
//                        Assert.Equal(_transactionDate, _fTransaction.TransactionDate.ToString("yyyy-MM-dd hh:mm tt"));
//                        Assert.Equal(_vaMerchantID, _fTransaction.MerchantAccountID);
//                        Assert.Equal(_referenceID, _fTransaction.ReferenceID);
//                    }

//                }


//            }


//        }
//        */


//    }
//}