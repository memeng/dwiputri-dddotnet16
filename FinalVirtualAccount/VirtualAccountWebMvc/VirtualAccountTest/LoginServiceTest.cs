﻿
using Xunit;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using VirtualAccountTest;

namespace VirtualAccountServices.Test
{
    public class LoginTest
    {
        [Fact]

        public void TestRegister_NormalTest()
        {
            //connect dengan dbcontext
            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                    .UseSqlServer(TestVars.ConnectionString)
                    .Options;


            //migrate dengan 
            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {

                RegisterService _registerService = new RegisterService(_db);
                _registerService.Migrate();

                LoginService _LoginService = new LoginService(_db,
                    TestVars.LoginEmail);
                _LoginService.Migrate();

                VirtualAccountService _virtualAccountService = new VirtualAccountService(_db,
                    TestVars.LoginEmail);
                _virtualAccountService.Migrate();

                var _virtualAccountID = "001";
                var _email = "dwi@gmail.com";
                var _password = "123456";
                var _username = "dwi0103";
                var _mobilephone = "08123456";
                var _active = true;
                var _balance = 100000;



                _registerService.AddRegister(_email,
                    _password,
                    _password,
                    _username,
                    _active,
                    _mobilephone);

                _virtualAccountService.AddVirtualAccount(_virtualAccountID,
                    _email,
                    _password,
                    _username,
                    _mobilephone,
                    _balance
                    );

                var _fRegister = _registerService.FindByEmail(_email, false);
                Assert.NotNull(_fRegister);
                if (_fRegister != null)
                {
                    Assert.Equal(_email, _fRegister.Email);
                    Assert.Equal(_username, _fRegister.Username);

                    var _createdAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd hh:mm tt");
                    Assert.Equal(_createdAt, _fRegister.CreatedAt.ToString("yyyy-MM-dd hh:mm tt"));

                    //  Assert.Equal(TestVars.LoginEmail, _fRegister.CreatedBy);

                }


                _username = "dwi putri";
                _active = false;
                _registerService.UpdateRegister(_email,
                    _username,
                    _active);

                _fRegister = _registerService.FindByEmail(_email, false);
                Assert.NotNull(_fRegister);
                if (_fRegister != null)
                {
                    Assert.Equal(_email, _fRegister.Email);
                    Assert.Equal(_username, _fRegister.Username);

                    /*var _updatedAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd");
                    Assert.Equal(_updatedAt, _fAdminUser.UpdatedAt.ToString("yyyy-MM-dd"));*/

                    // Assert.Equal(TestVars.LoginEmail, _fRegister.UpdatedBy);

                }



                var _Login = _LoginService.Login(_email, _password);

                var _fLogin = _LoginService.FindByLoginID(_Login.LoginID);
                if (_fLogin != null)
                {
                    Assert.Equal(_email, _fLogin.Email);
                    var _createdAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd hh:mm tt");
                    Assert.Equal(_createdAt, _fLogin.CreatedAt.ToString("yyyy-MM-dd hh:mm tt"));
                    Assert.Equal(false, _fLogin.Expired);
                }


                _LoginService.Logout(_Login.LoginID);

                _fLogin = _LoginService.FindByLoginID(_Login.LoginID);
                if (_fLogin != null)
                {
                    Assert.Equal(true, _fLogin.Expired);
                }

            }

        }

        public class akundua
        {

            [Fact]

            public void tesakunkedua()
            {
                //connect dengan dbcontext
                var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                        .UseSqlServer(TestVars.ConnectionString)
                        .Options;


                //migrate dengan 
                using (var _db = new Models.VirtualAccountDbContext(dbOption))
                {

                    RegisterService _registerService = new RegisterService(_db);
                    _registerService.Migrate();

                    LoginService _LoginService = new LoginService(_db,
                        TestVars.LoginEmail);
                    _LoginService.Migrate();

                    VirtualAccountService _virtualAccountService = new VirtualAccountService(_db,
                        TestVars.LoginEmail);
                    _virtualAccountService.Migrate();

                    var _virtualAccountID = "002";
                    var _email = "putri@gmail.com";
                    var _password = "123456";
                    var _username = "putri0103";
                    var _mobilephone = "08123457";
                    var _active = true;
                    var _balance = 100000;



                    _registerService.AddRegister(_email,
                        _password,
                        _password,
                        _username,
                        _active,
                        _mobilephone);

                    _virtualAccountService.AddVirtualAccount(_virtualAccountID,
                        _email,
                        _password,
                        _username,
                        _mobilephone,
                        _balance
                        );

                    var _fRegister = _registerService.FindByEmail(_email, false);
                    Assert.NotNull(_fRegister);
                    if (_fRegister != null)
                    {
                        Assert.Equal(_email, _fRegister.Email);
                        Assert.Equal(_username, _fRegister.Username);

                        var _createdAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd hh:mm tt");
                        Assert.Equal(_createdAt, _fRegister.CreatedAt.ToString("yyyy-MM-dd hh:mm tt"));

                        //  Assert.Equal(TestVars.LoginEmail, _fRegister.CreatedBy);

                    }


                    _username = "putri0103";
                    _active = false;
                    _registerService.UpdateRegister(_email,
                        _username,
                        _active);

                    _fRegister = _registerService.FindByEmail(_email, false);
                    Assert.NotNull(_fRegister);
                    if (_fRegister != null)
                    {
                        Assert.Equal(_email, _fRegister.Email);
                        Assert.Equal(_username, _fRegister.Username);

                        /*var _updatedAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd");
                        Assert.Equal(_updatedAt, _fAdminUser.UpdatedAt.ToString("yyyy-MM-dd"));*/

                        // Assert.Equal(TestVars.LoginEmail, _fRegister.UpdatedBy);

                    }



                    var _Login = _LoginService.Login(_email, _password);

                    var _fLogin = _LoginService.FindByLoginID(_Login.LoginID);
                    if (_fLogin != null)
                    {
                        Assert.Equal(_email, _fLogin.Email);
                        var _createdAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd hh:mm tt");
                        Assert.Equal(_createdAt, _fLogin.CreatedAt.ToString("yyyy-MM-dd hh:mm tt"));
                        Assert.Equal(false, _fLogin.Expired);
                    }


                    _LoginService.Logout(_Login.LoginID);

                    _fLogin = _LoginService.FindByLoginID(_Login.LoginID);
                    if (_fLogin != null)
                    {
                        Assert.Equal(true, _fLogin.Expired);
                    }

                }

            }
        }

    }

}

