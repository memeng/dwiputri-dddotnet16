﻿using Xunit;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using VirtualAccountTest;

namespace VirtualAccountServices.Test
{
    public class BankAccountServiceTest
    {
        [Fact]
        public void TestBankAccount_NormalTest()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {
                BankAccountService _bankAccountService = new BankAccountService(_db, TestVars.LoginEmail);
                _bankAccountService.Migrate();

                var _bankAccountID = "B001";
                var _virtualAccountID = "001";
                var _bankID = "BTN01";
                var _bankAccountNumber = "00001";
                var _bankAccountOwner = "dwi putri";
                var _balance = 1000000;


                _bankAccountService.AddBankAccount(_bankAccountID,
                    _virtualAccountID,
            _bankID,
          _bankAccountNumber,
            _bankAccountOwner,
           _balance);



                var _fBankAccount = _bankAccountService.FindByBankAccountID(_bankAccountID);
                Assert.NotNull(_fBankAccount);
                if (_fBankAccount != null)
                {
                    Assert.Equal(_bankID, _fBankAccount.BankID);
                    Assert.Equal(_bankAccountNumber, _fBankAccount.BankAccountNumber);
                    Assert.Equal(_bankAccountOwner, _fBankAccount.BankAccountOwner);
                    Assert.Equal(_balance, _fBankAccount.Balance);
                }

            }
        }
        /*
        [Fact]
        public void TestBankAccount_SeedingBankAccounts()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {

                BankAccountService _bankAccountService = new BankAccountService(_db);
                _bankAccountService.Migrate();

                Models.BankAccountModel[] _bankAccounts = new Models.BankAccountModel[]
                {
                    new BankAccountModel("JT619", "1A","qqqqq","wwwww",100000),
                    new BankAccountModel("JT619", "1B","qqqqq","wwwww",100000),
                    new BankAccountModel("JT619", "1C","qqqqq","wwwww",100000),
                    new BankAccountModel("JT619", "1D","qqqqq","wwwww",100000),
                    new BankAccountModel("JT619", "1E","qqqqq","wwwww",100000),
                    new BankAccountModel("JT619", "1F","qqqqq","wwwww",100000),


                };

                for (int i = 0; i < _bankAccounts.Length; i++)
                {
                    var _bankAccount = _bankAccounts[i];
                    var _bankID = _bankAccount.BankID;
                    var _bankAccountNumber = _bankAccount.BankAccountNumber;
                    var _bankAccountOwner = _bankAccount.BankAccountOwner;
                    var _balance = _bankAccount.Balance;

                    var _bankAccountID = _bankAccountService.AddBankAccount(_bankID, _bankAccountNumber, _bankAccountOwner, _balance );

                    var _fBankAccount = _bankAccountService.FindByBankAccountID(_bankAccountID);
                    Assert.NotNull(_fBankAccount);
                    if (_fBankAccount != null)
                    {
                        Assert.Equal(_bankID, _fBankAccount.BankID);
                        Assert.Equal(_bankAccountNumber, _fBankAccount.BankAccountNumber);
                        Assert.Equal(_bankAccountOwner, _fBankAccount.BankAccountOwner);
                        Assert.Equal(_balance, _fBankAccount.Balance);

                    }

                }

            }
        }
        */

    }
}
