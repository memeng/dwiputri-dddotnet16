using Xunit;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;
using VirtualAccountTest;

namespace VirtualAccountServices.Test
{
    public class BankServiceTest
    {

        [Fact]
        public void TestBank_NormalTest()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {

                BankService _bankService = new BankService(_db, TestVars.LoginEmail);
                _bankService.Migrate();

                var _bankID = "BTN01";
                var _bankName = "BTN";

                _bankService.AddBank(_bankID, _bankName);

                var _fBank = _bankService.FindBankByBankID(_bankID, true);
                Assert.NotNull(_fBank);
                if (_fBank != null)
                {
                    Assert.Equal(_bankID, _fBank.BankID);
                    Assert.Equal(_bankName, _fBank.BankName);
                }
            }
        }
    }
}





//        [Fact]
//        public void TestBank_SeedingBanks()
//        {
//            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
//                .UseSqlServer(TestVars.ConnectionString)
//                .Options;


//            using (var _db = new Models.VirtualAccountDbContext(dbOption))
//            {
//                BankService _bankService = new BankService(_db, TestVars.LoginEmail);
//                _bankService.Migrate();

//                Models.BankModel[] _banks = new Models.BankModel[]
//                {
//                    new BankModel("BTN01", "BTN"),
//                    new BankModel("MND01", "Mandiri"),
//                };

//                for (int i = 0; i < _banks.Length; i++)
//                {
//                    var _bank = _banks[i];
//                    var _bankID = _bank.BankID;
//                    var _bankName = _bank.BankName;

//                    _bankService.AddBank(_bankID, _bankName);

//                    var _fbank = _bankService.FindBankByBankID(_bankID, true);
//                    Assert.NotNull(_fbank);
//                    if (_fbank != null)
//                    {
//                        Assert.Equal(_bankID, _fbank.BankID);
//                        Assert.Equal(_bankName, _fbank.BankName);

//                    }

//                }
//            }
//        }

//    }
//}
