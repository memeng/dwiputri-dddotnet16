﻿using Xunit;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using VirtualAccountTest;

namespace VirtualAccountServices.Test
{
    //    /*
    //     * developer new project
    //     * developer maintenance project
    //     * QA Developer / Test Developer
    //     * Lead Technical 
    //     */
    public class TransactionServiceTest
    {
        //        /*
        //         * Negative Test / False Test
        //         * 
        //         */
        //        //[Fact]
        //        //public void TransactionService_NegativeTest()
        //        //{

        //        //    TransactionService _transactionService = new TransactionService(null);

        //        //    string _transactionDate = "";
        //        //    string _virtualAccountID = "";
        //        //    string _vaMerchantID = "";
        //        //    int _transactionAmount = -1;
        //        //    string _referenceID = "";

        //        //    try
        //        //    {
        //        //        _transactionService.TambahTransaction(_transactionDate,
        //        //        _virtualAccountID,
        //        //        _vaMerchantID,
        //        //        _transactionAmount,
        //        //        _referenceID);
        //        //    }

        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("transaction_date_kosong", ex.Message);
        //        //    }

        //        //    _transactionDate = "2022-02-24";
        //        //    try
        //        //    {
        //        //        _transactionService.TambahTransaction(_transactionDate,
        //        //          _virtualAccountID,
        //        //          _vaMerchantID,
        //        //          _transactionAmount,
        //        //          _referenceID);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("virtual_account_id_kosong", ex.Message);
        //        //    }

        //        //    _virtualAccountID = "1234565";
        //        //    try
        //        //    {
        //        //        _transactionService.TambahTransaction(_transactionDate,
        //        //        _virtualAccountID,
        //        //        _vaMerchantID,
        //        //        _transactionAmount,
        //        //        _referenceID);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("transaction_amount_tidak boleh negatif", ex.Message);
        //        //    }


        //        //    _vaMerchantID = "1234565";
        //        //    try
        //        //    {
        //        //        _transactionService.TambahTransaction(_transactionDate,
        //        //        _virtualAccountID,
        //        //        _vaMerchantID,
        //        //        _transactionAmount,
        //        //        _referenceID);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("transaction_amount_tidak boleh negatif", ex.Message);
        //        //    }

        //        //    _transactionAmount = 100000;
        //        //    try
        //        //    {
        //        //        _transactionService.TambahTransaction(_transactionDate,
        //        //        _virtualAccountID,
        //        //        _vaMerchantID,
        //        //        _transactionAmount,
        //        //        _referenceID);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("reference_id_kosong", ex.Message);
        //        //    }

        //        //    _referenceID = "153745673";
        //        //    _transactionDate = "2022-02-3 25:00";
        //        //    try
        //        //    {
        //        //        _transactionService.TambahTransaction(_transactionDate,
        //        //         _virtualAccountID,
        //        //         _vaMerchantID,
        //        //         _transactionAmount,
        //        //         _referenceID);

        //        //    }

        //        //    catch (Exception ex)
        //        //    {
        //        //        /*String '2022-02-3' was not recognized as a valid DateTime.*/
        //        //        Assert.Contains("was not recognized as a valid DateTime", ex.Message);
        //        //    }

        //            /*Positive Test */

        [Fact]

        void TestTambahTransaction_PositiveTest()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {

                TransactionService _transactionService = new TransactionService(_db, TestVars.LoginEmail);
                _transactionService.Migrate();

               // string _transactionID = "TR001";
                string _transactionDate = "2022-03-01 12:05 AM";
                string _virtualAccountID = "001";
                string _merchantAccountID = "001";
                int _transactionAmount = 10000;
                string _referenceID = "001";

                var _transactionid = _transactionService.TambahTransaction(
                    _transactionDate,
             _virtualAccountID,
             _merchantAccountID,
             _transactionAmount,
             _referenceID);


                Models.TransactionModel? _ftransaction = _transactionService.FindTrasanctionByID(_transactionid);
                Assert.NotNull(_ftransaction);

                if (_ftransaction != null)
                {
                    
                    Assert.Equal(_transactionid, _ftransaction.TransactionID);
                    Assert.Equal(_transactionDate, _ftransaction.TransactionDate.ToString("yyyy-MM-dd hh:mm tt"));
                    Assert.Equal(_transactionAmount, _ftransaction.TransactionAmount);
                    Assert.Equal(_virtualAccountID, _ftransaction.VirtualAccountID);
                    Assert.Equal(_merchantAccountID, _ftransaction.MerchantAccountID);
                    Assert.Equal(_referenceID, _ftransaction.ReferenceID);

                }

            }

        }
    }
}



//            //[Fact]
//            //void TestTambahTransaction_SeedingTransaction()
//            //{
//            //    var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
//            //        .UseSqlServer(TestVars.ConnectionString)
//            //        .Options;

//            //    using (var _db = new Models.VirtualAccountDbContext(dbOption))
//            //    {

//            //        TransactionService _transactionService = new TransactionService(_db);
//            //        _transactionService.Migrate();

//            //        List<TransactionModel> _transactions = new List<TransactionModel>();
//            //        _transactions.Add(new TransactionModel()
//            //        {

//            //            strTransactionDate = "2022-6-23",
//            //            TransactionAmount = 10000,
//            //            VirtualAccountID = "123454323",
//            //            VaMerchantID = "1235645",
//            //            ReferenceID = "btn",
//            //        });

//            //        _transactions.Add(new TransactionModel()
//            //        {

//            //            strTransactionDate = "2022-6-23",
//            //            TransactionAmount = 10000,
//            //            VirtualAccountID = "123454323",
//            //            VaMerchantID = "1235645",
//            //            ReferenceID = "btn",
//            //        });


//            //        for (int i = 0; i < _transactions.Count; i++)
//            //        {

//            //            var _transaction = _transactions[i];
//            //            var _virtualAccountID = _transaction.VirtualAccountID;
//            //            var _transactionAmount = _transaction.TransactionAmount;
//            //            var _transactionDate = _transaction.strTransactionDate;
//            //            var _vaMerchantID = _transaction.VaMerchantID;
//            //            var _referenceID = _transaction.ReferenceID;

//            //            var _transactionID = _transactionService.TambahTransaction(
//            //                _transactionDate,
//            //     _virtualAccountID,
//            //              _vaMerchantID,
//            //               _transactionAmount,
//            //     _referenceID);


//            //            Models.TransactionModel? _fTransaction = _transactionService.FindTrasanctionByID(_transactionID);
//            //            Assert.NotNull(_fTransaction);

//            //            if (_fTransaction != null)
//            //            {
//            //                Assert.Equal(_virtualAccountID, _fTransaction.VirtualAccountID);
//            //                Assert.Equal(_transactionAmount, _fTransaction.TransactionAmount);
//            //                Assert.Equal(_transactionDate, _fTransaction.TransactionDate.ToString("yyyy-MM-dd hh:mm tt"));
//            //                Assert.Equal(_vaMerchantID, _fTransaction.VaMerchantID);
//            //                Assert.Equal(_referenceID, _fTransaction.ReferenceID);
//            //            }

//            //        }


//            //    }






//    }
//}