﻿using Xunit;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using VirtualAccountTest;

namespace VirtualAccountServices.Test
{

    public class TransactionHistoryServiceTest
    {
        public void TestHistoryTransactionPositiveTest()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {

                TransactionHistoryService _transactionHistoryService = new TransactionHistoryService(_db);
                _transactionHistoryService.Migrate();

                //string _topUpID = "Top01";
                string _historyDate = "2022-03-01 12:05 AM";
                int _amount = 10000;
                string _virtualAccountID = "001";
                string _merchantAccountID = "001";
                string _transactionID = "";
                string _transactionType = "";

                List<Models.TransactionHistoryModel> histories = new List<TransactionHistoryModel>();

                //_bankAccounts.Add(new BankAccountModel("B001", "001", "BTN01", "00001", "dwi putri", 100000));
                //_bankAccounts.Add(new BankAccountModel("B002", "002", "MND01", "00002", "putri dwi", 200000));



                var _historyTransactionID = _transactionHistoryService.AddHistory(
                    _historyDate,
             _virtualAccountID,
             _merchantAccountID,
             _transactionID,
                 _amount,
             _transactionType);            



                Models.TransactionHistoryModel _fhistory = _transactionHistoryService.FindByHistoryID(_historyTransactionID);
                Assert.NotNull(_fhistory);

                if (_fhistory != null)
                {
                    Assert.Equal(_historyDate, _fhistory.HistoryDate.ToString("yyyy-MM-dd hh:mm tt"));
                    Assert.Equal(_amount, _fhistory.Amount);
                    Assert.Equal(_virtualAccountID, _fhistory.VirtualAccountID);
                    Assert.Equal(_merchantAccountID, _fhistory.MerchantAccountID);

                    Assert.Equal(_transactionID, _fhistory.TransactionID);
                    Assert.Equal(_transactionType, _fhistory.TransactionType);

                }
            }

        }
    }
}

