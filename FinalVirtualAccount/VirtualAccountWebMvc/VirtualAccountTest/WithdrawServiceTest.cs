﻿using Xunit;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using VirtualAccountTest;

namespace VirtualAccountServices.Test
{
    //    /*
    //     * developer new project
    //     * developer maintenance project
    //     * QA Developer / Test Developer
    //     * Lead Technical 
    //     */
    public class WithdrawServiceTest
    {
        //        /*
        //         * Negative Test / False Test
        //         * 
        //         */
        //        //[Fact]
        //        //public void WithdrawService_NegativeTest()
        //        //{

        //        //    WithdrawService _withdrawService = new WithdrawService(null);

        //        //    string _withdrawDate = "";
        //        //    string _virtualAccountID = "";
        //        //    string _bankID = "";
        //        //    int _withdrawAmount = -1;
        //        //    string _bankName = "";
        //        //    int _bankAccountNumber = -1;
        //        //    string _bankAccountOwner = "";

        //        //    try
        //        //    {
        //        //        _withdrawService.TambahWithdraw(_virtualAccountID,
        //        //                                         _withdrawAmount,
        //        //                                         _withdrawDate,
        //        //                                         _bankID,
        //        //                                        _bankName,
        //        //                                        _bankAccountNumber,
        //        //                                        _bankAccountOwner);
        //        //    }

        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("virtual_account_id_kosong", ex.Message);
        //        //    }

        //        //    _virtualAccountID = "1245253465";
        //        //    try
        //        //    {
        //        //        _withdrawService.TambahWithdraw(_virtualAccountID,
        //        //                                           _withdrawAmount,
        //        //                                           _withdrawDate,
        //        //                                           _bankID,
        //        //                                          _bankName,
        //        //                                          _bankAccountNumber,
        //        //                                          _bankAccountOwner);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("withdraw_Amount_negative", ex.Message);
        //        //    }

        //        //    _withdrawAmount = 1000000;
        //        //    try
        //        //    {
        //        //        _withdrawService.TambahWithdraw(_virtualAccountID,
        //        //                                          _withdrawAmount,
        //        //                                          _withdrawDate,
        //        //                                          _bankID,
        //        //                                         _bankName,
        //        //                                         _bankAccountNumber,
        //        //                                         _bankAccountOwner);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("withdraw_date_kosong", ex.Message);
        //        //    }


        //        //    _withdrawDate = "2022-06-23";
        //        //    try
        //        //    {
        //        //        _withdrawService.TambahWithdraw(_virtualAccountID,
        //        //                                           _withdrawAmount,
        //        //                                           _withdrawDate,
        //        //                                           _bankID,
        //        //                                          _bankName,
        //        //                                          _bankAccountNumber,
        //        //                                          _bankAccountOwner);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("bank_id_kosong", ex.Message);
        //        //    }

        //        //    _bankID = "1234455";
        //        //    try
        //        //    {
        //        //        _withdrawService.TambahWithdraw(_virtualAccountID,
        //        //                                           _withdrawAmount,
        //        //                                           _withdrawDate,
        //        //                                           _bankID,
        //        //                                          _bankName,
        //        //                                          _bankAccountNumber,
        //        //                                          _bankAccountOwner);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("bank_name_kosong", ex.Message);
        //        //    }

        //        //    _bankName = "mandiri";
        //        //    try
        //        //    {
        //        //        _withdrawService.TambahWithdraw(_virtualAccountID,
        //        //                                           _withdrawAmount,
        //        //                                           _withdrawDate,
        //        //                                           _bankID,
        //        //                                          _bankName,
        //        //                                          _bankAccountNumber,
        //        //                                          _bankAccountOwner);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("bank_account_number_kosong", ex.Message);
        //        //    }

        //        //    _bankAccountNumber = 1234545;
        //        //    try
        //        //    {
        //        //        _withdrawService.TambahWithdraw(_virtualAccountID,
        //        //                                           _withdrawAmount,
        //        //                                           _withdrawDate,
        //        //                                           _bankID,
        //        //                                          _bankName,
        //        //                                          _bankAccountNumber,
        //        //                                          _bankAccountOwner);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        Assert.Equal("bank_account_owner_kosong", ex.Message);
        //        //    }

        //        //    _bankAccountOwner = "dwi putri";
        //        //    _withdrawDate = "2022-02-3 25:00";

        //        //    try
        //        //    {
        //        //        _withdrawService.TambahWithdraw(_virtualAccountID,
        //        //                                           _withdrawAmount,
        //        //                                           _withdrawDate,
        //        //                                           _bankID,
        //        //                                          _bankName,
        //        //                                          _bankAccountNumber,
        //        //                                          _bankAccountOwner);
        //        //    }
        //        //    catch (Exception ex)
        //        //    {
        //        //        /*String '2022-02-3' was not recognized as a valid DateTime.*/
        //        //        Assert.Contains("was not recognized as a valid DateTime", ex.Message);
        //        //    }



        //        /*Positive Test */

        [Fact]

        public void TestTambahWithdraw_PositiveTest()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {

                WithdrawService _withdrawService = new WithdrawService(_db);
                _withdrawService.Migrate();

                string _withdrawDate = "2022-03-01 12:05 AM";
                string _virtualAccountID = "001";
                string _bankID = "BTN01";
                int _withdrawAmount = 10000;
                string _bankName = "BTN";
                string _bankAccountNumber = "0001";
                string _bankAccountOwner = "dwi putri";
                string _withdrawID = "WD01";

                _withdrawService.TambahWithdraw(_withdrawID,
                    _virtualAccountID,
                                               _withdrawAmount,
                                               _withdrawDate,
                                               _bankID,
                                              _bankName,
                                              _bankAccountNumber,
                                              _bankAccountOwner);


                Models.WithdrawModel? _fwithdraw = _withdrawService.FindWithdrawByID(_withdrawID, true);
                Assert.NotNull(_fwithdraw);

                if (_fwithdraw != null)
                {
                    Assert.Equal(_withdrawDate, _fwithdraw.WithdrawDate.ToString("yyyy-MM-dd hh:mm tt"));
                    Assert.Equal(_withdrawAmount, _fwithdraw.WithdrawAmount);
                    Assert.Equal(_virtualAccountID, _fwithdraw.VirtualAccountID);
                    Assert.Equal(_bankID, _fwithdraw.BankID);
                    Assert.Equal(_bankName, _fwithdraw.BankName);
                    Assert.Equal(_bankAccountNumber, _fwithdraw.BankAccountNumber);
                    Assert.Equal(_bankAccountOwner, _fwithdraw.BankAccountOwner);
                    Assert.Equal(_withdrawID, _fwithdraw.WithdrawID);
                }

            }

        }
    }
}



//        //        [Fact]
//        //        void TestTambahWithdraw_SeedingWithdraw()
//        //        {
//        //            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
//        //                .UseSqlServer(TestVars.ConnectionString)
//        //                .Options;

//        //            using (var _db = new Models.VirtualAccountDbContext(dbOption))
//        //            {

//        //                WithdrawService _withdrawService = new WithdrawService(_db);
//        //                _withdrawService.Migrate();

//        //                List<WithdrawModel> _withdraws = new List<WithdrawModel>();
//        //                _withdraws.Add(new WithdrawModel()
//        //                {

//        //                    VirtualAccountID = "132165789",
//        //                    WithdrawAmount = 1900000,
//        //                    strWithdrawDate = "2022-06-23",
//        //                    BankID = "1231324656",
//        //                    BankName = "bni",
//        //                    BankAccountNumber = 345783745,
//        //                    BankAccountOwner = "xcgvdfgv",
//        //            });

//        //                _withdraws.Add(new WithdrawModel()
//        //                {

//        //                    VirtualAccountID = "132165789",
//        //                    WithdrawAmount = 1900000,
//        //                    strWithdrawDate = "2022-06-23",
//        //                    BankID = "1231324656",
//        //                    BankName = "bni",
//        //                    BankAccountNumber = 345783745,
//        //                    BankAccountOwner = "xcgvdfgv",
//        //                });


//        //                for (int i = 0; i < _withdraws.Count; i++)
//        //                {

//        //                    var _withdraw = _withdraws[i];
//        //                    var _virtualAccountID = _withdraw.VirtualAccountID;
//        //                    var _withdrawAmount = _withdraw.WithdrawAmount;
//        //                    var _withdrawDate = _withdraw.strWithdrawDate;
//        //                    var _bankID = _withdraw.BankID;
//        //                    var _bankName = _withdraw.BankName;
//        //                    var _bankAccountNumber = _withdraw.BankAccountNumber;
//        //                    var _bankAccountOwner = _withdraw.BankAccountOwner;

//        //                    var _withdrawID = _withdrawService.TambahWithdraw(
//        //                       _virtualAccountID,
//        //                                               _withdrawAmount,
//        //                                               _withdrawDate,
//        //                                               _bankID,
//        //                                              _bankName,
//        //                                              _bankAccountNumber,
//        //                                              _bankAccountOwner);


//        //                    Models.WithdrawModel? _fWithdraw = _withdrawService.FindWithdrawByID(_withdrawID);
//        //                    Assert.NotNull(_fWithdraw);

//        //                    if (_fWithdraw != null)
//        //                    {
//        //                        Assert.Equal(_virtualAccountID, _fWithdraw.VirtualAccountID);
//        //                        Assert.Equal(_withdrawAmount, _fWithdraw.WithdrawAmount);
//        //                        Assert.Equal(_withdrawDate, _fWithdraw.WithdrawDate.ToString("yyyy-MM-dd hh:mm tt"));
//        //                        Assert.Equal(_bankID, _fWithdraw.BankID);
//        //                        Assert.Equal(_bankName, _fWithdraw.BankName);
//        //                        Assert.Equal(_bankAccountNumber, _fWithdraw.BankAccountNumber);
//        //                        Assert.Equal(_bankAccountOwner, _fWithdraw.BankAccountOwner);
//        //                    }

//        //                }



//        //    }

//        //}


//        //    }


//        //[Fact]
//        //public void TestWithdrawService_Withdraw()
//        //{
//        //    var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
//        //           .UseSqlServer(TestVars.ConnectionString)
//        //            .Options;

//        //    using (var _db = new Models.VirtualAccountDbContext(dbOption)) ;
//        //    {

//        //    }
//        //}


//    }
//}

///*
// ﻿using Xunit;

//namespace VirtualAccount.Test;

//public class WithdrawServiceTest
//{
//    [Fact]
//    public void TestWithdrawService_Withdraw()
//    {
//        /*
//         * test top up
//         * 
//         * cek saldo di bank account
//         * cek saldo di virtual account

//         * cek transction history dimana transactionID = withdrawID
//         * cek topUp dimana withdrawID = withdrawID
//         * 

//    }

//}

// */