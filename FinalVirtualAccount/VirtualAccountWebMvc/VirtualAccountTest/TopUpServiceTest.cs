﻿using Xunit;
using VirtualAccountServices.Services;
using VirtualAccountServices.Models;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using VirtualAccountTest;

namespace VirtualAccountServices.Test
{
    //    /*
    //     * developer new project
    //     * developer maintenance project
    //     * QA Developer / Test Developer
    //     * Lead Technical 
    //     */
    public class TopUpServiceTest
    {
        //        /*
        //         * Negative Test / False Test
        //         * 
        //         */
        //        /*
        //        [Fact]
        //        public void TopUpService_NegativeTest()
        //        {

        //            TopUpService _topUpService = new TopUpService(null);

        //            string _topUpDate = "";
        //            int _topUpAmount = -1;
        //            string _virtualAccountID = "";
        //            string _bankID = "";
        //            string _bankName = "";
        //            string _bankAccountNumber = "";
        //            string _bankAccountOwner = "";

        //            try
        //            {
        //                _topUpService.AddTopUp(_topUpDate,
        //               _topUpAmount,
        //             _virtualAccountID,
        //             _bankID,
        //             _bankName,
        //             _bankAccountNumber,
        //             _bankAccountOwner);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("top_up_date_kosong", ex.Message);
        //            }


        //            _topUpDate = "2022-02-24";
        //            try
        //            {
        //                _topUpService.AddTopUp(_topUpDate,
        //               _topUpAmount,
        //             _virtualAccountID,
        //             _bankID,
        //             _bankName,
        //             _bankAccountNumber,
        //             _bankAccountOwner);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("top_amount_harus_lebih_besar_atau_sama_dengan_nol", ex.Message);
        //            }



        //            _topUpAmount = 100000;
        //            try
        //            {
        //                _topUpService.AddTopUp(_topUpDate,
        //               _topUpAmount,
        //             _virtualAccountID,
        //             _bankID,
        //             _bankName,
        //             _bankAccountNumber,
        //             _bankAccountOwner);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("virtual_account_id_kosong", ex.Message);
        //            }




        //            _virtualAccountID = "1121111111";
        //            try
        //            {
        //                _topUpService.AddTopUp(_topUpDate,
        //               _topUpAmount,
        //             _virtualAccountID,
        //             _bankID,
        //             _bankName,
        //             _bankAccountNumber,
        //             _bankAccountOwner);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("bank_id_kosong", ex.Message);
        //            }

        //            _bankID = "1234565432";
        //            try
        //            {
        //                _topUpService.AddTopUp(_topUpDate,
        //              _topUpAmount,
        //            _virtualAccountID,
        //            _bankID,
        //            _bankName,
        //            _bankAccountNumber,
        //            _bankAccountOwner);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("bank_name_kosong", ex.Message);
        //            }

        //            _bankName = "mandiri";

        //            try
        //            {
        //                _topUpService.AddTopUp(_topUpDate,
        //               _topUpAmount,
        //             _virtualAccountID,
        //             _bankID,
        //             _bankName,
        //             _bankAccountNumber,
        //             _bankAccountOwner);
        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("bank_account_number_kosong", ex.Message);
        //            }

        //            _bankAccountNumber = "12345678";
        //            try
        //            {
        //                _topUpService.AddTopUp(_topUpDate,
        //              _topUpAmount,
        //            _virtualAccountID,
        //            _bankID,
        //            _bankName,
        //            _bankAccountNumber,
        //            _bankAccountOwner);

        //            }
        //            catch (Exception ex)
        //            {
        //                Assert.Equal("bank_account_owner_kosong", ex.Message);
        //            }


        //            _bankAccountOwner = "dwi putri";
        //            _topUpDate = "2022-02-3 25:00";
        //            try
        //            {
        //                _topUpService.AddTopUp(_topUpDate,
        //               _topUpAmount,
        //             _virtualAccountID,
        //             _bankID,
        //             _bankName,
        //             _bankAccountNumber,
        //             _bankAccountOwner);

        //            }

        //            catch (Exception ex)
        //            {
        //                /*String '2022-02-3' was not recognized as a valid DateTime.*/
        //        //    Assert.Contains("was not recognized as a valid DateTime", ex.Message);
        //        //}
        //        //    */
        //        /*Positive Test */

        [Fact]

        public void TestAddTopUp_PositiveTest()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.VirtualAccountDbContext(dbOption))
            {

                TopUpService _topUpService = new TopUpService(_db);
                _topUpService.Migrate();

                //string _topUpID = "Top01";
                string _topUpDate = "2022-03-01 12:05 AM";
                int _topUpAmount = 10000;
                string _virtualAccountID = "001";
                string _bankID = "BTN01";
                string _bankName = "BTN";
                string _bankAccountNumber = "0001";
                string _bankAccountOwner = "dwi putri";

                List<Models.TopUpModel> topUps = new List<TopUpModel>();

                //_bankAccounts.Add(new BankAccountModel("B001", "001", "BTN01", "00001", "dwi putri", 100000));
                //_bankAccounts.Add(new BankAccountModel("B002", "002", "MND01", "00002", "putri dwi", 200000));

                

                var topUp = _topUpService.AddTopUp(
                    _topUpDate,
               _topUpAmount,
             _virtualAccountID,
             _bankID,
             _bankName,
             _bankAccountNumber,
             _bankAccountOwner
            );

                

                Models.TopUpModel _ftopUp = _topUpService.FindTopUpByID(topUp);
                Assert.NotNull(_ftopUp);

                if (_ftopUp != null)
                {
                    Assert.Equal(_topUpDate, _ftopUp.TopUpDate.ToString("yyyy-MM-dd hh:mm tt"));
                    Assert.Equal(_topUpAmount, _ftopUp.TopUpAmount);
                    Assert.Equal(_virtualAccountID, _ftopUp.VirtualAccountID);
                    Assert.Equal(_bankID, _ftopUp.BankID);

                    Assert.Equal(_bankName, _ftopUp.BankName);
                    Assert.Equal(_bankAccountNumber, _ftopUp.BankAccountNumber);

                    Assert.Equal(_bankAccountOwner, _ftopUp.BankAccountOwner);
                }


                //                //         // CekTopUp(string topUpID,
                //                //         string virtualAccountID,
                //                //int topUpAmount,
                //                //string bankID,
                //                //string bankAccountID,
                //                //string bankName,
                //                //string bankAccountNumber,
                //                //string bankAccountOwner,
                //                //int balance)


                //                // var _cekTopUp = _topUpService.CekTopUp("Top01", "001", 10000, "BTN01", "B001", "BTN", "00001", "dwi putri", 100000);
                //              //  var _cekTopUp = _topUpService.CekTopUp("Top01", "001", 10000, "BTN01", "B001", "BTN", "00001", "dwi putri", 100000);

            }

        }
    }
}



//        //  [Fact]
//        //public  void TestAddTopUp_SeedingTopUp()
//        //  {
//        //      var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
//        //          .UseSqlServer(TestVars.ConnectionString)
//        //          .Options;



//        //  using (var _db = new Models.VirtualAccountDbContext(dbOption))
//        //      {

//        //          TopUpService _topUpService = new TopUpService(_db, TestVars.LoginEmail);
//        //          _topUpService.Migrate();

//        //          List<TopUpModel> _topUps = new List<TopUpModel>();

//        //          _topUps.Add(new TopUpModel()
//        //          {

//        //              strTopUpDate = "2022-06-23",
//        //              TopUpAmount = 10000,
//        //              VirtualAccountID = "001",
//        //              BankID = "BTN01",
//        //              BankName = "BTN",
//        //              BankAccountNumber = "00001",
//        //              BankAccountOwner = "dwi putri",
//        //          });

//        //          for (int i = 0; i < _topUps.Count; i++)
//        //          {
//        //              var _topUp = _topUps[i];
//        //              var _virtualAccountID = _topUp.VirtualAccountID;
//        //              var _topUpAmount = _topUp.TopUpAmount;
//        //              var _topUpDate = _topUp.strTopUpDate;
//        //              var _bankID = _topUp.BankID;
//        //              var _bankName = _topUp.BankName;
//        //              var _bankAccountNumber = _topUp.BankAccountNumber;
//        //              var _bankAccountOwner = _topUp.BankAccountOwner;


//        //              var _topUpID = _topUpService.AddTopUp(
//        //                  _topUpDate,
//        //         _topUpAmount,
//        //       _virtualAccountID,
//        //       _bankID,
//        //       _bankName,
//        //       _bankAccountNumber,
//        //       _bankAccountOwner
//        //       );


//        //              Models.TopUpModel? _fTopUp = _topUpService.FindTopUpByID(_topUpID);
//        //              Assert.NotNull(_fTopUp);

//        //              if (_fTopUp != null)
//        //              {
//        //                  Assert.Equal(_virtualAccountID, _fTopUp.VirtualAccountID);
//        //                  Assert.Equal(_topUpAmount, _fTopUp.TopUpAmount);
//        //                  Assert.Equal(_topUpDate, _fTopUp.TopUpDate.ToString("yyyy-MM-dd hh:mm tt"));
//        //                  Assert.Equal(_bankID, _fTopUp.BankID);
//        //                  Assert.Equal(_bankName, _fTopUp.BankName);
//        //                  Assert.Equal(_bankAccountNumber, _fTopUp.BankAccountNumber);
//        //                  Assert.Equal(_bankAccountOwner, _fTopUp.BankAccountOwner);
//        //              }

//        //          }


//        //      }


//        //  }


//        //[Fact]
//        //void tesTopUp()
//        //{

//        //}




//    }

//}

///*

// */