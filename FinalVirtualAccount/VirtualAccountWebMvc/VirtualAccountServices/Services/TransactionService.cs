﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore.Storage;


namespace VirtualAccountServices.Services
{
    public class TransactionService : BaseService
    {

        private CultureInfo _cultureInfo;


        /*Constructors*/
        public TransactionService(Models.VirtualAccountDbContext db, string loginID) : base(db, loginID)
        {
            _cultureInfo = CultureInfo.InvariantCulture;
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE transactionn;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();

        }

        private DateTime[] parseTransactionDate(string transactionDate)
        {
            DateTime[] result = new DateTime[1];

            result[0] = DateTime.ParseExact(transactionDate, "yyyy-MM-dd hh:mm tt", _cultureInfo);
            return result;
        }

        private void validateBase(
            string transactionDate,
            string merchantAccountID,
            string virtualAccountID,
            int transactionAmount,
            string referenceID)
        {
            if (string.IsNullOrEmpty(virtualAccountID))
            {
                throw new Exception("virtual_account_id_kosong");
            }

            if (string.IsNullOrEmpty(merchantAccountID))
            {
                throw new Exception("merchant_account_id_kosong");
            }

            if (transactionAmount < 0)
            {
                throw new Exception("transaction_amount_negative");
            }

            if (string.IsNullOrEmpty(referenceID))
            {
                throw new Exception("reference_id_kosong");
            }

            if (string.IsNullOrEmpty(transactionDate))
            {
                throw new Exception("transaction_date_kosong");
            }

        }

        private void validateID(string transactionID)
        {
            if (string.IsNullOrEmpty(transactionID))
            {
                throw new Exception("transaction_id_kosong");
            }
        }

        private void setValue(ref Models.TransactionModel _transaction,
            DateTime transactionDate,
            string merchantAccountID,
            string virtualAccountID,
            int transactionAmount,
            string referenceID)

        {
            _transaction.TransactionDate = transactionDate;
            _transaction.MerchantAccountID = merchantAccountID;
            _transaction.VirtualAccountID = virtualAccountID;
            _transaction.TransactionAmount = transactionAmount;
            _transaction.ReferenceID = referenceID;
        }


        public string TambahTransaction(
            string transactionDate,
            string virtualAccountID,
            string merchantAccountID,
            int transactionAmount,
            string referenceID)
        {

            this.validateBase(
                transactionDate,
                virtualAccountID,
                merchantAccountID,
                transactionAmount,
                referenceID);

            var dt = parseTransactionDate(transactionDate);

            DateTime _transactionDate = dt[0];

            var _transaction = new Models.TransactionModel();

            setValue(ref _transaction,
                _transactionDate,
                virtualAccountID,
               merchantAccountID,
                transactionAmount,
                referenceID);

           // _transaction.KodeAkun = null;

            _db.Transactions.Add(_transaction);
            _db.SaveChanges();

            return _transaction.TransactionID;
        }

        public void UpdateTransaction(string transactionID,
            string transactionDate,
            string virtualAccountID,
            string merchantAccountID,
            int transactionAmount,
            string referenceID)
        {
            validateID(transactionID);

            this.validateBase(
                transactionDate,
                virtualAccountID,
                merchantAccountID,
                transactionAmount,
                referenceID);

            var dt = parseTransactionDate(transactionDate);

            DateTime _transactionDate = dt[0];

            //var _ftransaction = new Models.TransactionModel();

            var _ftransaction = findByTransactionID(transactionID);

            setValue(ref _ftransaction,
                _transactionDate,
                virtualAccountID,
                merchantAccountID,
                transactionAmount,
                referenceID);

           // _ftransaction.KodeAkun = null;

            _db.SaveChanges();

        }

        //        //public void DeleteTransaction(long transactionID)
        //        //{


        //        //    var _ftransaction = findByTransactionID(transactionID);
        //        //    if (_ftransaction != null)
        //        //    {
        //        //        _db.Transactions.Remove(_ftransaction);
        //        //        _db.SaveChanges();
        //        //    }
        //        //}

        public Models.TransactionModel? findByTransactionID(string transactionID)
        {
            var _ftransaction = _db.Transactions.Where(x => x.TransactionID == transactionID).FirstOrDefault();
            if (_ftransaction == null)
            {
                throw new Exception("transaction_not_found");
            }
            return _ftransaction;
        }

        public Models.TransactionModel? FindTrasanctionByID(string transactionID)
        {
            return findByTransactionID(transactionID);
        }


        public Models.TransactionModel? findFromVirtualAccountIDbyVirtualAccountID(string VirtualAccountID)
        {

            var _ftransaction = _db.Transactions.Where(x => x.VirtualAccountID == VirtualAccountID).FirstOrDefault();
            if (_ftransaction == null)
            {
                throw new Exception("virtual_account_id_not_found");
            }
            return _ftransaction;
        }


        public Models.TransactionModel? findFromVirtualAccountIDbyMerchantAccountID(string MerchantAccountID)
        {

            var _ftransaction = _db.Transactions.Where(x => x.VirtualAccountID == MerchantAccountID).FirstOrDefault();
            if (_ftransaction == null)
            {
                throw new Exception("merchant_account_id_not_found");
            }
            return _ftransaction;
        }


    }
}
//        public string Shop(string transactionID,
//            string virtualAccountID,
//            string merchantAccountID,
//            int balance,
//            int transactionAmount,
//            int transferAmount,
//            string bankAccountID
//            )
//        {
//            var _virtualAccountModel = new Models.VirtualAccountModel();
//            var _virtualAccountService = new VirtualAccountService(_db, _loginEmail);

//            var _transaction = new Models.TransactionModel();
//            _transaction.TransactionID = transactionID;

//            var _virtualAccount = _virtualAccountService.FindVirtualAccountByVirtualAccountID(virtualAccountID, false);

//            if (virtualAccountID == null)
//            {
//                throw new Exception("virtual_account_id_null");
//            }

//            else
//            {
//                var _merchantAccount = _virtualAccountService.FindVirtualAccountByVirtualAccountID(merchantAccountID, false);

//                if (merchantAccountID == null)
//                {
//                    throw new Exception("merchant_account_id_null");
//                }
//                else
//                {
//                    var _bankAccountModel = new Models.BankAccountModel();
//                    _bankAccountModel.Balance = balance;

//                    if (balance < transactionAmount)
//                    {
//                        throw new Exception("balance tidak cukup");
//                    }

//                    else
//                    {
//                        var _tr = _db.Database.BeginTransaction();
//                        try
//                        {
//                            var _transactionModel = new Models.TransactionModel();

//                            _transactionModel.TransactionID = transactionID;

//                            var _fVirtualAccount = _db.VirtualAccounts
//                                 .Include(x => x.Balance)
//                                .FirstOrDefault(s => s.VirtualAccountID == virtualAccountID);

//                            var _fMerchantAccount = _db.VirtualAccounts
//                                 .Include(x => x.Balance)
//                                .FirstOrDefault(s => s.VirtualAccountID == merchantAccountID);

//                            _fMerchantAccount.Balance = _fMerchantAccount.Balance + transactionAmount;

//                            _fVirtualAccount.Balance = _fVirtualAccount.Balance - transactionAmount;

//                            _db.VirtualAccounts.Add(_virtualAccountModel);
//                            _db.SaveChanges();

//                            var _transactionHistoryModel = new Models.TransactionHistoryModel();

//                            _transactionHistoryModel.VirtualAccountID = virtualAccountID;
//                            _transactionHistoryModel.VirtualAccountID = merchantAccountID;
//                            _transactionHistoryModel.TransactionType = "Shop";

//                            _db.TransactionHistories.Add(_transactionHistoryModel);
//                            _db.SaveChanges();

//                            _tr.Commit();
//                        }


//                         catch (Exception e)
//                        {
//                            _tr.Rollback();
//                            Console.WriteLine(e);
//                            throw;
//                        }

//                    }

//                }

//            }

//            return transactionID;

//        }


//    }
//}



//        //private void _insertTransaction(IDbContextTransaction tr,
//        //    long transactionID,
//        //    //DateTime transactionDate,
//        //    //string virtualAccountID,
//        //    //string merchantAccountID,
//        //    //int transactionAmount,
//        //    //string referenceID,
//        //    List<TransactionModel> transactions)
//        //{
//        //    foreach (var transaction in transactions)
//        //    {
//        //        try
//        //        {
//        //            var _transaction = new Models.TransactionModel();
//        //            _transaction.CreatedAt = DateTime.Now.ToUniversalTime(); //UTC
//        //            _transaction.TransactionID = transactionID;
//        //            _transaction.TransactionDate = transaction.TransactionDate;
//        //            _transaction.VirtualAccountID = transaction.VirtualAccountID;
//        //            _transaction.MerchantAccountID = transaction.MerchantAccountID;
//        //            _transaction.TransactionAmount = transaction.TransactionAmount;
//        //            _transaction.ReferenceID = transaction.ReferenceID;

//        //            _transaction.CreatedBy = _loginEmail;

//        //            _db.Transactions.Add(_transaction);
//        //            _db.SaveChanges();
//        //        }
//        //        catch (Exception e)
//        //        {
//        //            if (e.InnerException.Message.Contains("uq_transaction"))
//        //            {
//        //                throw new Exception("Transaction duplikat");
//        //            }
//        //            else
//        //            {
//        //                throw;
//        //            }
//        //        }

//        //    }
//        //}



//        //private void _insertVirtualAccount(IDbContextTransaction tr,
//        //    string virtualAccountID,
//        //    //DateTime transactionDate,
//        //    //string virtualAccountID,
//        //    //string merchantAccountID,
//        //    //int transactionAmount,
//        //    //string referenceID,
//        //    List<VirtualAccountModel> virtualAccounts)
//        //{
//        //    foreach (var virtualAccount in virtualAccounts)
//        //    {
//        //        try
//        //        {
//        //            var _virtualAccount = new Models.VirtualAccountModel();
//        //            _virtualAccount.CreatedAt = DateTime.Now.ToUniversalTime(); //UTC
//        //            _virtualAccount.VirtualAccountID = virtualAccountID;
//        //            _virtualAccount.Balance = virtualAccount.Balance;
//        //            //_virtualAccount.VirtualAccountID = transaction.VirtualAccountID;
//        //            //_virtualAccount.MerchantAccountID = transaction.MerchantAccountID;
//        //            //_virtualAccount.TransactionAmount = transaction.TransactionAmount;
//        //            //_virtualAccount.ReferenceID = transaction.ReferenceID;



//        //            _db.VirtualAccounts.Add(_virtualAccount);
//        //            _db.SaveChanges();
//        //        }
//        //        catch (Exception e)
//        //        {
//        //            if (e.InnerException.Message.Contains("uq_transaction"))
//        //            {
//        //                throw new Exception("Virtual Account duplikat");
//        //            }
//        //            else
//        //            {
//        //                throw;
//        //            }
//        //        }

//        //    }

///*
// using VirtualAccount.Models;

//namespace VirtualAccount.Services;

//public class TransactionService
//{
//    public long Shop(long virtualAccountID,
//        long merchantVirtualAccountID,
//        double amount,
//        string reference)
//    {

//        /*
//         * find virtualAccount by virtualAccountID
//         *   -jika tidak ketemu, maka erro virtualAccount not found
//         * 
//         * find merchantVirtualAccount by merchantVirtualAccountID
//         *   -jika tidak ketemu, maka error merchantVirtualAccount not found
//         *
//         * cek apakah virtualAccount.Balance >= amount
//         *  - jika tidak cukup, maka error account balance ga cukup
//         *
//         * transaction begin
//         *
//         *  insert data transaction
//         *      (transactionID)
//         * 
//         * - update balance merchant virtual account
//         *          balance = balance + amount
//         *
//         * - update balance virtual acocunt:
//         *          balance = balance - transfer amount
//         *
//         *  insert into transaction history:
//         *      -transactionID <= transactionID
//         *          - virtualAccountID <= virtualAccountID (yg belanja)
//                    -transaction type= "Shop"

//         *  insert into transaction history:
//         *      -transactionID <= transactionID
//         *      -virtualAccountID <= merchant virtual accountID
//                    -transaction type= "Shop"

//         * jika terjadi error, maka transction rollback()
//         * 
//         * jika ok, transaction commit()
//         *
//         * return transactionID
//         * 


//return 0;

//    }

//    public List<Models.Transaction> FindList(long virtualAccountID)
//{
//    return new List<Models.Transaction>();
//}
//}

// */