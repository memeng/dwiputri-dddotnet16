﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore.Storage;


namespace VirtualAccountServices.Services
{
    public class TransactionHistoryService : BaseService
    {

        private CultureInfo _cultureInfo;


        /*Constructors*/
        public TransactionHistoryService(Models.VirtualAccountDbContext db) : base(db)
        {
            _cultureInfo = CultureInfo.InvariantCulture;
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE transaction_history;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();

        }

        private DateTime[] parseHistoryDate(string historyDate)
        {
            DateTime[] result = new DateTime[1];

            result[0] = DateTime.ParseExact(historyDate, "yyyy-MM-dd hh:mm tt", _cultureInfo);
            return result;
        }

        private void validateBase(
            string virtualAccountID,
             string merchantAccountID,
            string transactionID,
            string historyDate,
                int amount,
                string transactionType)
        {
            if (string.IsNullOrEmpty(historyDate))
            {
                throw new Exception("history_date_kosong");
            }
            if (string.IsNullOrEmpty(virtualAccountID))
            {
                throw new Exception("virtual_account_id_kosong");
            }

            if (string.IsNullOrEmpty(merchantAccountID))
            {
                throw new Exception("merchant_account_id_kosong");
            }
            if (amount < 0)
            {
                throw new Exception("amount_transaksi_negative");
            }
            if (string.IsNullOrEmpty(transactionID))
            {
                throw new Exception("transaksi_id_kosong");
            }
            if (string.IsNullOrEmpty(transactionType))
            {
                throw new Exception("transaksi_type_kosong");
            }
        }

        private void validateID(string historyID)
        {
            if (string.IsNullOrEmpty(historyID))
            {
                throw new ArgumentNullException("history_id_is_invalid");
            }
        }


        private void setValue(ref Models.TransactionHistoryModel _history,
           DateTime historyDate,
           string virtualAccountID,
           string merchantAccountID,
            string transactionID,
           int amount,
           string transactionType)

        {
            _history.HistoryDate = historyDate;
            _history.TransactionID = transactionID;
            _history.Amount = amount;
            _history.VirtualAccountID = virtualAccountID;
            _history.MerchantAccountID = merchantAccountID;
            _history.TransactionType = transactionType;
        }


        public string AddHistory(string historyDate,
            string virtualAccountID,
            string merchantAccountID,
            string transactionID,
           int amount,
           string transactionType)
        {

            this.validateBase(historyDate,
             virtualAccountID,
             merchantAccountID,
            transactionID,
      
                amount,
                transactionType);

            var dt = parseHistoryDate(historyDate);

            DateTime _historyDate = dt[0];

            var _history = new Models.TransactionHistoryModel();

            setValue(ref _history,
                _historyDate,
            virtualAccountID,
            merchantAccountID,
            transactionID,
           amount,
          transactionType);


            _db.TransactionHistories.Add(_history);
            _db.SaveChanges();

            return _history.HistoryID;

        }

        public void UpdateHistory(string historyDate,
    string historyID,
            string virtualAccountID,
            string merchantAccountID,
            string transactionID,
           int amount,
           string transactionType)
        {
            validateID(historyID);

            this.validateBase(
                historyDate,
            virtualAccountID,
            merchantAccountID,
            transactionID,
            amount,
             transactionType);

            var dt = parseHistoryDate(historyDate);

            DateTime _historyDate = dt[0];

            // var _ftransaction = new Models.TransactionModel();

            var _fHistory = findByHistoryID(historyID);

            setValue(ref _fHistory,
                _historyDate,
            virtualAccountID,
            merchantAccountID,
            transactionID,
           amount,
          transactionType);

            _db.SaveChanges();

        }

        //        //public void DeleteTransfer(long transferID)
        //        //{
        //        //    validateID(transferID);

        //        //    var _ftransfer = findByTransferID(transferID);
        //        //    if (_ftransfer != null)
        //        //    {
        //        //        _db.Transfers.Remove(_ftransfer);
        //        //        _db.SaveChanges();
        //        //    }
        //        //}

        public Models.TransactionHistoryModel? findByHistoryID(string historyID)
        {
            /*
             * LINQ
             * First => temukan yang pertama, jika tidak ketemu error
             * FirstOrDefault => temukan yang pertama, jika tidak ketemu return null             
             */
            /*var _fSchedule = _schedules.Where(x => x.ScheduleID == scheduleID).FirstOrDefault();*/
            var _fhistory = _db.TransactionHistories.Where(x => x.HistoryID == historyID).FirstOrDefault();
            if (_fhistory == null)
            {
                throw new Exception("history_not_found");
            }
            return _fhistory;
        }


        public Models.TransactionHistoryModel? FindByHistoryID(string historyID)
        {
            return findByHistoryID(historyID);
        }

        public List<Models.TransactionHistoryModel> GetHistory()
        {
            return _db.TransactionHistories.ToList();
        }


    }
}
