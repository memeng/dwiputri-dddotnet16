﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAccountServices.Services
{
    public class BaseService
    {
        protected Models.VirtualAccountDbContext _db;

        protected string _loginEmail = String.Empty;


        public BaseService(Models.VirtualAccountDbContext db)
        {
            _db = db;
        }
        public BaseService(Models.VirtualAccountDbContext db,
            string loginEmail)
        {
            _db = db;
            _loginEmail = loginEmail;
        }

    }
}
