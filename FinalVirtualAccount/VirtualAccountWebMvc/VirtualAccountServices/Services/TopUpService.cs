﻿using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;




namespace VirtualAccountServices.Services
{
    public class TopUpService : BaseService
    {

        private CultureInfo _cultureInfo;

        public TopUpService(Models.VirtualAccountDbContext db) : base(db)
        {
            _cultureInfo = CultureInfo.InvariantCulture;

        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE top_up;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        private DateTime[] parseTopUpDate(string topUpDate)
        {
            DateTime[] result = new DateTime[1];

            //_jamKedatangan
            result[0] = DateTime.ParseExact(topUpDate, "yyyy-MM-dd hh:mm tt", _cultureInfo);
            return result;
        }

        private void validateBase(
            string topUpDate,
            int topUpAmount,
            string virtualAccountID,
            string bankID,
            string bankName,
            string bankAccountNumber,
            string bankAccountOwner)
            
        {
            if (string.IsNullOrEmpty(topUpDate))
            {
                throw new ArgumentNullException("top_up_date_null");
            }
            if (string.IsNullOrEmpty(virtualAccountID))
            {
                throw new ArgumentNullException("virtual_account_id_is_null");
            }

            if (string.IsNullOrEmpty(bankName))
            {
                throw new ArgumentNullException("bank_name_is_null");
            }
            if (string.IsNullOrEmpty(bankID))
            {
                throw new ArgumentNullException("bank_id_is_null");
            }
            if (string.IsNullOrEmpty(bankAccountNumber))
            {
                throw new ArgumentNullException("bank_account_number_is_null");
            }

            if (string.IsNullOrEmpty(bankAccountOwner))
            {
                throw new ArgumentNullException("bank_account_owner_is_null");
            }
            if (topUpAmount < 0)
            {
                throw new ArgumentNullException("top_up_amount_is_negatife");

            }
        }

        private void validateID(string topUpID)
        {
            if (string.IsNullOrEmpty(topUpID))
            {
                throw new ArgumentNullException("top_up_id_is_invalid");
            }
        }

        private void setValue(ref Models.TopUpModel _topUp,
           
            DateTime topUpDate,
            int topUpAmount,
            string virtualAccountID,
            string bankID,
            string bankName,
            string bankAccountNumber,
            string bankAccountOwner)
        {
            _topUp.TopUpAmount = topUpAmount;
            _topUp.VirtualAccountID = virtualAccountID;
            _topUp.TopUpDate = topUpDate;
            _topUp.BankID = bankID;
            _topUp.BankName = bankName;
            _topUp.BankAccountNumber = bankAccountNumber;
            _topUp.BankAccountOwner = bankAccountOwner;

        }

        public string AddTopUp(
            string topUpDate,
            int topUpAmount,
            string virtualAccountID,
            string bankID,
            string bankName,
            string bankAccountNumber,
            string bankAccountOwner
            )
        {

            this.validateBase(
                topUpDate,
               topUpAmount,
             virtualAccountID,
             bankID,
             bankName,
             bankAccountNumber,
             bankAccountOwner);

            var dt = parseTopUpDate(topUpDate);

            DateTime _topUpDate = dt[0];

            var _topUp = new Models.TopUpModel();

            setValue(ref _topUp,
                _topUpDate,
               topUpAmount,
             virtualAccountID,
             bankID,
             bankName,
             bankAccountNumber,
             bankAccountOwner);

            //_topUp.KodeAkun = null;
            //_topUp.KodeBank = null;
            //_topUp.NamaBank = null;
            //_topUp.NomorRekening = null;
            //_topUp.PemilikRekening = null;

            _db.TopUps.Add(_topUp);
            _db.SaveChanges();

            return _topUp.TopUpID;

        }

        public void UpdateTopUp(string topUpID,
            string topUpDate,
            int topUpAmount,
            string virtualAccountID,
            string bankID,
            string bankName,
            string bankAccountNumber,
            string bankAccountOwner
            )
        {
            validateID(topUpID);


            this.validateBase(
                topUpDate,
                topUpAmount,
              virtualAccountID,
              bankID,
              bankName,
              bankAccountNumber,
              bankAccountOwner);

            var dt = parseTopUpDate(topUpDate);

            DateTime _topUpDate = dt[0];

            var _ftopUp = findTopUpByID(topUpID);

            setValue(ref _ftopUp,
              _topUpDate,
             topUpAmount,
           virtualAccountID,
           bankID,
           bankName,
           bankAccountNumber,
           bankAccountOwner);

            //_ftopUp.KodeAkun = null;
            //_ftopUp.KodeBank = null;
            //_ftopUp.NamaBank = null;
            //_ftopUp.NomorRekening = null;
            //_ftopUp.PemilikRekening = null;

            _db.SaveChanges();

        }
        //public void DeleteTopUp(long topUpID)
        //{
        //    validateID(topUpID);

        //    var _ftopUp = findTopUpByID(topUpID);
        //    if (_ftopUp != null)
        //    {
        //        _db.TopUps.Remove(_ftopUp);
        //        _db.SaveChanges();
        //    }
        //}




        public Models.TopUpModel? findTopUpByID(string topUpID)
        {
            var _ftopUp = _db.TopUps.Where(x => x.TopUpID == topUpID).FirstOrDefault();
            if (_ftopUp == null)
            {

                throw new Exception("top_up_not_found");
            }
            return _ftopUp;
        }


        public Models.TopUpModel? FindTopUpByID(string topUpID)
        {
            return findTopUpByID(topUpID);
        }

        //public List<Models.TopUpModel> GetTopUps()
        //{
        //    return _db.TopUps.
        //        Include("KodeAkun").
        //        Include("KodeBank").
        //        Include("PemilikRekening").
        //        Include("NomorRekening").
        //        Include("NamaBank").
        //        ToList();

        //}

        //public List<Models.TopUpModel> FindVirtualAccountList(string virtualAccountID)
        //{
        //    return new List<TopUpModel>();
        //}

        public string CekTopUp(string topUpID,
            string virtualAccountID,
       int topUpAmount,
       string bankID,
       string bankAccountID,
       string bankName,
       string bankAccountNumber,
       string bankAccountOwner,
       int balance)
        {

            var _bankAccountService = new BankAccountService(_db, _loginEmail);

            var _findbankAccount = _bankAccountService.FindByBankAccountID(bankAccountID);

            // var _topUp = new Models.TopUpModel();

            if (bankAccountID == null)
            {
                throw new Exception("invalid_bank_account_id");
            }

            else
            {

                var _bankAccountModel = new Models.BankAccountModel();

                _bankAccountModel.BankAccountNumber = bankAccountNumber;
                _bankAccountModel.BankAccountOwner = bankAccountOwner;
                _bankAccountModel.BankID = bankID;

                _db.BankAccounts.Add(_bankAccountModel);
                _db.SaveChanges();

                _bankAccountModel.Balance = balance;

                if (balance >= topUpAmount)
                {
                    throw new Exception("saldo tidak cukup");
                }
                else
                {
                    var _tr = _db.Database.BeginTransaction();
                    try
                    {
                        var _topUp = new Models.TopUpModel();

                        _topUp.TopUpID = topUpID;
                        _topUp.TopUpAmount = topUpAmount;
                        _topUp.VirtualAccountID = virtualAccountID;
                        //_topUp.TopUpDate = topUpDate;
                        _topUp.BankID = bankID;
                        _topUp.BankName = bankName;
                        _topUp.BankAccountNumber = bankAccountNumber;
                        _topUp.BankAccountOwner = bankAccountOwner;

                        _db.TopUps.Add(_topUp);
                        _db.SaveChanges();

                        //var _transactionHistory = new Models.TransactionHistoryModel();

                        //_transactionHistory.TransactionID = topUpID;
                        //_transactionHistory.TransactionType = "TopUp";

                        var _fBankAccount = _db.BankAccounts
                         .Include(x => x.Balance)
                         .FirstOrDefault(s => s.BankAccountID == bankAccountID);

                        _fBankAccount.Balance = _fBankAccount.Balance - topUpAmount;

                        _db.BankAccounts.Add(_bankAccountModel);
                        _db.SaveChanges();



                        var _virtualAccountService = new VirtualAccountService(_db, _loginEmail);
                        if (virtualAccountID == null)
                        {
                            throw new Exception("virtual_account_id_invalid");
                        }
                        else
                        {
                            var _fVirtualAccount = _db.BankAccounts
                             .Include(x => x.Balance)
                            .FirstOrDefault(s => s.BankAccountID == bankAccountID);

                            _fVirtualAccount.Balance += topUpAmount;

                        }
                        _tr.Commit();

                    }

                    catch (Exception e)
                    {
                        _tr.Rollback();
                        Console.WriteLine(e);
                        throw;
                    }

                    _db.SaveChanges();
                    return topUpID;
                }

            }

        }

                //private void _insertBankAccounts(IDbContextTransaction tr,
                //    string bankAccountID,
                //    List<BankAccountModel> bankAccounts)
                //{
                //    foreach (var bankAccount in bankAccounts)
                //    {
                //        try
                //        {
                //            var _bankAccount = new Models.BankAccountModel();

                //            _bankAccount.BankAccountID = bankAccountID;
                //            _bankAccount.BankAccountNumber = _bankAccount.BankAccountNumber;
                //            _bankAccount.BankAccountOwner = _bankAccount.BankAccountOwner;


                //            _db.BankAccounts.Add(_bankAccount);
                //            _db.SaveChanges();
                //        }
                //        catch (Exception e)
                //        {
                //            if (e.InnerException.Message.Contains("uq_bank_account"))
                //            {
                //                throw new Exception("Bank Account duplikat");
                //            }
                //            else
                //            {
                //                throw;
                //            }
                //        }

                //    }
                //}






            }
}
///*
// ﻿using VirtualAccount.Models;

//namespace VirtualAccount.Services;

//public class TopUpService
//{
//    public long TopUp(long virtualAccountID, 
//        double topUpAmount,
//        long bankAccountID)
//    {
//        /*
//         * find bank account by bankAccountID
//         * jika tidak ketemu, maka bank account found
//         * jika ketemu:
//         *   - mapping data bank account ke model topup:
//         *          public string BankCode { get; set; }
//                    public string BankAccountNumber { get; set; }
//                    public string BankAccountOwner { get; set; }
        
//             - cek apakah saldo bank account >= topUp Amount 
//                - jika kurang, maka return error => saldo bank tidak cukup
//                - jika lebih atau sama dengan, lanjutkan
        
//             - pakai transaction begin disini
//             - isi data topup
                                 
//             - isi data transaction history 
//                    -transactionID <= topUpID
//                    -transaction type= "TopUp"
                    
//             - potong saldo bank account tsb    
//                    -  fBankAccount.Balance =  fBankAccount.Balance - topUpAmount
//                        where bankAccountID= ? 
            
//             - tambah saldo di virtual account :
//                    - find virtual account:
//                        jika tidak ketemu, maka error , virtual account not found
//                        jika ketemu, maka fVirtualAccount.Balance += topUpAmount
                                        
//             - kalau terjadi error, maka transaction rollback()
             
//             - kalau tidak terjadi error, transaction commit (diakhir) 
             
//            return topUpID
         

//return 0;
//    }

//    public List<TopUp> FindList(long virtualAccountID)
//{
//    return new List<TopUp>();
//}
    
//}
 
//  private void _insertAirportGateways(IDbContextTransaction tr,
//            string kodeAirport,
//            List<AirportGatewayModel> gateways)
//        {
//            foreach (var gateway in gateways)
//            {
//                try
//                {
//                    var _airportGateway = new Models.AirportGatewayModel();
//                    _airportGateway.CreatedAt = DateTime.Now.ToUniversalTime(); //UTC
//                    _airportGateway.KodeAirport = kodeAirport;
//                    _airportGateway.NomorGate = gateway.NomorGate;
//                    _airportGateway.NomorPintu = gateway.NomorPintu;
//                    _airportGateway.CreatedAt = DateTime.Now.ToUniversalTime();
//                    _airportGateway.CreatedBy = _loginEmail;

//                    _db.AirportGateways.Add(_airportGateway);
//                    _db.SaveChanges();
//                }
//                catch (Exception e)
//                {
//                    if (e.InnerException.Message.Contains("uq_airport_gateway"))
//                    {
//                        throw new Exception("Gateway duplikat");
//                    }
//                    else
//                    {
//                        throw;
//                    }
//                }
                
//            }
//        }


// */