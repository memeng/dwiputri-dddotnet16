﻿using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.ComponentModel.Design;

namespace VirtualAccountServices.Services
{
    public class RegisterService : BaseService
    {
        public RegisterService(VirtualAccountDbContext db) : base(db)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE register;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }


        private void validateBase(string email,
            string password,
            string confirmPassword,
            string username,
            string mobilephone)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("email_is_required");
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("password_is_required");
            }
            if (string.IsNullOrEmpty(confirmPassword))
            {
                throw new Exception("confirm_password_is_required");
            }
            if (!password.Equals(confirmPassword))
            {
                throw new Exception("password_must_be_matched_with_confirm_password");
            }
            if (string.IsNullOrEmpty(username))
            {
                throw new Exception("username_is_required");
            }

            if (string.IsNullOrEmpty(mobilephone))
            {
                throw new Exception("mobilephone_is_required");
            }
        }

        private void setValue(ref Models.RegisterModel _register,
            string email,
            string password,
            string username,
            bool active,
            string mobilephone
            )
        {
            _register.Email = email;
            _register.MobilePhone = mobilephone;
            var _hashPass = HashService.GenerateHash(password);
            _register.Password = _hashPass;
            _register.Username = username;
            _register.Active = active;

        }

      

        public void UpdateRegister(string email,
            string username,
            bool active)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("email_is_required");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new Exception("username_is_required");
            }

            var _register = _findByEmail(email, true);
            if (_register != null)
            {
               // _register.UpdatedAt = DateTime.Now.ToUniversalTime();
               // _register.UpdatedBy = _loginEmail;
                _register.Username = username;
                _register.Active = active;
                _db.SaveChanges();
            }
        }


            public Models.RegisterModel AddRegister(string email,
            string password,
            string confirmPassword,
            string username,
            bool active,
            string mobilephone)
        {
            validateBase(email,
                password,
                confirmPassword,
                username,
                mobilephone);

            var _register = new Models.RegisterModel();
            _register.CreatedAt = DateTime.Now.ToUniversalTime();
           // _register.CreatedBy = _loginEmail;

            setValue(ref _register,
                email,
                password,
                username,
                active,
                mobilephone);

            _db.Registers.Add(_register);
            
            try
            {
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                if (e.InnerException.Message.Contains("duplicate key"))
                {
                    throw new Exception("Email sudah terdaftar");
                }
                else
                {
                    throw e;
                }
            }
            return _register;

        }

        private Models.RegisterModel _findByEmail(string email, bool Tracking)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException("email_is_required");
            }

            RegisterModel _fUser;

            if (Tracking)
            {
                _fUser = _db.Registers
                    .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            }
            else
            {
                _fUser = _db.Registers.AsNoTracking()
                    .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            }


            if (_fUser == null)
            {
                throw new Exception("Invalid User");
            }
            return _fUser;
        }

        public Models.RegisterModel FindByEmail(string email, bool Tracking)
        {
            return _findByEmail(email, Tracking);
        }

        public List<Models.RegisterModel> FindList()
        {
            return _db.Registers.AsNoTracking().ToList();
        }

    }

}
