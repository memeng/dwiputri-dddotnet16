﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore.Storage;


namespace VirtualAccountServices.Services
{
    public class WithdrawService : BaseService
    {

        private CultureInfo _cultureInfo;


        /*Constructors*/
        public WithdrawService(Models.VirtualAccountDbContext db) : base(db)
        {
            _cultureInfo = CultureInfo.InvariantCulture;
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE withdraw;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();

        }

        private DateTime[] parseWithdrawDate(string withdrawDate)
        {
            DateTime[] result = new DateTime[1];

            result[0] = DateTime.ParseExact(withdrawDate, "yyyy-MM-dd hh:mm tt", _cultureInfo);
            return result;
        }

        private void validateBase(string withdrawID,
            string virtualAccountID,
    int withdrawAmount,
    string withdrawDate,
    string bankID,
    string bankName,
    string bankAccountNumber,
    string bankAccountOwner)
        {

            if (string.IsNullOrEmpty(withdrawID))
            {
                throw new Exception("withdraw_id_kosong");
            }

            if (string.IsNullOrEmpty(bankID))
            {
                throw new Exception("bank_id_kosong");
            }

            if (string.IsNullOrEmpty(bankName))
            {
                throw new Exception("bank_name_kosong");
            }

            if (string.IsNullOrEmpty(bankAccountOwner))
            {
                throw new Exception("bank_account_owner_kosong");
            }

            if (string.IsNullOrEmpty(virtualAccountID))
            {
                throw new Exception("virtual_account_id_kosong");
            }

            if (string.IsNullOrEmpty(withdrawDate))
            {
                throw new Exception("withdraw_date_kosong");
            }
            if (withdrawAmount < 0)
            {
                throw new Exception("withdraw_amount_negative");
            }
            if (string.IsNullOrEmpty(bankAccountNumber))
            {
                throw new Exception("bank_account_number_null");
            }

        }

        //private void validateID(string withdrawID)
        //{
        //    if (withdrawID == null)
        //    {
        //        throw new Exception("invalid_withdraw_id");
        //    }
        //}

        private void setValue(ref Models.WithdrawModel _withdraw,
            string withdrawID,
          string virtualAccountID,
    int withdrawAmount,
    DateTime withdrawDate,
    string bankID,
    string bankName,
    string bankAccountNumber,
    string bankAccountOwner)

        {
            _withdraw.WithdrawID = withdrawID;
            _withdraw.VirtualAccountID = virtualAccountID;
            _withdraw.WithdrawAmount = withdrawAmount;
            _withdraw.WithdrawDate = withdrawDate;
            _withdraw.BankID = bankID;
            _withdraw.BankName = bankName;
            _withdraw.BankAccountNumber = bankAccountNumber;
            _withdraw.BankAccountOwner = bankAccountOwner;
        }


        public void TambahWithdraw(string withdrawID,
            string virtualAccountID,
    int withdrawAmount,
    string withdrawDate,
    string bankID,
    string bankName,
    string bankAccountNumber,
    string bankAccountOwner)
        {

            this.validateBase(withdrawID,
                virtualAccountID,
    withdrawAmount,
    withdrawDate,
    bankID,
    bankName,
    bankAccountNumber,
    bankAccountOwner);

            var dt = parseWithdrawDate(withdrawDate);

            DateTime _withdrawDate = dt[0];

            var _withdraw = new Models.WithdrawModel();

            setValue(ref _withdraw,
                withdrawID,
                virtualAccountID,
    withdrawAmount,
    _withdrawDate,
    bankID,
    bankName,
    bankAccountNumber,
    bankAccountOwner);

            var _tr = _db.Database.BeginTransaction();

            try
            {

                _db.Withdraws.Add(_withdraw);
                _db.SaveChanges();

                _tr.Commit();
            }

            catch (Exception e)
            {
                _tr.Rollback();
                if (e.InnerException.Message.Contains("PK_withdraw"))
                {
                    throw new Exception("Kode withdraw duplikat");
                }
                else if (e.InnerException.Message.Contains("uq_withdraw_withdraw_amount"))
                {
                    throw new Exception("withdraw amount duplikat");
                }
                else
                {
                    throw;
                }
            }
        }

        public void UpdateWithdraw(string withdrawID,
           string virtualAccountID,
    int withdrawAmount,
    string withdrawDate,
    string bankID,
    string bankName,
    string bankAccountNumber,
    string bankAccountOwner)
        {

            //validateID(withdrawID);

            this.validateBase(withdrawID,
                virtualAccountID,
    withdrawAmount,
    withdrawDate,
    bankID,
    bankName,
    bankAccountNumber,
    bankAccountOwner);

            var dt = parseWithdrawDate(withdrawDate);

            DateTime _withdrawDate = dt[0];

            // var _ftransaction = new Models.TransactionModel();

            var _fwithdraw = findByWithdrawID(withdrawID);

            if (_fwithdraw != null)
            {

                var _tr = _db.Database.BeginTransaction();

                try
                {

                    _fwithdraw.WithdrawID = withdrawID;
                    _fwithdraw.VirtualAccountID = virtualAccountID;
                    _fwithdraw.WithdrawAmount = withdrawAmount;
                    _fwithdraw.WithdrawDate = _withdrawDate;
                    _fwithdraw.BankID = bankID;
                    _fwithdraw.BankName = bankName;
                    _fwithdraw.BankAccountNumber = bankAccountNumber;
                    _fwithdraw.BankAccountOwner = bankAccountOwner;


                    _db.SaveChanges();

                    _tr.Commit();
                }
                catch (Exception e)
                {
                    _tr.Rollback();
                    Console.WriteLine(e);
                    throw;
                }

            }

            setValue(ref _fwithdraw,
                withdrawID,
                 virtualAccountID,
    withdrawAmount,
    _withdrawDate,
    bankID,
    bankName,
    bankAccountNumber,
    bankAccountOwner);

            _db.SaveChanges();

        }

        //public void DeleteWithdraw(string withdrawID)
        //{
        //    validateID(withdrawID);

        //    var _fwithdraw = findByWithdrawID(withdrawID);
        //    if (_fwithdraw != null)
        //    {
        //        _db.Withdraws.Remove(_fwithdraw);
        //        _db.SaveChanges();
        //    }
        //}

        public Models.WithdrawModel? findByWithdrawID(string withdrawID)
        {

            var _fwithdraw = _db.Withdraws.Where(x => x.WithdrawID == withdrawID).FirstOrDefault();
            if (_fwithdraw == null)
            {
                throw new Exception("withdraw_not_found");
            }
            return _fwithdraw;
        }


        public Models.WithdrawModel FindWithdrawByID(string withdrawID, bool asNoTracking)
        {
            if (string.IsNullOrEmpty(withdrawID))
            {
                throw new ArgumentNullException("withdraw_id_is_null");
            }
            var _fWithdraw = _db.Withdraws.
                //Include("Gateways").
                AsNoTracking().
                FirstOrDefault(x => x.WithdrawID.ToLower().Equals(withdrawID));

            if (_fWithdraw == null)
            {
                throw new NullReferenceException("withdraw_not_found");
            }
            return _fWithdraw;
        }

        public List<Models.WithdrawModel> GetWithdraws()
        {
            return _db.Withdraws.ToList();
        }
    }
}

//        public string CekWithdraw(string withdrawID,
//            string virtualAccountID,
//            string bankAccountID,
//            string bankAccountNumber,
//            string bankAccountOwner, 
//            string bankID, 
//            int withdrawAmount,
//            DateTime withdrawDate,
//            string bankName,
//            int balance
//            )
//        {

//            var bankAccountService = new BankAccountService(_db, _loginEmail);

//            var _findBankAccount = bankAccountService.FindByBankAccountID(bankAccountID);

//            if (bankAccountID == null)
//            {
//                throw new Exception("invalid_bank_account_id");
//            }

//            else

//            {
//                var _bankAccountModel = new Models.BankAccountModel();

//                _bankAccountModel.BankAccountNumber = bankAccountNumber;
//                _bankAccountModel.BankAccountOwner = bankAccountOwner;
//                _bankAccountModel.BankID = bankID;

//                _db.BankAccounts.Add(_bankAccountModel);
//                _db.SaveChanges();

//                _bankAccountModel.Balance = balance;

//                var _tr = _db.Database.BeginTransaction();
//                try
//                {
//                    var _withdraw = new Models.WithdrawModel();
//                    _withdraw.WithdrawAmount = withdrawAmount;
//                    _withdraw.VirtualAccountID = virtualAccountID;
//                    _withdraw.WithdrawDate = withdrawDate;
//                    _withdraw.BankID = bankID;
//                    _withdraw.BankName = bankName;
//                    _withdraw.BankAccountNumber = bankAccountNumber;
//                    _withdraw.BankAccountOwner = bankAccountOwner;

//                    var _transactionHistory = new Models.TransactionHistoryModel();

//                    _transactionHistory.TransactionID = withdrawID;
//                    _transactionHistory.TransactionType = "withdraw";

//                    var _fBankAccount = _db.BankAccounts
//                    .Include(x => x.Balance)
//                    .FirstOrDefault(s => s.BankAccountID == bankAccountID);

//                    _fBankAccount.Balance = _fBankAccount.Balance + withdrawAmount;

//                    var virtualAccountService = new VirtualAccountService(_db, _loginEmail);

//                    var _findVirtualAccount = virtualAccountService.FindVirtualAccountByVirtualAccountID(virtualAccountID, false);

//                    if (virtualAccountID == null)
//                    {
//                        throw new Exception("invalid_virtual_account_id");
//                    }

//                    else
//                    {
//                        var _fVirtualAccount = _db.VirtualAccounts
//                    .Include(x => x.Balance)
//                    .FirstOrDefault(s => s.VirtualAccountID == virtualAccountID);

//                        _fVirtualAccount.Balance -= withdrawAmount;
//                    }

//                    if (balance >= withdrawAmount)
//                    {
//                        _db.Withdraws.Add(_withdraw);
//                        _db.SaveChanges();

//                        var _fWithdraw = _db.BankAccounts
//                    .Include(x => x.Balance)
//                    .FirstOrDefault(s => s.BankAccountID == bankAccountID);

//                        _fWithdraw.Balance += withdrawAmount;

//                    }
//                }

//                catch (Exception e)
//                {
//                    _tr.Rollback();
//                    Console.WriteLine(e);
//                    throw;
//                }
//                _tr.Commit();
//            }
//                _db.SaveChanges();

//                return withdrawID;
//        }

//    }
//}

///*
// namespace VirtualAccount.Services;

//public class WithdrawService
//{
//    public long Withdraw(long virtualAccountID, 
//        double withdrawAmount,
//        long bankAccountID)
//    {
//        /*
//         * find bank account by bankAccountID
//         * jika tidak ketemu, maka bank account found
//         * jika ketemu:
//         *   - mapping data bank account ke model topup:
//         *          public string BankCode { get; set; }
//                    public string BankAccountNumber { get; set; }
//                    public string BankAccountOwner { get; set; }



//             - pakai transaction begin disini
//             - isi data withdraw

//             - isi data transaction history 
//                    -transactionID <= withdrawID
//                    -transaction type= "Withdraw"

//             - tambah saldo bank account tsb    
//                    -  fBankAccount.Balance =  fBankAccount.Balance + withdrawAmount
//                        where bankAccountID= ? 

//             - kurang saldo di virtual account :
//                    - find virtual account:
//                        jika tidak ketemu, maka error , virtual account not found
//                        jika ketemu, maka fVirtualAccount.Balance -= withdrawAmount

//             - kalau terjadi error, maka transaction rollback()

//             - kalau tidak terjadi error, transaction commit (diakhir) 

//            return withdrawID


//return 0;
//    }

//    public List<Models.Withdraw> FindList(long virtualAccountID)
//{
//    return new List<Models.Withdraw>();
//}
//}

// */