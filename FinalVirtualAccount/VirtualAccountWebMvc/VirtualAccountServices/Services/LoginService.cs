﻿using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;


namespace VirtualAccountServices.Services
{
    public class LoginService : BaseService
    {
        public LoginService(VirtualAccountDbContext db,
            string loginEmail) : base(db, loginEmail)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE login;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }




        private string _generateToken(string email)
        {
            var rawToken = email + Helpers.IDHelper.ID().ToString();
            return HashService.GenerateHash(rawToken);
        }


        public Models.LoginModel _addLogin(string email)
        {

            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("invalid_admin_user");
            }

            var _Login = new Models.LoginModel();
            _Login.CreatedAt = DateTime.Now.ToUniversalTime();
            _Login.LoginID = _generateToken(email);
            _Login.Email = email;
            _Login.Expired = false;

            _db.Logins.Add(_Login);
            _db.SaveChanges();

            return _Login;
        }

        public Models.LoginModel FindByLoginID(string loginID)
        {
            var _login = _db.Logins.Where(x => x.LoginID == loginID)
                .OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
            if (_login == null)
            {
                throw new Exception("invalid_login_session");
            }
            return _login;
        }

        private Models.LoginModel? _findByemail(string email)
        {
            return _db.Logins.Where(x => x.Email == email)
                .OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
        }

        public Models.LoginModel Login(string email,
            string password)
        {

            RegisterService _registerService = new RegisterService(_db);

            var _register = _registerService.FindByEmail(email, false);

            if (!HashService.CompareHash(password, _register.Password))
            {
                throw new Exception("invalid_password");
            }

            var _fLogin = _findByemail(email);
            if (_fLogin == null)
            {
                var _login = _addLogin(email);
                return _login;

            }
            else
            {
                if (_fLogin.Expired == false)
                {
                    return _fLogin;

                }
                else
                {
                    var _login = _addLogin(email);
                    return _login;
                }

            }

        }


        public void Logout(string loginID)
        {
            var _login = FindByLoginID(loginID);
            if (_login != null)
            {
                _login.Expired = true;
                _db.SaveChanges();

            }
        }


    }
}
