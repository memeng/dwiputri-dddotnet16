﻿using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;

namespace VirtualAccountServices.Services
{
    public class TransferService : BaseService
    {

        private CultureInfo _cultureInfo;


        /*Constructors*/
        public TransferService(Models.VirtualAccountDbContext db) : base(db)
        {
            _cultureInfo = CultureInfo.InvariantCulture;
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE transferr;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();

        }

        private DateTime[] parseTransferDate(string transferDate)
        {
            DateTime[] result = new DateTime[1];

            result[0] = DateTime.ParseExact(transferDate, "yyyy-MM-dd hh:mm tt", _cultureInfo);
            return result;
        }

        private void validateBase(string transferDate,
                string fromVirtualAccountID,
                string toVirtualAccountID,
                int transferAmount)
        {

            if (string.IsNullOrEmpty(transferDate))
            {
                throw new Exception("transfer_date_kosong");
            }
            if (string.IsNullOrEmpty(fromVirtualAccountID))
            {
                throw new Exception("from_virtual_account_id_kosong");
            }

            if (string.IsNullOrEmpty(toVirtualAccountID))
            {
                throw new Exception("to_virtual_account_id_kosong");
            }
            if (transferAmount < 0)
            {
                throw new Exception("transfer_amount_negative");
            }


        }

        private void validateID(string transferID)
        {
            if (string.IsNullOrEmpty(transferID))
            {
                throw new Exception("invalid_transfer_id");
            }
        }

        private void setValue(ref Models.TransferModel _transfer,
           DateTime transferDate,
    string fromVirtualAccountID,
    string toVirtualAccountID,
    int transferAmount)

        {
            _transfer.TransferDate = transferDate;
            _transfer.FromVirtualAccountID = fromVirtualAccountID;
            _transfer.ToVirtualAccountID = toVirtualAccountID;
            _transfer.TransferAmount = transferAmount;
        }


        public string AddTransfer(string transferDate,
                string fromVirtualAccountID,
                string toVirtualAccountID,
                  int transferAmount)
        {

            this.validateBase(transferDate,
                 fromVirtualAccountID,
                toVirtualAccountID,
                  transferAmount);

            var dt = parseTransferDate(transferDate);

            DateTime _transferDate = dt[0];

            var _transfer = new Models.TransferModel();

            setValue(ref _transfer,
                _transferDate,
                fromVirtualAccountID,
                toVirtualAccountID,
                  transferAmount);


            _db.Transfers.Add(_transfer);
            _db.SaveChanges();

            return _transfer.TransferID;

        }

        public void UpdateTransfer(string transferID,
            string transferDate,
                string fromVirtualAccountID,
                string toVirtualAccountID,
                 int transferAmount)
        {

            validateID(transferID);

            this.validateBase(transferDate,
                fromVirtualAccountID,
                toVirtualAccountID,
                transferAmount);

            var dt = parseTransferDate(transferDate);

            DateTime _transferDate = dt[0];

            // var _ftransaction = new Models.TransactionModel();

            var _ftransfer = findByTransferID(transferID);

            setValue(ref _ftransfer,
                _transferDate,
                fromVirtualAccountID,
                toVirtualAccountID,
                transferAmount);

            _db.SaveChanges();

        }

        //        public void DeleteTransfer(string transferID)
        //        {
        //            validateID(transferID);

        //            var _ftransfer = findByTransferID(transferID);
        //            if (_ftransfer != null)
        //            {
        //                _db.Transfers.Remove(_ftransfer);
        //                _db.SaveChanges();
        //            }
        //        }

        public Models.TransferModel? findByTransferID(string transferID)
        {

            var _ftransfer = _db.Transfers.Where(x => x.TransferID == transferID).FirstOrDefault();
            if (_ftransfer == null)
            {
                throw new Exception("transfer_not_found");
            }
            return _ftransfer;
        }


        public Models.TransferModel? FindFromVirtualAccountIDByVirtualAccountID(string fromVirtualAccountID)
        {
            return findFromVirtualAccountIDByVirtualAccountID(fromVirtualAccountID);
        }

        public Models.TransferModel? FindtoVirtualAccountIDByVirtualAccountID(string toVirtualAccountID)
        {
            return findtoVirtualAccountIDByVirtualAccountID(toVirtualAccountID);
        }

        public List<Models.TransferModel> GetTransfer()
        {
            return _db.Transfers.ToList();
        }


        public Models.TransferModel? findFromVirtualAccountIDByVirtualAccountID(string fromVirtualAccountID)
        {

            var _ftransfer = _db.Transfers.Where(x => x.FromVirtualAccountID == fromVirtualAccountID).FirstOrDefault();
            if (_ftransfer == null)
            {
                throw new Exception("from_virtual_account_not_found");
            }
            return _ftransfer;
        }


        public Models.TransferModel? findtoVirtualAccountIDByVirtualAccountID(string toVirtualAccountID)
        {
            var _ftransfer = _db.Transfers.Where(x => x.ToVirtualAccountID == toVirtualAccountID).FirstOrDefault();
            if (_ftransfer == null)
            {
                throw new Exception("to_virtual_account_not_found");
            }
            return _ftransfer;
        }


        public Models.TransferModel? FindTransferByID(string transferID)
        {
            return findByTransferID(transferID);
        }

    }
}





//        public string Transfer(string transferID,
//            string fromVirtualAccountID,
//            string toVirtualAccountID)
//            {

//            var _transferService = new TransferService(_db, _loginEmail);

//            var _transfer = _transferService.findFromVirtualAccountIDByVirtualAccountID(fromVirtualAccountID);

//            var _transfer2 = _transferService.FindtoVirtualAccountIDByVirtualAccountID(toVirtualAccountID);

//            if (fromVirtualAccountID == null)
//            {
//                throw new Exception("fromVirtualAccountID_is_not_found");
//            }
//            else
//            {
//                if (toVirtualAccountID == null)
//                {
//                    throw new Exception("toVirtualAccountID_is_not_found");
//                }



//                else
//                {

//                }
//            }
//            return transferID;

//        }

//    }
//}
///*
// namespace VirtualAccount.Services;

//public class TransferService
//{
//    public long Transfer(long fromVirtualAccountID,
//        long toVirtualAccountID,
//        double transferAmount)
//    {

//        /*
//         * find fromVirtualAccount byVirtualAccountID
//         *   -jika tidak ketemu, maka error fromVirtualAccount not found
//         * 
//         * find toVirtualAccount by toVirtualAccountID
//         *   -jika tidak ketemu, maka error toVirtualAccount not found
//         *
//         * cek apakah fromVirtualAccount.Balance >= transferAmount
//         *  - jika tidak cukup, maka error from account balance ga cukup
//         *
//         * transaction begin
//         *
//         *  insert data transfer
//         *      (transferID)
//         *
//         * 
//         * - update balance to virtual account
//         *          balance = balance + transferAmount
//         *
//         * - update balance from virtual acocunt:
//         *          balance = balance - transfer Amount
//         *
//         *  insert into transaction history:
//         *      -transactionID <= transferID
//         *      - virtualAccount <= fromVirtualAccountID
//                    -transaction type= "Transfer"

//         *  insert into transaction history:
//         *      -transactionID <= transferID
//         *      - virtualAccountID <= toVirtualAccountID
//                    -transaction type= "Transfer"


//         * jika terjadi error, maka transction rollback()
//         * 
//         * jika ok, transaction commit()
//         *
//         * return transferID
//         * 


//return 0;

//    }

//    public List<Models.Transfer> FindList(long virtualAccountID)
//{
//    return new List<Models.Transfer>();
//}
//}
//*/
