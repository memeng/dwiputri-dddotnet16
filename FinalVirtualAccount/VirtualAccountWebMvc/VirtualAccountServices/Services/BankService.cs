﻿using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace VirtualAccountServices.Services
{
    public class BankService : BaseService
    {
        public BankService(VirtualAccountDbContext db, string loginID) : base(db, loginID)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE bank;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();


            //var _dbConn = _db.Database.GetDbConnection();
            //_dbConn.Open();
            //_cmd = _dbConn.CreateCommand();
            // _cmd.CommandText = "TRUNCATE TABLE bank_account;";
            // _cmd.CommandType = System.Data.CommandType.Text;
            // _cmd.ExecuteNonQuery();
            // _dbConn.Close();

            // _cmd = _dbConn.CreateCommand();
            // _cmd.CommandText = "TRUNCATE TABLE top_up;";
            // _cmd.CommandType = System.Data.CommandType.Text;
            // _cmd.ExecuteNonQuery();
            // _dbConn.Close();

            // _cmd = _dbConn.CreateCommand();
            // _cmd.CommandText = "TRUNCATE TABLE withdraw;";
            // _cmd.CommandType = System.Data.CommandType.Text;
            // _cmd.ExecuteNonQuery();
            // _dbConn.Close();
        }


        private void validateBase(string bankID,
            string bankName)
        ////List<BankAccountModel> bankAccounts,
        ////            List<TopUpModel> topUps,
        ////   List<WithdrawModel> withdraws)
        {
            if (string.IsNullOrEmpty(bankID))
            {
                throw new Exception("id_bank_kosong");
            }
            if (string.IsNullOrEmpty(bankName))
            {
                throw new Exception("nama_bank_kosong");
            }


            //if (bankAccounts == null)
            //{
            //    throw new Exception("bankAccounts_is_required");
            //}
            //if (topUps == null)
            //{
            //    throw new Exception("topUps_is_required");
            //}
            //if (withdraws == null)
            //{
            //    throw new Exception("withdraws_is_required");
            //}

            //if (bankAccounts.Count == 0)
            //{
            //    throw new Exception("bankAccounts_is_required");
            //}

            //if (topUps.Count == 0)
            //{
            //    throw new Exception("topUps_is_required");
            //}

            //if (withdraws.Count == 0)
            //{
            //    throw new Exception("withdraws_is_required");
            //}

            //foreach (var g in bankAccounts)
            //{
            //    if (string.IsNullOrEmpty(g.BankAccountNumber))
            //    {
            //        throw new Exception("bank_account_number_is_required");
            //    }
            //    if (string.IsNullOrEmpty(g.BankAccountOwner))
            //    {
            //        throw new Exception("bank_account_Owner_is_required");
            //    }
            //}



        }

        private void setValue(ref Models.BankModel _bank,
            string bankID,
            string bankName)
        {
            // var _tr = _db.Database.BeginTransaction();
            _bank.BankID = bankID;
            _bank.BankName = bankName;
        }
        public void AddBank(string bankID,
            string bankName)
        {

            validateBase(bankID, bankName);

            var _bank = new Models.BankModel();

            setValue(ref _bank, bankID, bankName);

            _db.Banks.Add(_bank);

            _db.SaveChanges();

        }

        public void UpdateBank(string bankID,
            string bankName)
        {
            validateBase(bankID, bankName);

            var _bank = findBankByBankID(bankID, false);
            if (_bank != null)
            {
                _bank.BankName = bankName;
                //_bank.UpdatedAt = DateTime.Now.ToUniversalTime();
                //_bank.UpdatedBy = _loginEmail;
                _db.SaveChanges();
            }

        }

        public void DeleteBank(string bankID)
        {
            if (string.IsNullOrEmpty(bankID))
            {
                throw new Exception("invalid_kode_bank");
            }
            var _bank = findBankByBankID(bankID, false);
            if (_bank != null)
            {
                _db.Banks.Remove(_bank);
                _db.SaveChanges();
            }

        }


        private Models.BankModel findBankByBankID(string bankID, bool asNoTracking)
        {
            if (string.IsNullOrEmpty(bankID))
            {
                throw new ArgumentNullException("bank_id_is_null");
            }

            Models.BankModel _fBank;
            if (asNoTracking)
            {
                _fBank = _db.Banks.AsNoTracking()
                    .FirstOrDefault(x => x.BankID.ToLower().Equals(bankID));
            }
            else
            {
                _fBank = _db.Banks
                   .FirstOrDefault(x => x.BankID.ToLower().Equals(bankID));
            }

            if (_fBank == null)
            {
                throw new NullReferenceException("bank_id_not_found");
            }
            return _fBank;
        }

        public Models.BankModel FindBankByBankID(string bankID, bool AsNoTracking)
        {
            return findBankByBankID(bankID, AsNoTracking);
        }
        public List<Models.BankModel> FindBankList()
        {
            return _db.Banks.ToList();
        }

    }
}
