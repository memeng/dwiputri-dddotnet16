﻿using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;


namespace VirtualAccountServices.Services
{
    public class VirtualAccountService : BaseService
    {
        public VirtualAccountService(VirtualAccountDbContext db, string loginID) : base(db, loginID)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE virtual_account;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();

            _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE register;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();

            _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE login;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();

            _dbConn.Close();
        }

        private string _generateToken(string email)
        {
            var rawToken = email + Helpers.IDHelper.ID().ToString();
            return HashService.GenerateHash(rawToken);
        }


        public Models.VirtualAccountModel FindEmailorPassword(string email, string password)
        {
            var _email = _db.VirtualAccounts.Where(x => x.Email == email)
                 //.OrderByDescending(x => x.CreatedAt)
                 .FirstOrDefault();


            var _password = _db.VirtualAccounts.Where(x => x.Password == password)
                 //.OrderByDescending(x => x.CreatedAt)
                 .FirstOrDefault();


            if (_email == null)
            {
                throw new Exception("invalid_login_session");
            }

            else if (_password == null)
            {
                throw new Exception("invalid_password");
            }

            var _Login = new Models.VirtualAccountModel();
            _Login.CreatedAt = DateTime.Now.ToUniversalTime();
            _Login.Password = _generateToken(password);
            _Login.Email = email;
            //_Login.Expired = false;

            _db.VirtualAccounts.Add(_Login);
            _db.SaveChanges();

            return null;


        }



        private void validateBase(string virtualAccountID,
            string email,
            string password,
            string username,
            string mobilephone,
            int balance)
        {
            if (string.IsNullOrEmpty(virtualAccountID))
            {
                throw new Exception("virtual_account_id_kosong");
            }
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("email_kosong");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("password_kosong");
            }
            if (string.IsNullOrEmpty(username))
            {
                throw new Exception("username_kosong");
            }
            if (string.IsNullOrEmpty(mobilephone))
            {
                throw new Exception("mobilephone_kosong");
            }
            if (balance < 0)
            {
                throw new Exception("balance_negatif");
            }
        }

        private void setValue(ref Models.VirtualAccountModel _virtualAccount,
            string virtualAccountID,
            string email,
            string password,
            string username,
            string mobilephone,
            int balance)
        {

            _virtualAccount.VirtualAccountID = virtualAccountID;
            _virtualAccount.Email = email;
            _virtualAccount.Password = password;
            _virtualAccount.Username = username;
            _virtualAccount.MobilePhone = mobilephone;
            _virtualAccount.Balance = balance;

        }

        public void AddVirtualAccount(string virtualAccountID,
            string email,
            string password,
            string username,
            string mobilephone,
            int balance)
        {

            validateBase(virtualAccountID,
            email,
            password,
            username,
            mobilephone,
            balance);

            var _virtualAccount = new Models.VirtualAccountModel();

            _virtualAccount.CreatedAt = DateTime.Now.ToUniversalTime();

            setValue(ref _virtualAccount,
                virtualAccountID,
            email,
            password,
            username,
            mobilephone,
            balance);

            _db.VirtualAccounts.Add(_virtualAccount);

            _db.SaveChanges();

        }

        public void UpdateVirtualAccount(string virtualAccountID,
            string email,
            string password,
            string username,
            string mobilephone,
            int balance)
        {
            validateBase(virtualAccountID, email, password, username, mobilephone, balance);

            var _virtualAccount = findVirtualAccountByVirtualAccountID(virtualAccountID, false);
            if (_virtualAccount != null)
            {
                _virtualAccount.Email = email;
                _virtualAccount.Password = password;
                _virtualAccount.Username = username;
                _virtualAccount.MobilePhone = mobilephone;
                _virtualAccount.Balance = balance;

                _db.SaveChanges();
            }

        }

        private Models.VirtualAccountModel findVirtualAccountByVirtualAccountID(string virtualAccountID, bool AsNoTracking)
        {
            if (string.IsNullOrEmpty(virtualAccountID))
            {
                throw new ArgumentNullException("virtual_account_id_is_null");
            }

            var _fvirtualAccount = _db.VirtualAccounts?.Where(x => x.VirtualAccountID.ToLower().Equals(virtualAccountID)).FirstOrDefault();
            if (_fvirtualAccount == null)
            {
                throw new NullReferenceException("virtual_account_id_not_found");
            }
            return _fvirtualAccount;
        }

        public Models.VirtualAccountModel FindVirtualAccountByVirtualAccountID(string virtualAccountID, bool AsNoTracking)
        {
            return findVirtualAccountByVirtualAccountID(virtualAccountID, AsNoTracking);
        }

        public List<Models.VirtualAccountModel> FindList()
        {
            return _db.VirtualAccounts.ToList();
        }


        public Models.VirtualAccountModel FindVirtualAccountByEmail(string email)
        {
            var _email = _db.VirtualAccounts.Where(x => x.Email == email)
                //.OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
            if (_email == null)
            {
                throw new Exception("invalid_email");
            }
            return _email;
        }

        private Models.VirtualAccountModel? _findVirtualAccountByEmail(string email)
        {
            return _db.VirtualAccounts.Where(x => x.Email == email)
                //.OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
        }

        public Models.LoginModel FindByLoginID(string loginID)
        {
            var _login = _db.Logins.Where(x => x.LoginID == loginID)
                .OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
            if (_login == null)
            {
                throw new Exception("invalid_login_session");
            }
            return _login;
        }

        public void Logout(string loginID)
        {
            var _login = FindByLoginID(loginID);
            if (_login != null)
            {
                _login.Expired = true;
                _db.SaveChanges();

            }
        }

        public List<Models.VirtualAccountModel> FindVirtualAccountList()
        {
            return _db.VirtualAccounts.ToList();
        }

        public Models.VirtualAccountModel Balance(string virtualAccountID)
        {
            return new Models.VirtualAccountModel();

        }

    }
}

/*
  public Models.VirtualAccount Saldo(long virtualAccountID)
    {
        return new Models.VirtualAccount();
    }


 */