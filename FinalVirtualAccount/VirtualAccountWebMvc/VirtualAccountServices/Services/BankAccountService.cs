﻿using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountServices.Services
{
    public class BankAccountService : BaseService
    {
        public BankAccountService(VirtualAccountDbContext db, string loginID) : base(db, loginID)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE bank_account;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }


        private void validateBase(string bankAccountID,
            string virtualAccountID,
            string bankID,
            string bankAccountNumber,
            string bankAccountOwner,
            int balance)
        {
            if (string.IsNullOrEmpty(bankAccountID))
            {
                throw new ArgumentNullException("bank_account_id_is_null");
            }
            if (string.IsNullOrEmpty(virtualAccountID))
            {
                throw new ArgumentNullException("virtual_account_id_is_null");
            }
            if (string.IsNullOrEmpty(bankID))
            {
                throw new ArgumentNullException("bank_id_is_null");
            }
            if (string.IsNullOrEmpty(bankAccountNumber))
            {
                throw new ArgumentNullException("bank_account_number_is_null");
            }

            if (string.IsNullOrEmpty(bankAccountOwner))
            {
                throw new ArgumentNullException("bank_account_owner_is_null");
            }
            if (balance < 0)
            {
                throw new ArgumentNullException("balance is negative");
            }
        }

        private void setValue(ref Models.BankAccountModel _bankAccount,
            string bankAccountID,
            string virtualAccountID,
            string bankID,
            string bankAccountNumber,
            string bankAccountOwner,
            int balance)
        {
            _bankAccount.BankAccountID = bankAccountID;
            _bankAccount.VirtualAccountID = virtualAccountID;
            _bankAccount.BankID = bankID;
            _bankAccount.BankAccountNumber = bankAccountNumber;
            _bankAccount.BankAccountOwner = bankAccountOwner;
            _bankAccount.Balance = balance;

        }

        public void AddBankAccount(string bankAccountID,
            string virtualAccountID,
            string bankID,
            string bankAccountNumber,
            string bankAccountOwner,
            int balance)
        {
            validateBase(bankAccountID,
                virtualAccountID,
             bankID,
             bankAccountNumber,
            bankAccountOwner,
            balance);

            var _bankAccount = new Models.BankAccountModel();

            //_bankAccount.BankAccountID = Helpers.IDHelper.ID();
            //Thread.Sleep(100);


            setValue(ref _bankAccount,
                bankAccountID,
                virtualAccountID,
                bankID,
             bankAccountNumber,
            bankAccountOwner,
            balance);

            _db.BankAccounts.Add(_bankAccount);
            _db.SaveChanges();

            //return _bankAccount.BankAccountID;

        }

        public void UpdateBankAccount(string bankAccountID,
            string virtualAccountID,
            string bankID,
            string bankAccountNumber,
            string bankAccountOwner,
            int balance)
        {

            validateBase(bankAccountID,
                virtualAccountID,
                bankID,
             bankAccountNumber,
            bankAccountOwner,
            balance);

            var _bankAccount = _findByBankAccountID(bankAccountID);
            if (_bankAccount != null)
            {
                // _bankAccount.UpdatedAt = DateTime.Now.ToUniversalTime();
                setValue(ref _bankAccount,
                    bankAccountID,
                    virtualAccountID,
                    bankID,
             bankAccountNumber,
            bankAccountOwner,
            balance);
                _db.SaveChanges();
            }
        }

        public Models.BankAccountModel _findByBankAccountID(string bankAccountID)
        {
            if (string.IsNullOrEmpty(bankAccountID))
            {
                throw new ArgumentNullException("bank_account_is_required");
            }
            var _fBankAccount = _db.BankAccounts.Where(x => x.BankAccountID == bankAccountID).FirstOrDefault();
            if (_fBankAccount == null)
            {
                throw new NullReferenceException("bank_account_not_found");
            }
            return _fBankAccount;
        }

        public Models.BankAccountModel FindByBankAccountID(string bankAccountID)
        {
            return _findByBankAccountID(bankAccountID);
        }

        public List<Models.BankAccountModel> FindBankAccountList()
        {
            return _db.BankAccounts.ToList();
        }

    }
}
