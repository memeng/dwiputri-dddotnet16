use VIRTUAL_ACCOUNT;


drop table if exists [virtual_account];

create table virtual_account
(virtual_account_id varchar (50),
email varchar (50),
password varchar (50),
username varchar (50),
mobile_phone varchar (50),
balance decimal, 
created_at datetime,

 CONSTRAINT [PK_virtual_account] PRIMARY KEY CLUSTERED 
(
	[virtual_account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
                                                                                                                                   