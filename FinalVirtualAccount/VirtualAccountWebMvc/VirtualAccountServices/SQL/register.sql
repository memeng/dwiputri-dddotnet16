use VIRTUAL_ACCOUNT;

drop table if exists [register];

create table register 
(email varchar (50),
password varchar (50),
username varchar (50),
active bit,
created_at datetime,

CONSTRAINT [PK_register] PRIMARY KEY CLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

