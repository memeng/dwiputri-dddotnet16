USE VIRTUAL_ACCOUNT;

drop table IF EXISTS [login];

create table login(
login_id varchar(50),
email varchar (50),
username varchar (50),
created_at datetime,
expired bit,

CONSTRAINT [PK_login] PRIMARY KEY CLUSTERED 
(
	[login_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
