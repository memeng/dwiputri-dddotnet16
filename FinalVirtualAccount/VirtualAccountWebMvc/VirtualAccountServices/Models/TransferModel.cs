﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("transferr")]  //transfer antar virtual account
    public class TransferModel
    {
        [Key]
        [Column("transfer_id", TypeName = "varchar(50)")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string TransferID { get; set; } = string.Empty;

        [Column("transfer_date", TypeName = "datetime")]
        public DateTime TransferDate { get; set; }

        [Column("from_virtual_account_id", TypeName = "varchar(50)")]
        public string FromVirtualAccountID { get; set; } = string.Empty;

        [Column("to_virtual_account_id", TypeName = "varchar(50)")]
        public string ToVirtualAccountID { get; set; } = string.Empty;

        [Column("transfer_amount", TypeName = "decimal")]
        public int TransferAmount { get; set; }

        [NotMapped]
        public string strTransferDate { get; set; }

        //        [ForeignKey("FromVirtualAccountID")]
        //        public VirtualAccountModel FromKodeAkun { get; set; } = new VirtualAccountModel();

        //        [ForeignKey("ToVirtualAccountID")]
        //        public VirtualAccountModel ToKodeAkun { get; set; } = new VirtualAccountModel();

        public TransferModel() { }

        public TransferModel(
    string transferDate,
    string fromVirtualAccountID,
    string toVirtualAccountID,
    int transferAmount)
        {
            this.strTransferDate = transferDate;
            this.FromVirtualAccountID = fromVirtualAccountID;
            this.ToVirtualAccountID = toVirtualAccountID;
            this.TransferAmount = transferAmount;
        }
    }
}

