﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("transactionn")]
    public class TransactionModel
    {
        [Key]
        [Column("transaction_id", TypeName = "varchar(50)")]
        public string TransactionID { get; set; } = string.Empty;

        [Column("transaction_date", TypeName = "datetime")]
        public DateTime TransactionDate { get; set; }

        [Column("virtual_account_id", TypeName = "varchar(50)")]
        public string VirtualAccountID { get; set; } = string.Empty;

        [Column("merchant_account_id", TypeName = "varchar(50)")]
        public string MerchantAccountID { get; set; } = string.Empty;

        [Column("transaction_amount", TypeName = "decimal")]
        public int TransactionAmount { get; set; }

        [Column("reference_id", TypeName = "varchar(50)")]
        public string ReferenceID { get; set; } = string.Empty;

        [NotMapped]
        public string strTransactionDate { get; set; } = string.Empty;

        //        [ForeignKey("VirtualAccountID")]
        //        public VirtualAccountModel KodeAkun { get; set; } = new VirtualAccountModel();


        public TransactionModel() { }

        public TransactionModel(string transactionDate,
    string virtualAccountID,
    string merchantAccountID,
    int transactionAmount,
    string referenceID)
        {

            this.strTransactionDate = transactionDate;
            this.VirtualAccountID = virtualAccountID;
            this.MerchantAccountID = merchantAccountID;
            this.TransactionAmount = transactionAmount;
            this.ReferenceID = referenceID;
        }

    }
}
