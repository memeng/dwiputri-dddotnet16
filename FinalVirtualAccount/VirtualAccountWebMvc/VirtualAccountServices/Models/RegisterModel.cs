﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using VirtualAccountServices.Models;

namespace VirtualAccountServices.Models
{
    [Table("register")]
    public class RegisterModel
    {

        [Key]
        [Column("email", TypeName = "varchar(50)")]
        public string Email { get; set; } = string.Empty;

        [Column("password", TypeName = "varchar(50)")]
        public string Password { get; set; } = string.Empty;

        [Column("username", TypeName = "varchar(50)")]
        public string Username { get; set; } = string.Empty;

        [Column("mobile_phone", TypeName = "varchar(50)")]
        public string MobilePhone { get; set; } = string.Empty;

        [Column("active", TypeName = "bit")]
        public bool Active { get; set; }


        [Column("created_at", TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }

      //  public VirtualAccountModel VirtualAccountModel { get; set; }
       // public LoginModel LoginModel { get; set; }

        //[Column("created_by", TypeName = "varchar(50)")]
        //public string CreatedBy { get; set; } = string.Empty;


        //[Column("updated_at", TypeName = "datetime")]
        //public DateTime? UpdatedAt { get; set; }

        //[Column("updated_by", TypeName = "varchar(50)")]
        //public string? UpdatedBy { get; set; } = string.Empty;

    }
}
