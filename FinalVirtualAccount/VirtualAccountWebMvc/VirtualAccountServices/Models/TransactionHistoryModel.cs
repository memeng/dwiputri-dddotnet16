﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("transaction_history")]
    public class TransactionHistoryModel
    {
        [Key]
        [Column("history_id", TypeName = "varchar (50)")]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string HistoryID { get; set; }

        [Column("virtual_account_id", TypeName = "Varchar(50)")]
        public string VirtualAccountID { get; set; }

        [Column("merchant_account_id", TypeName = "Varchar(50)")]
        public string MerchantAccountID { get; set; }

        [Column("transaction_id", TypeName = "varchar (50)")]
        public string TransactionID { get; set; }

        [Column("history_date", TypeName = "datetime")]
        public DateTime HistoryDate { get; set; }

        [Column("amount", TypeName = "decimal")]
        public int Amount { get; set; }

        [Column("transaction_type", TypeName = "varchar(50)")]
        public string TransactionType { get; set; }

        //[ForeignKey("VirtualAccountID")]
        //public VirtualAccountModel KodeAkun { get; set; } = new VirtualAccountModel();

        //[ForeignKey("MerchantAccountID")]
        //public TransactionModel KodeMerchantAkun { get; set; } = new TransactionModel();

        //[ForeignKey("TransactionID")]
        //public TransactionModel Transaction { get; set; } = new TransactionModel();

        [NotMapped]
        public string strHistoryDate { get; set; }


        public TransactionHistoryModel() { }

        public TransactionHistoryModel(string historyID,
            string virtualAccountID,
            string merchantAccountID,
        string transactionID,
        string historyDate,
        int amount,
                string transactionType)
        {
            this.HistoryID = historyID;
            this.VirtualAccountID = virtualAccountID;
            this.MerchantAccountID = merchantAccountID;
            this.TransactionID = transactionID;
            this.strHistoryDate = historyDate;
            this.Amount = amount;
            this.TransactionType = transactionType;
        }


    }
}

