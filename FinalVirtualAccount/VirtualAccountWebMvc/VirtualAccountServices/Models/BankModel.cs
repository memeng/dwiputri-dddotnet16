﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace VirtualAccountServices.Models
{
    [Table("bank")]
    public class BankModel
    {
        [Key]
        [Column("bank_id", TypeName = "varchar(50)")]
        public string BankID { get; set; } = String.Empty;

        [Column("bank_name", TypeName = "varchar(50) ")]
        public string BankName { get; set; } = String.Empty;



        //[ForeignKey("BankAccountID")]
       //ublic List<BankAccountModel> BankAccounts { get; set; } = new List<BankAccountModel>();

        //[ForeignKey("TopUpID")]
        //public List<TopUpModel> TopUps { get; set; } = new List<TopUpModel>();

        //[ForeignKey("WithdrawID")]
        //public List<WithdrawModel> Withdraws { get; set; } = new List<WithdrawModel>();

        public BankModel() { }
        public BankModel(string bankID,
            string bankName)
        {
            this.BankID = bankID;
            this.BankName = bankName;
        }
    }
}
