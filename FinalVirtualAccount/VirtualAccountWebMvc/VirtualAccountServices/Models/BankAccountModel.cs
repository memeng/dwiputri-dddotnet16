﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace VirtualAccountServices.Models
{
    [Table("bank_account")]
    public class BankAccountModel
    {
        [Key]
        [Column("bank_account_id", TypeName = "varchar(50)")]
        public string BankAccountID { get; set; } = String.Empty;

        [Column("virtual_account_id", TypeName = "varchar(50)")]
        public string VirtualAccountID { get; set; } = String.Empty;

        [Column("bank_id", TypeName = "varchar(50)")]
        public string BankID { get; set; } = String.Empty;

        [Column("bank_account_number", TypeName = "varchar(50)")]
        public string BankAccountNumber { get; set; } = String.Empty;

        [Column("bank_account_owner", TypeName = "varchar(50)")]
        public string BankAccountOwner { get; set; } = String.Empty;

        [Column("balance", TypeName = "decimal")]
        public int Balance { get; set; }

        //        [ForeignKey("bank_id")]
        //        public BankModel KodeBank { get; set; } = new BankModel();

        //        [ForeignKey("virtual_account_id")]
        //        public VirtualAccountModel KodeAkun { get; set; } = new VirtualAccountModel();

        public BankAccountModel() { }

        public BankAccountModel(string bankAccountID,
            string virtualAccountID,
    string bankID,
    string bankAccountNumber,
    string bankAccountOwner,
    int balance)
        {

            this.BankAccountID = bankAccountID;
            this.VirtualAccountID = virtualAccountID;
            this.BankID = bankID;
            this.BankAccountNumber = bankAccountNumber;
            this.BankAccountOwner = bankAccountOwner;
            this.Balance = balance;
        }


    }
}



