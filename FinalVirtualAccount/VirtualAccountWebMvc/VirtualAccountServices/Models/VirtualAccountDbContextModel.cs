﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.SqlServer;

namespace VirtualAccountServices.Models
{
    public class VirtualAccountDbContext : DbContext
    {

        //        /* tables */
        // public DbSet<AdminUserModel> AdminUsers { get; set; }

        public DbSet<LoginModel> Logins { get; set; }

        public DbSet<RegisterModel> Registers { get; set; }

        public DbSet<BankAccountModel> BankAccounts { get; set; }

        public DbSet<BankModel> Banks { get; set; }

        public DbSet<TopUpModel> TopUps { get; set; }

        public DbSet<TransactionHistoryModel> TransactionHistories { get; set; }

        public DbSet<TransactionModel> Transactions { get; set; }
        public DbSet<TransferModel> Transfers { get; set; }



        public DbSet<VirtualAccountModel> VirtualAccounts { get; set; }

        public DbSet<WithdrawModel> Withdraws { get; set; }


        //        /*constructors*/


        public VirtualAccountDbContext(DbContextOptions<VirtualAccountDbContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<LoginModel>()
        //    .HasOne(p => p.RegisterModel)
        //    .WithMany(b => b.Logins);
        //}

        //private string _connString;
        //public VirtualAccountDbContext(string connectionString)
        //{
        //    _connString = connectionString;
        //}



        //        /*Configurasi DbContext*/
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BankAccountModel>();

            modelBuilder.Entity<LoginModel>();


            modelBuilder.Entity<BankModel>();

            modelBuilder.Entity<TopUpModel>();

            modelBuilder.Entity<TransactionHistoryModel>();

            modelBuilder.Entity<TransactionModel>();

            modelBuilder.Entity<TransferModel>();

            modelBuilder.Entity<VirtualAccountModel>();
            //.HasOne(b => b.RegisterModel)
            //.WithOne(i => i.VirtualAccountModel)
            //.HasForeignKey<RegisterModel>(b => b.Email);

            modelBuilder.Entity<WithdrawModel>();
            //modelBuilder.Entity<LoginModel>();
            //.Property<string>(Email);

            //  modelBuilder.Entity<Blog>()
            //.HasOne(b => b.BlogImage)
            //.WithOne(i => i.Blog)
            //.HasForeignKey<BlogImage>(b => b.BlogForeignKey);


            modelBuilder.Entity<RegisterModel>();
               // .HasOne(b => b.LoginModel)
                //.WithOne(i => i.RegisterModel)
                //.HasForeignKey<LoginModel>(b => b.Email);


        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(_connString);
        //}

    }


    public class VirtualAccountDbContextFactory : IDesignTimeDbContextFactory<VirtualAccountDbContext>
    {
        public VirtualAccountDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<VirtualAccountDbContext>();

            optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=VIRTUAL_ACCOUNT;User Id=sa;Password=123456; TrustServerCertificate=True;  Trusted_Connection=True;");

            return new VirtualAccountDbContext(optionsBuilder.Options);

            //DESKTOP-1T59TA1
        }
        /*
                      optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=AIRLINE_BOOKING;User Id=sa;Password=123456;");


          */
    }


}
