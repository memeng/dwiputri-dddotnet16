﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using VirtualAccountServices.Models;

namespace VirtualAccountServices.Models
{
    [Table("login")]
    public class LoginModel
    {

        [Key]
        [Column("login_id", TypeName = "varchar(50)")]
        public string LoginID { get; set; } = string.Empty;

        [Column("email", TypeName = "varchar(50)")]
        public string Email { get; set; }

        [Column("username", TypeName = "varchar(50)")]
        public string Username { get; set; } = string.Empty;

        [Column("created_at", TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }

        [Column("expired", TypeName = "bit")]
        public bool Expired { get; set; }

        //public RegisterModel RegisterModel { get; set; }


        //[ForeignKey("username")]
        //public RegisterModel Username2 { get; set; } = new RegisterModel();

        //[ForeignKey("created_at")]
        //public RegisterModel CreatedAt2 { get; set; } = new RegisterModel();


    }
}
