﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("withdraw")]
    public class WithdrawModel
    {
        [Key]
        [Column("withdraw_id", TypeName = "varchar(50)")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string WithdrawID { get; set; }

        [Column("virtual_account_id", TypeName = "varchar(50)")]
        public string VirtualAccountID { get; set; }

        [Column("withdraw_amount", TypeName = "decimal")]
        public int WithdrawAmount { get; set; } //fk

        [Column("withdraw_date", TypeName = "datetime")]
        public DateTime WithdrawDate { get; set; }

        [Column("bank_id", TypeName = "varchar(50)")] //fk
        public string BankID { get; set; }

        [Column("bank_name", TypeName = "varchar(50)")]
        public string BankName { get; set; }

        [Column("bank_account_number", TypeName = "varchar(50)")]
        public string BankAccountNumber { get; set; }

        [Column("bank_account_owner", TypeName = "varchar(50)")]
        public string BankAccountOwner { get; set; }

        [NotMapped]
        public string strWithdrawDate { get; set; }

        //        [ForeignKey("VirtualAccountID")]
        //        public VirtualAccountModel KodeAkun { get; set; } = new VirtualAccountModel();

        //        [ForeignKey("BankID")]
        //        public BankModel KodeBank { get; set; } = new BankModel();

        //        [ForeignKey("BankName")]
        //        public BankModel NamaBank { get; set; } = new BankModel();

        //        [ForeignKey("BankAccountNumber")]
        //        public BankAccountModel NomorRekening { get; set; } = new BankAccountModel();

        //        [ForeignKey("BankAccountOwner")]
        //        public BankAccountModel PemilikRekening { get; set; } = new BankAccountModel();



        public WithdrawModel() { }

        public WithdrawModel(string virtualAccountID,
    int withdrawAmount,
    string withdrawDate,
    string bankID,
    string bankName,
    string bankAccountNumber,
    string bankAccountOwner)
        {
            this.VirtualAccountID = virtualAccountID;
            this.WithdrawAmount = withdrawAmount;
            this.strWithdrawDate = withdrawDate;
            this.BankID = bankID;
            this.BankName = bankName;
            this.BankAccountNumber = bankAccountNumber;
            this.BankAccountOwner = bankAccountOwner;
        }

    }
}

