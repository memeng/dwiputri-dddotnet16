﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;



namespace VirtualAccountServices.Models
{
    [Table("top_up")]
    public class TopUpModel
    {
        [Key]
        [Column("topup_id", TypeName = "varchar(50)")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string TopUpID { get; set; }

        [Column("virtual_account_id", TypeName = "varchar(50)")]
        public string VirtualAccountID { get; set; } //fk

        [Column("topup_amount", TypeName = "decimal")]
        public int TopUpAmount { get; set; }

        [Column("topup_date", TypeName = "datetime")]
        public DateTime TopUpDate { get; set; }

        [Column("bank_id", TypeName = "varchar(50)")]
        public string BankID { get; set; }

        [Column("bank_name", TypeName = "varchar(50)")]
        public string BankName { get; set; }

        [Column("bank_account_number", TypeName = "varchar(50)")]
        public string BankAccountNumber { get; set; }

        [Column("bank_account_owner", TypeName = "varchar(50)")]
        public string BankAccountOwner { get; set; }

        [NotMapped]
        public string strTopUpDate { get; set; }


        //        [ForeignKey("virtual_account_id")]
        //        public VirtualAccountModel KodeAkun { get; set; } = new VirtualAccountModel();

        //        [ForeignKey("bank_id")]
        //        public BankModel KodeBank { get; set; } = new BankModel();

        //        [ForeignKey("bank_name")]
        //        public BankModel NamaBank { get; set; } = new BankModel();

        //        [ForeignKey("bank_account_number")]
        //        public BankAccountModel NomorRekening { get; set; } = new BankAccountModel();

        //        [ForeignKey("bank_account_owner")]
        //        public BankAccountModel PemilikRekening { get; set; } = new BankAccountModel();

        public TopUpModel() { }


        public TopUpModel(int topUpAmount,
            string virtualAccountID,
            string topupdate,
            string bankID,
            string bankName,
            string bankAccountNumber,
            string bankAccountOwner)
        {

            this.TopUpAmount = topUpAmount;
            this.VirtualAccountID = virtualAccountID;
            this.strTopUpDate = topupdate;
            this.BankID = bankID;
            this.BankName = bankName;
            this.BankAccountNumber = bankAccountNumber;
            this.BankAccountOwner = bankAccountOwner;
        }

    }
}
