﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace VirtualAccountServices.Models
{
    [Table("virtual_account")]
    public class VirtualAccountModel
    {
        [Key]
        [Column("virtual_account_id", TypeName = "varchar(50)")]
        public string VirtualAccountID { get; set; }

        [Column("email", TypeName = "varchar(50)")]
        public string Email { get; set; }

        [Column("password", TypeName = "varchar(50)")]
        public string Password { get; set; }

        [Column("username", TypeName = "varchar(50)")]
        public string Username { get; set; }

        [Column("balance", TypeName = "decimal")]
        public int Balance { get; set; }

        [Column("mobile_phone", TypeName = "varchar(50)")]
        public string MobilePhone { get; set; }

        [Column("created_at", TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }

        // public RegisterModel RegisterModel { get; set; }


        //[ForeignKey("email")]
        //public RegisterModel EmailPengguna { get; set; } = new RegisterModel();

        //[ForeignKey("balance")]
        //public BankAccountModel Saldo { get; set; } = new BankAccountModel();


        public VirtualAccountModel() { }
        public VirtualAccountModel(string virtualAccountID,
            string email,
            string password,
            string username,
            string mobilePhone,
            int balance)
        {
            this.VirtualAccountID = virtualAccountID;
            this.Email = email;
            this.MobilePhone = mobilePhone;
            this.Password = password;
            this.Username = username;
            this.Balance = balance;
        }
    }


}
